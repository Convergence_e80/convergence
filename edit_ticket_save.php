<!--%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

VERSION: 	

- 1.1

COMMENTS:	(add comment describing what has been changed)

-  
- 

DATE:

- 03/14/13

AUTHOR:

- Jose Hermida

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%-->

<?php	
      include("check_connection.php");
	  require("send_email.php");
	  ini_set('display_errors','Off');

      $value = 'cualquier cosa';
      setcookie("TestCookie", $value);
	  
	  
	  $snom = $_SESSION["nom"];

	  $ticket = $_GET['Id'];	  
	  $title = $_POST['title'];
	  $post = $_POST['post'];
	  $priority = $_POST['priority'];	  	  
	  $status = $_POST['status'];
	  $comment = $_POST['comment'];
	  $assignee = $_POST['assignee'];	  
	  $division = $_POST['division'];	
	  $Job_type = $_POST['Job_type'];		  	
	  $level = $_POST['level'];		
	  
	  $name_contact = $_POST['name_contact'];	  
	  $cellphone_contact = $_POST['cellphone_contact'];	
	  $phone_contact = $_POST['phone_contact'];		  	
	  $email_contact = $_POST['email_contact'];
	  	  
	  
	 $title_new = str_replace("'", "�", $title ); 
	 $post_new = str_replace("'", "�", $post );	  

	  $tag1 = $_POST['tag1'];	  
	  $tag2 = $_POST['tag2'];	  
	  $tag3 = $_POST['tag3'];	  	    	  	 

	  $time = date("Y-m-d H:i:s"); 	  
	  $date = date("Y-m-d");
	  $time2 = date("H:i:s");

	  
	  $save_sql = " UPDATE Tickets 	SET 	
                    Ticket_Title = '$title_new', 
					Ticket_Post = '$post_new',
                    Priority = '$priority', 
					Status = '$status' , 
					Id_Assignee = '$assignee',
					
					Name_Contact = '$name_contact',
					Cellphone_Contact = '$cellphone_contact',																						
					Email_Contact = '$email_contact',
					Phone_Contact = '$phone_contact',																						
					
					Id_System = '$division',
					Job_Type = '$Job_type',
					Level = '$level',
					Tag1_Id = '$tag1',
					Tag2_Id = '$tag2',											
					Tag3_Id = '$tag3'";
	
	if ($status == 6) {
		$save_sql .= ", Comment = '$comment'";
	}
	
	$save_sql .= "WHERE 	Id = '$ticket' ";
	  
	  $result = sqlsrv_query( $conn, $save_sql );	


      if( $result === false )
             {
               echo "Error in statement execution.\n";
               die( print_r( sqlsrv_errors(), true));
              }		
			  
		   $sta = "2";  
           // Update Ticke History
		  
		  //if ($status == '7') { $ticket_user = $_SESSION["ticket_user"]; }
		  
		  $ticket_user = $_SESSION["ticket_user"];
		  
		  $save_sql_Hist = " INSERT INTO Ticket_History  
		                                (Id_Ticket, 
	                                     Id_Priority, 
										 Id_Status, 
										 Id_Division,
										 Id_User,
										 Date_Time,
										 Status,
										 Id_User_changed_by
										 )
										  
                                  VALUES ('$ticket',
								         '$priority',
										 '$status',
										 '$division',
										 '$assignee',
										 '$time',
										 '$sta',
										 '$ticket_user'
										 ); ";

					
	  $result_Hist = sqlsrv_query( $conn, $save_sql_Hist );	

      	if( $result_Hist === false )
		{
			echo "Error in statement execution.\n";
			die( print_r( sqlsrv_errors(), true));
		}
		else {
			echo "success!";
		}

	if ($status == 3) {
		$save_sql = " INSERT INTO Posts	(Id_Ticket,  
							  	Post,
								Author,
								Date_Creation,
								Post_Public,
								Time)
							 
					  VALUES 	('$ticket',
							  	'Waiting for feedback: $comment',
								'$snom',
								'$date',
								'1',
								'$time2'); ";

	//die($save_sql);

	$result = sqlsrv_query( $conn, $save_sql );	

	}	  
 
	$query_ticket = "select * from Tickets where Id = '$ticket' ";
	$result_query_ticket = sqlsrv_query($conn,$query_ticket);
	$row_query_ticket = sqlsrv_fetch_array($result_query_ticket);


	$ticket_title = $row_query_ticket[1];
	$ticket_post = $row_query_ticket[2];
	$ticket_creator = $row_query_ticket[3];
	$ticket_status = $row_query_ticket[4]; 
	$ticket_priority = $row_query_ticket[5];
	$ticket_CC = $row_query_ticket[6];	  
	$ticket_assignee = $row_query_ticket[7];
	$ticket_customer = $row_query_ticket[8];
	$date = $row_query_ticket[9];
	$date_update = $row_query_ticket[10];
	$ticket_name_contact = $row_query_ticket[11];
	$ticket_cellphone_contact = $row_query_ticket[12];
	$ticket_email_contact = $row_query_ticket[13];	

	$query_assignee = "select * from Employees where Id=$ticket_assignee";
	$result_query_assignee = sqlsrv_query($conn,$query_assignee);
	$row_query_assignee = sqlsrv_fetch_array($result_query_assignee);

	$assignee=$row_query_assignee[14]." ".$row_query_assignee[13];
	$assignee_email=$row_query_assignee[7];

	$query_creator = "select * from Employees where Id=$ticket_creator";
	$result_query_creator = sqlsrv_query($conn,$query_creator);
	$row_query_creator = sqlsrv_fetch_array($result_query_creator);

	$creator=$row_query_creator[14]." ".$row_query_creator[13];

	$query_status = "select * from Ticket_Status where Id=$ticket_status";
	$result_query_status = sqlsrv_query($conn,$query_status);
	$row_query_status = sqlsrv_fetch_array($result_query_status);

	$status=$row_query_status[1];

	$query_priority = "select * from Priority where Id=$ticket_priority";
	$result_query_priority = sqlsrv_query($conn,$query_priority);
	$row_query_priority = sqlsrv_fetch_array($result_query_priority);

	$priority = $row_query_priority[1];

	$query_customer = "select * from Customers where Id=$ticket_customer";
	$result_query_customer = sqlsrv_query($conn,$query_customer);
	$row_query_customer = sqlsrv_fetch_array($result_query_customer);


	$customer = $row_query_customer[1];
	$customer_email = $row_query_customer[9];


      //session_start(); 
	  $_SESSION['email_copy']= $email_copy;

	  $to = array(
	  	$assignee => $assignee_email,
	  	$name_contact => $email_contact
	  );

	
	  if ($ticket_status == 3) {

		Email::send_email(	'template_post',  // template method
						array(

							'introduction' => 'The purpose of this email is to communicate a new update for the ticket shown below. We are now waiting you to give us a feedback before proceeding',
							'ticket_id' => $ticket,
							'author' => $creator,
							'assignee' => $assignee,
							'status' => $status,
							'priority' => $priority,
							'customer' => $ticket_name_contact,
							'date' => $date,
							'title' => $ticket_title,
							'ticket_content' => $ticket_post,
							'ticket_post' => $comment,
							'contact_name' => $customer,
							'contact_phone' => $ticket_cellphone_contact,
							'contact_email' => $ticket_email_contact
						),
						 														// template content
						'Convergence - Ticketing System',  						// subject
						$to,   													// to  								
						array(), 												// cc
						array(	 												//
							'No Reply' => 'no_reply@elettric80.com')			// reply_to
			);	


	  }

	  elseif ($ticket_status == 6) {

	  	Email::send_email(	'template_post',  // template method

						array(

							'introduction' => "Please confirm if the issue is resolved or not. If we don't receive any feedback this ticket will automatically close in two weeks.",
							'ticket_id' => $ticket,
							'author' => $creator,
							'assignee' => $assignee,
							'status' => $status,
							'priority' => $priority,
							'customer' => $ticket_name_contact,
							'date' => $date,
							'title' => $ticket_title,
							'ticket_content' => $ticket_post,
							'ticket_post' => $comment,
							'contact_name' => $customer,
							'contact_phone' => $ticket_cellphone_contact,
							'contact_email' => $ticket_email_contact
						),
						 														// template content
						'Convergence - Ticketing System',  						// subject
						$to,   													// to  								
						array(), 												// cc
						array(	 												//
							'No Reply' => 'no_reply@elettric80.com')			// reply_to
			);	

	  }

	  else {

	  	Email::send_email(	'template_new_edit',  // template method

						array(

							'introduction' => "The purpose of this email is to communicate a new update for the ticket shown below. Please review the information shown below, and contact us if you have any questions, comments, or any additional information regarding this ticket. Please also feel free to contact the assigned technical support specialist shown below, or the help desk at usahelp@elettric80.it, to arrange any activities such as equipment/system tests, to modify implementation plans, or if the technical support specialist assigned has requested any further information.",
							'ticket_id' => $ticket,
							'author' => $creator,
							'assignee' => $assignee,
							'status' => $status,
							'priority' => $priority,
							'customer' => $ticket_name_contact,
							'title' => $ticket_title,
							'ticket_content' => $ticket_post,
							'contact_name' => $customer,
							'contact_phone' => $ticket_cellphone_contact,
							'contact_email' => $ticket_email_contact
						),
						 														// template content
						'Convergence - Ticketing System',  						// subject
						$to,   													// to  								
						array(), 												// cc
						array(	 												//
							'No Reply' => 'no_reply@elettric80.com')			// reply_to
			);	

	  }

			  	  
   header("Location: ticket.php?Id=$ticket");
?>

<html>
<head>

<link type="text/css" rel="stylesheet" href="style.css" />


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Elettric 80 Inc - Data Base</title>


<script language="JavaScript">
<!--
function refreshParent() {
  window.opener.location.href = window.opener.location.href;

  if (window.opener.progressWindow)
		
 {
    window.opener.progressWindow.close()
  }
  window.close();
}
//-->
</script>
</head>

<body onLoad=" refreshParent();" >
</body>
</html>
