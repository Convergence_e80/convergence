<!--%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

VERSION: 	

- 1.1

COMMENTS:	(add comment describing what has been changed)

-  
- 

DATE:

- 03/14/13

AUTHOR:

- Jose Hermida

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%-->

<?php
// Archivo en donde se acumular&aacute; el numero de visitas
$archivo = "contador.txt";

// Abrimos el archivo para solamente leerlo (r de read)
$abre = fopen($archivo, "r");

// Leemos el contenido del archivo
$total = fread($abre, filesize($archivo));

// Cerramos la conexi&oacute;n al archivo
fclose($abre);

// Abrimos nuevamente el archivo
$abre = fopen($archivo, "w");

// Sumamos 1 nueva visita
$total = $total + 1;

// Y reemplazamos por la nueva cantidad de visitas
$grabar = fwrite($abre, $total);

// Cerramos la conexi&oacute;n al archivo
fclose($abre);

echo "<font color = '#F5F5F5' >".$total."</font>";
?>