<!--%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

VERSION: 	

- 1.1

COMMENTS:	(add comment describing what has been changed)

-  
- 

DATE:

- 03/14/13

AUTHOR:

- Jose Hermida

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%-->

<?php
include("check_connection.php");	// import file for checking the session of the login

	  ini_set('display_errors','Off');

	  $value = 'cualquier cosa';
     setcookie("TestCookie", $value);
?>

<html>
<head>
<link href="favicon.ico" type="image/x-icon" rel="shortcut icon">
<link type="text/css" rel="stylesheet" href="style.css" />
<script type="text/javascript" src="jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="jquery.tablesorter.min.js"></script>

	<link rel="stylesheet" href="loading/css/modal-message.css" type="text/css">
	<script type="text/javascript" src="loading/js/ajax.js"></script>
	<script type="text/javascript" src="loading/js/modal-message.js"></script>
	<script type="text/javascript" src="loading/js/ajax-dynamic-content.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Elettric 80 Inc - Convergence</title>
<script language="JavaScript1.2" >
<!--

//for tablesorting
$(document).ready(function()     {         $("#myTable").tablesorter();     } ); 

//for mouse over effects
function cambiar_color_over(celda){ 
   celda.style.backgroundColor="#F9BF6B" 
} 
function cambiar_color_out(celda){ 
   celda.style.backgroundColor="#FFFFFF" 
}
//-->
</script>
<style type="text/css">
@import url("images/status_menu/status.css");


.style40 {font-size: 17px}
.style44 {font-family: Verdana, Arial, Helvetica, sans-serif; color: #000000;}
.style48 {font-size: 14px}
.style67 {font-size: 9px}
.style50 {font-weight: bold; color: #094FA4; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 15px; }
.style51 {font-size: 14px; font-weight: bold; }
.style68 {font-size: 11px}
.style69 {font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; }
.style5 {color: #000000}
.tb11 {background:#FFFFFF url(images/search.png) no-repeat 4px 4px;
	padding:4px 4px 4px 22px;
	border:1px solid #CCCCCC;
	width:150px;
	height:30px;
}
</style>
<script language="JavaScript1.2" type="text/javascript" src="images/status_menu/mm_css_menu.js"></script>

		 <script>
		 function window_function (bar,list) {
		 	list=document.getElementById(list); 
			bar=document.getElementById(bar);
			
			if(list.style.display=='none') {
				list.style.display='block';
				bar.src="images/status_close.jpg";
			} 
			
			else {
			list.style.display='none'; 
			bar.src='images/status_open.jpg';
			}
		 }
		 </script> 
		 
<script type="text/javascript">
messageObj = new DHTML_modalMessage();	// We only create one object of this class
messageObj.setShadowOffset(5);	// Large shadow

function displayStaticMessage(messageContent,cssClass)
{
	messageObj.setHtmlContent(messageContent);
	messageObj.setSize(1,1);
	messageObj.setCssClassMessageBox(cssClass);
	messageObj.setSource(true);	// no html source since we want to use a static message here.
	messageObj.setShadowDivVisible(true);	// Disable shadow for these boxes	
	messageObj.display();
	
	
}

function closeMessage()
{
	messageObj.close();	
}


</script>		 
</head>

<body>
<br />

<?php include_once('header.php');?>   



<table width="850" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="images/borde_arriba.jpg" width="850" height="20" /></td>
  </tr>
</table>
<table width="850" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="4"  background="images/borde_izq.jpg" background-repeat: repeat-y; >&nbsp;</td>
    <td width="842" bgcolor="#FFFFFF"><div align="center"><br />
	
	
	   <form id="form1" name="form1" method="get" action="ticket_filter.php">
	     <table width="835" border="0" cellspacing="0" cellpadding="0">
           <tr>
             <td width="605"><img src="images/tickets_top_big.jpg" width="583" height="34" border="0"></td>
             <td width="220"><table width="223" border="0" cellspacing="0" cellpadding="0">
                 <tr>
                   <td width="152"><input name="search_ticket" type="text" class="tb11" id="search_ticket" /></td>
                   <td width="10">&nbsp;</td>
                   <td width="61"><input type="image" src="images/go.jpg" name="Submit" value="Submit">                   </td>
                 </tr>
             </table></td>
             <td width="10">&nbsp;</td>
           </tr>
         </table>
	   </form>
		
		
      <table width="825" height="19" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="60" valign="top"><strong>Filter  by:</strong> </td>
          <td width="534"><div align="left">
              <table width="511" height="19" border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td width="150"><img src="images/filter_menu.jpg" width="150" height="11" border="0" usemap="#Map"></td>
                  <td width="97"><div id="FWTableContainer2078133118"> <img name="status" src="images/status_menu/status.jpg" width="153" height="11" border="0" id="status" usemap="#m_status" alt="" />
                        <map name="m_status" id="m_status">
                          <area shape="poly" coords="105,1,151,1,151,9,105,9,105,1" href="javascript:;" alt="" onMouseOut="MM_menuStartTimeout(1000);"  onMouseOver="MM_menuShowMenu('MMMenuContainer1030170646_0', 'MMMenu1030170646_0',106,13,'status');"  />
                          <area shape="poly" coords="52,2,92,2,92,10,52,10,52,2" href="javascript:;" alt="" onMouseOut="MM_menuStartTimeout(1000);"  onMouseOver="MM_menuShowMenu('MMMenuContainer0119100750_0', 'MMMenu0119100750_0',53,14,'status');"  />
                          <area shape="rect" coords="0,2,36,10" href="javascript:;" alt="" onMouseOut="MM_menuStartTimeout(1000);"  onMouseOver="MM_menuShowMenu('MMMenuContainer0119100736_1', 'MMMenu0119100736_1',1,14,'status');"  />
                        </map>
                        <div id="MMMenuContainer1030170646_0">
                          <div id="MMMenu1030170646_0" onMouseOut="MM_menuStartTimeout(1000);" onMouseOver="MM_menuResetTimeout();"> <a href="ticket_filter.php?Id=20" id="MMMenu1030170646_0_Item_0" class="MMMIFVStyleMMMenu1030170646_0" onMouseOver="MM_menuOverMenuItem('MMMenu1030170646_0');"> LGV </a> <a href="ticket_filter.php?Id=21" id="MMMenu1030170646_0_Item_1" class="MMMIVStyleMMMenu1030170646_0" onMouseOver="MM_menuOverMenuItem('MMMenu1030170646_0');"> PLC </a> <a href="ticket_filter.php?Id=22" id="MMMenu1030170646_0_Item_2" class="MMMIVStyleMMMenu1030170646_0" onMouseOver="MM_menuOverMenuItem('MMMenu1030170646_0');"> PC </a> <a href="ticket_filter.php?Id=23" id="MMMenu1030170646_0_Item_3" class="MMMIVStyleMMMenu1030170646_0" onMouseOver="MM_menuOverMenuItem('MMMenu1030170646_0');"> WMS </a> <a href="ticket_filter.php?Id=24" id="MMMenu1030170646_0_Item_4" class="MMMIVStyleMMMenu1030170646_0" onMouseOver="MM_menuOverMenuItem('MMMenu1030170646_0');"> BEMA </a> <a href="ticket_filter.php?Id=25" id="MMMenu1030170646_0_Item_5" class="MMMIVStyleMMMenu1030170646_0" onMouseOver="MM_menuOverMenuItem('MMMenu1030170646_0');"> FIELD </a> <a href="ticket_filter.php?Id=26" id="MMMenu1030170646_0_Item_6" class="MMMIVStyleMMMenu1030170646_0" onMouseOver="MM_menuOverMenuItem('MMMenu1030170646_0');"> OTHER </a> </div>
                        </div>
                    <div id="MMMenuContainer0119100750_0">
                          <div id="MMMenu0119100750_0" onMouseOut="MM_menuStartTimeout(1000);" onMouseOver="MM_menuResetTimeout();"> <a href="ticket_filter.php?Id=13" id="MMMenu0119100750_0_Item_0" class="MMMIFVStyleMMMenu0119100750_0" onMouseOver="MM_menuOverMenuItem('MMMenu0119100750_0');"> High </a> <a href="ticket_filter.php?Id=14" id="MMMenu0119100750_0_Item_1" class="MMMIVStyleMMMenu0119100750_0" onMouseOver="MM_menuOverMenuItem('MMMenu0119100750_0');"> Normal </a> <a href="ticket_filter.php?Id=15" id="MMMenu0119100750_0_Item_2" class="MMMIVStyleMMMenu0119100750_0" onMouseOver="MM_menuOverMenuItem('MMMenu0119100750_0');"> Low </a> </div>
                    </div>
                    <div id="MMMenuContainer0119100736_1">
                          <div id="MMMenu0119100736_1" onMouseOut="MM_menuStartTimeout(1000);" onMouseOver="MM_menuResetTimeout();"> <a href="ticket_filter.php?Id=1" id="MMMenu0119100736_1_Item_0" class="MMMIFVStyleMMMenu0119100736_1" onMouseOver="MM_menuOverMenuItem('MMMenu0119100736_1');"> New </a> <a href="ticket_filter.php?Id=2" id="MMMenu0119100736_1_Item_1" class="MMMIVStyleMMMenu0119100736_1" onMouseOver="MM_menuOverMenuItem('MMMenu0119100736_1');"> In&nbsp;Progress </a> <a href="ticket_filter.php?Id=3" id="MMMenu0119100736_1_Item_2" class="MMMIVStyleMMMenu0119100736_1" onMouseOver="MM_menuOverMenuItem('MMMenu0119100736_1');"> Waiting&nbsp;Customer&nbsp;Feedback&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </a> <a href="ticket_filter.php?Id=4" id="MMMenu0119100736_1_Item_3" class="MMMIVStyleMMMenu0119100736_1" onMouseOver="MM_menuOverMenuItem('MMMenu0119100736_1');"> Waiting&nbsp;For&nbsp;Parts&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </a> <a href="ticket_filter.php?Id=5" id="MMMenu0119100736_1_Item_4" class="MMMIVStyleMMMenu0119100736_1" onMouseOver="MM_menuOverMenuItem('MMMenu0119100736_1');"> Customer&nbsp;Support&nbsp;Request&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </a> <a href="ticket_filter.php?Id=6" id="MMMenu0119100736_1_Item_5" class="MMMIVStyleMMMenu0119100736_1" onMouseOver="MM_menuOverMenuItem('MMMenu0119100736_1');"> Visit&nbsp;Required&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </a> <a href="ticket_filter.php?Id=7" id="MMMenu0119100736_1_Item_6" class="MMMIVStyleMMMenu0119100736_1" onMouseOver="MM_menuOverMenuItem('MMMenu0119100736_1');"> Solved </a> </div>
                    </div>
                  </div></td>
                  <td width="15">&nbsp;</td>
                  <td width="219">&nbsp;</td>
                  <td width="30">&nbsp;</td>
                </tr>
              </table>
          </div></td>
          <td width="231"><div class="add_voice">
              <div align="left">
                <table width="139" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="22"><img src="images/add_ticket.jpg" width="22" height="21" border="0"></td>
                    <td width="5">&nbsp;</td>
                    <td width="107"><a href="add_ticket.php">Add Ticket</a></td>
                  </tr>
                </table>
              </div>
          </div></td>
        </tr>
      </table>
      <map name="Map">
        <area shape="rect" coords="80,0,139,12" href="tickets.php?customer=all&assignee=all&division=all&status=all&Submit.x=0&Submit.y=0"  onClick="displayStaticMessage('')" >
        <area shape="rect" coords="2,1,70,11" href="ticket_filter.php?Id=my">
      </map>
      <br>
      <table width="830" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="325"><table width="315" height="192" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td width="43" bgcolor="#FFB64D"><span class="style44"><img src="images/status_open.jpg" width="40" height="48" id="1_bar" onClick="window_function('1_bar','1_list');"/></span></td>
              <td width="194" background="images/ticket_assigned_fondo.jpg"><span class="style48"><span class="style50">
 
			  <?php 
				$sql_quotes = "select   * from Tickets WHERE Id_Assignee = $row[0] and Deleted_Ticket is null ";
				$result_quotes = sqlsrv_query( $conn, $sql_quotes, array(), array( "Scrollable" => 'static' ) );
				$row_quotes = sqlsrv_num_rows($result_quotes);	
				echo $row_quotes;  
		  ?>
              </span></span></td>
            </tr>
            <tr>
              <td bgcolor="#FFB64D"><span class="style44"><img src="images/status_open.jpg" width="40" height="48" id="2_bar" onClick="window_function('2_bar','2_list');"/></span></td>
              <td background="images/ticket_issued_fondo.jpg"><span class="style50"><?php 
				$sql_quotes = "select   * from Tickets WHERE Creator = $row[0] and Deleted_Ticket is null";
				$result_quotes = sqlsrv_query( $conn, $sql_quotes, array(), array( "Scrollable" => 'static' ) );
				$row_quotes = sqlsrv_num_rows($result_quotes);	
				echo $row_quotes;  
		  ?></span></td>
            </tr>
            <tr>
              <td bgcolor="#FFB64D"><span class="style44"><img src="images/status_open.jpg" width="40" height="48" id="3_bar" onClick="window_function('3_bar','3_list');"/></span></td>
              <td background="images/ticket_commented_fondo.jpg"><span class="style50"><?php 
				$sql_post = "select   * from Posts WHERE Author = $row[0] ";
				$result_post = sqlsrv_query( $conn, $sql_post, array(), array( "Scrollable" => 'static' ) );
				$row_post = sqlsrv_num_rows($result_post);	
				echo $row_post;  
		  ?>
              </span></td>
            </tr>
            <tr>
              <td bgcolor="#FFB64D"><span class="style44"><img src="images/status_open.jpg" width="40" height="48" id="4_bar" onClick="window_function('4_bar','4_list');"/></span></td>
              <td background="images/unread_post _fondo.jpg"><span class="style50"><?php 
				$sql_post_Uptated = "select   * from Posts_Update WHERE Author = $row[0] ";
				$result_post_Uptated = sqlsrv_query( $conn, $sql_post_Uptated, array(), array( "Scrollable" => 'static' ) );
				$row_post_Uptated = sqlsrv_num_rows($result_post_Uptated);	
				echo $row_post_Uptated;  
				$Id = $row[0];
		  ?>
              </span></td>
            </tr>

          </table></td>
          <td width="10"></td>
          <td width="501" valign="top"><table width="382" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="381" height="169" valign="top">
                <div align="left">
                  <table width="282" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="17">
                        <div align="center">
                          <?php
// archivos incluidos. Librer&iacute;as PHP para poder graficar.
include "statics/FusionCharts.php";
include "statics/Functions.php";
// Gr&aacute;fico de Barras. 4 Variables, 4 barras.
// Estas variables ser&aacute;n usadas para representar los valores de cada unas de las 4 barras.
// Inicializo las variables a utilizar.




//BEGIN__Ticket query by Status -- NEW -- 
            $Id = $row[0];
			
			//echo "Es",$row[0];

            if (isset($Id)) {
				$sql_new = "SELECT * from Tickets WHERE Status = '1' and Id_Assignee = $Id and Deleted_Ticket is null ";
				//print_r ($sql_new);
				}else
				
				{

				$sql_new = "SELECT * from Tickets WHERE Status = '1'  and Deleted_Ticket is null";
				}


$result_new = sqlsrv_query( $conn, $sql_new, array(), array( "Scrollable" => 'static' ) );
$row_new = sqlsrv_num_rows($result_new);

//END__Ticket query by Status -- NEW -- 



//BEGIN__Ticket query by Status -- IN PROGRESS -- 

			   if (isset($Id)) {
				$sql_progress = "SELECT * from Tickets WHERE Status = '2' and Id_Assignee = $Id and Deleted_Ticket is null";
				}else
				
				{
				$sql_progress = "SELECT * from Tickets WHERE Status = '2' and Deleted_Ticket is null ";
				}

$result_sql_progress = sqlsrv_query( $conn, $sql_progress, array(), array( "Scrollable" => 'static' ) );
$row_sql_progress = sqlsrv_num_rows($result_sql_progress);

//END__Ticket query by Status -- IN PROGRESS --


//BEGIN__Ticket query by Status -- WAITING FEEDBACK --

			   if (isset($Id)) {
				$sql_w_feedback = "SELECT * from Tickets WHERE Status = '3' and Id_Assignee = $Id and Deleted_Ticket is null ";
				}else
				
				{
				$sql_w_feedback = "SELECT * from Tickets WHERE Status = '3' and Deleted_Ticket is null ";
				}

$result_sql_w_feedback = sqlsrv_query( $conn, $sql_w_feedback, array(), array( "Scrollable" => 'static' ) );
$row_w_feedback = sqlsrv_num_rows($result_sql_w_feedback);

//END__Ticket query by Status -- WAITING FEEDBACK --

//BEGIN__Ticket query by Status -- WAITING FOR PARTS --

			   if (isset($Id)) {
				$sql_wfp = "SELECT * from Tickets WHERE Status = '4' and Id_Assignee = $Id and Deleted_Ticket is null ";
				}else
				
				{

				$sql_wfp = "SELECT * from Tickets WHERE Status = '4' and Deleted_Ticket is null ";
				}

$result_wfp = sqlsrv_query( $conn, $sql_wfp, array(), array( "Scrollable" => 'static' ) );
$row_wfp = sqlsrv_num_rows($result_wfp);

//END__Ticket query by Status -- WAITING FOR PARTS --



//BEGIN__Ticket query by Status -- CUSTOMER SUPPORT REQUEST --

			   if (isset($Id)) {
				$sql_cs_request = "SELECT * from Tickets WHERE Status = '5' and Id_Assignee = $Id and Deleted_Ticket is null ";
				}else
				
				{
				$sql_cs_request = "SELECT * from Tickets WHERE Status = '5' and Deleted_Ticket is null  ";
				}

$result_cs_request = sqlsrv_query( $conn, $sql_cs_request, array(), array( "Scrollable" => 'static' ) );
$row_cs_request = sqlsrv_num_rows($result_cs_request);

//END__Ticket query by Status -- CUSTOMER SUPPORT REQUEST --


//BEGIN__Ticket query by Status -- VISIT REQUIRED --

			   if (isset($Id)) {
				$sql_v_required = "SELECT * from Tickets WHERE Status = '6' and Id_Assignee = $Id  and Deleted_Ticket is null";
				}else
				
				{
				$sql_v_required = "SELECT * from Tickets WHERE Status = '6'  and Deleted_Ticket is null";
				}

$result_v_required = sqlsrv_query( $conn, $sql_v_required, array(), array( "Scrollable" => 'static' ) );
$row_v_required = sqlsrv_num_rows($result_v_required);

//END__Ticket query by Status -- VISIT REQUIRED --


//BEGIN__Ticket query by Status -- SOLVED --

			   if (isset($Id)) {
				$sql_solved = "SELECT * from Tickets WHERE Status = '7' and Id_Assignee = $Id and Deleted_Ticket is null ";
				}else
				
				{
				$sql_solved = "SELECT * from Tickets WHERE Status = '7' and Deleted_Ticket is null ";
				}

$result_solved = sqlsrv_query( $conn, $sql_solved, array(), array( "Scrollable" => 'static' ) );
$row_solved = sqlsrv_num_rows($result_solved);

//END__Ticket query by Status -- SOLVED --


$status_new = $row_new;
$status_sql_progress = $row_sql_progress;
$status_w_feedback = $row_w_feedback;
$status_wfp = $row_wfp;
$status_cs_request = $row_cs_request;
$status_v_required = $row_v_required;
$status_solved = $row_solved;

$strXML = "";

$strXML = "<chart caption = 'Ticket statistics assigned to $row[1] $row[13] ' bgColor='#FFFFFF' baseFontSize='12' showValues='1' xAxisName='' >";

$strXML .= "<set label = '' value ='".$status_new."'color = 'EA1000' /> ";
$strXML .= "<set label = '' value ='".$status_sql_progress."' color = '6D8D16' />";
$strXML .= "<set label = '' value ='".$status_w_feedback."' color = 'FFBA00' />";
$strXML .= "<set label = '' value ='".$status_wfp."'color = 'AA2000' /> ";
$strXML .= "<set label = '' value ='".$status_cs_request."' color = '6A1F16' />";
$strXML .= "<set label = '' value ='".$status_v_required."' color = '1CBF10' />";
$strXML .= "<set label = '' value ='".$status_solved."' color = 'AAAA00' />";
$strXML .= "</chart>";

echo renderChartHTML("statics/Column3D.swf", "Pie3D",$strXML, "Pie3D", 300, 210, false);


?>
                          </div></td>
                          <td width="8"><div align="right"></div></td>
                          <td width="201"><div align="center">
                              <table width="200" height="152" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr>
                                  <td width="174"><div align="right"><a href="ticket_filter.php?Id=30&Id_customer=<?php echo $Id ?>">New ( <?php echo $status_new; ?> ) </a></div></td>
                                </tr>
                                <tr>
                                  <td><div align="right"><span class="style68"><a href="ticket_filter.php?Id=31&Id_customer=<?php echo $Id ?>">In Progress ( <?php echo $status_sql_progress; ?> ) </a></span></div></td>
                                </tr>
                                <tr>
                                  <td><div align="right"><span class="style68"><a href="ticket_filter.php?Id=32&Id_customer=<?php echo $Id ?>">Waiting Customer Feedback ( <?php echo $status_w_feedback; ?> ) </a></span></div></td>
                                </tr>
                                <tr>
                                  <td><div align="right"><span class="style67"><span class="style69"><a href="ticket_filter.php?Id=33&Id_customer=<?php echo $Id ?>">Waiting For Parts ( <?php echo $status_wfp; ?> ) </a></span></span></div></td>
                                </tr>
                                <tr>
                                  <td><div align="right"><span class="style67"><span class="style69"><a href="ticket_filter.php?Id=34&Id_customer=<?php echo $Id ?>">Visit Required ( <?php echo $status_cs_request; ?> ) </a></span></span></div></td>
                                </tr>
                                <tr>
                                  <td><div align="right"><span class="style68"><a href="ticket_filter.php?Id=35&Id_customer=<?php echo $Id ?>">Solved ( <?php echo $status_v_required; ?> ) </a></span></div></td>
                                </tr>
                                <tr>
                                  <td><div align="right"><a href="ticket_filter.php?Id=36&Id_customer=<?php echo $Id ?>">Closed ( <?php echo $status_solved; ?> ) </a></div></td>
                                </tr>
                                </table>
                              </div>                          </td>
                          <td width="56"><img src="images/statistis_bar.jpg" width="25" height="145"></td>
                        </tr>
                  </table>
                </div></td><td width="12">&nbsp;</td>
              </tr>
          </table></td>
        </tr>
      </table>
      <table width="841" height="30" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td><div align="center" class="style40"></div></td>
        </tr>
      </table>
    </div>
	  
    <div id="1_list" name="1_list" style="display:none;">
	    <div align="center" class="style51">Tickets Assigned </div>
	    <table width="98%" height="15" align="center" cellpadding="0" cellspacing="0"  id="myTable" class="tablesorter">
        <thead> 
			<tr>
				<th width="78" class="voice" align="left"><a href="ticket.php?Id=<?php echo $row_sql_tickets[0]; ?>"></a>Ticket N. </th>
				<th width="189" class="voice" align="left">Summary</th>
				<th width="71" class="voice" align="left">Status</th>
				<th width="71" class="voice" align="left">Priority</th>
				<th width="115" class="voice" align="left">Assignee to </th>				
				<th width="141" class="voice" align="left">Customer</th>
				<th width="76" class="voice" align="left">Division</th>
			    <th width="99" class="voice" align="left">Updated</th>
			</tr>
		</thead> 
		<tbody>
		
		<?php 			
	    $filter_id = $_GET[Id];
	    $filter_search_ticket = $_GET[search_ticket];		
		//echo $filter_search_ticket;

		       if ( isset($filter_search_ticket)) 	{ 

		            $sql_tickets = "select TOP 10 * from Tickets WHERE Ticket_Title  LIKE '%$filter_search_ticket%'  OR Ticket_Post LIKE '%$filter_search_ticket%' and Deleted_Ticket is null ";
					//echo "aca";
					
					} else {

					if ($filter_id == "my") 	{ 
					
						$sql_tickets = "select TOP 10 * from Tickets WHERE Id_Assignee = $row[0] and Deleted_Ticket is null ORDER BY Id DESC ";
						//echo "aca";
						} else {
						
						$Id_cust = $_GET[Id_customer];
						if ($filter_id == 1) { 
						    
							
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 1 and Id_Customer = $Id_cust and Deleted_Ticket is null";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 1  and Deleted_Ticket is null ";
				              }
						    }
						   
						if ($filter_id == 2) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 2 and Id_Customer = $Id_cust and Deleted_Ticket is null ";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 2 and Deleted_Ticket is null ";
				              }
						    }
							
						
						   
						if ($filter_id == 3) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 3 and Id_Customer = $Id_cust and Deleted_Ticket is null ";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 3  and Deleted_Ticket is null ";
				              }
						    }


						if ($filter_id == 4) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 4 and Id_Customer = $Id_cust and Deleted_Ticket is null ";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 4 and Deleted_Ticket is null ";
				              }
						    }
						if ($filter_id == 5) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 5 and Id_Customer = $Id_cust and Deleted_Ticket is null";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 5  and Deleted_Ticket is null ";
				              }
						    }							

						if ($filter_id == 6) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 6 and Id_Customer = $Id_cust ";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 6 and Deleted_Ticket is null ";
				              }
						    }



						if ($filter_id == 7) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 7 and Id_Customer = $Id_cust and Deleted_Ticket is null ";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 7 and Deleted_Ticket is null  ";
				              }
						    }
							
							
						if ($filter_id == 16) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Id_Customer = $Id_cust and Deleted_Ticket is null ";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 3 and Deleted_Ticket is null ";
				              }
						    }							


						if ($filter_id == 8) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 1 and Id_Customer = $Id_cust and Deleted_Ticket is null ";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 1 and Deleted_Ticket is null ";
				              }
						    }

						if ($filter_id == 9) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 2 and Id_Customer = $Id_cust and Deleted_Ticket is null";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 2 and Deleted_Ticket is null ";
				              }
						    }


						if ($filter_id == 10) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 3 and Id_Customer = $Id_cust  and Deleted_Ticket is null";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 3 and Deleted_Ticket is null ";
				              }
						    }
						if ($filter_id == 11) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 4 and Id_Customer = $Id_cust and Deleted_Ticket is null ";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 4  and Deleted_Ticket is null";
				              }
						    }							

						if ($filter_id == 12) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 5 and Id_Customer = $Id_cust  and Deleted_Ticket is null";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 5 and Deleted_Ticket is null ";
				              }
						    }
							
						if ($filter_id == 13) { 
							$sql_tickets = "select TOP 10 * from Tickets WHERE Priority = 1 and Deleted_Ticket is null";
						  }
						if ($filter_id == 14) { 
							$sql_tickets = "select TOP 10 * from Tickets WHERE Priority = 2 and Deleted_Ticket is null";
						   }
						if ($filter_id == 15) { 
							$sql_tickets = "select TOP 10 * from Tickets WHERE Priority = 3 and Deleted_Ticket is null";
						   }	
	
	
							if ($filter_id == 20) { 
							$sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 1 and Deleted_Ticket is null";
						   }						   
							if ($filter_id == 21) { 
							$sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 2 and Deleted_Ticket is null";
						   }						   
							if ($filter_id == 22) { 
							$sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 3 and Deleted_Ticket is null";
						   }
							if ($filter_id == 23) { 
							$sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 4 and Deleted_Ticket is null";
						   }						   
							if ($filter_id == 24) { 
							$sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 5 and Deleted_Ticket is null";
						   }						   
							if ($filter_id == 25) { 
							$sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 6 and Deleted_Ticket is null";
						   }						   
							if ($filter_id == 26) { 
							$sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 7 and Deleted_Ticket is null";
						   }
						   
						   						   						   	   
						 }  



						} 		   
						
						
						$result_sql_tickets = sqlsrv_query( $conn, $sql_tickets); 
						while($row_sql_tickets = sqlsrv_fetch_array($result_sql_tickets)) { 
		
		?>
		
			<tr bgcolor="#FFFFFF" onMouseOver="cambiar_color_over(this)" onMouseOut="cambiar_color_out(this)">
				
			
				
			  <td valign="middle">
			  <?php 
										$sql_quotes = "SELECT TOP 10 * FROM Posts_Update where Id_Ticket = $row_sql_tickets[0] AND Author = '$snom' ";
										$result_quotes = sqlsrv_query( $conn, $sql_quotes, array(), array( "Scrollable" => 'static' ) );
										$row_quotes = sqlsrv_num_rows($result_quotes);		
										If ($row_quotes > 0 ) { ?>
                <a href="ticket.php?Id=<?php echo $row_sql_tickets[0]; ?>"> <img src="images/new.jpg" width="63" height="20" border="0"> </a>
                <?php } ?>
                <?php echo "# ".$row_sql_tickets[0]; ?><a href="ticket.php?Id=<?php echo $row_sql_tickets[0]; ?>"></a> </td>
			  <td>
				
				
					<?php
					$sql_assignee = "select TOP 10 * from Employees where Id='$row_sql_tickets[3]'";
					$result_sql_assignee = sqlsrv_query( $conn, $sql_assignee); 
					$row_sql_assignee = sqlsrv_fetch_array($result_sql_assignee); 
					?>
					<div class="voice " >   <a href="ticket.php?Id=<?php echo $row_sql_tickets[0]; ?>"><?php echo $row_sql_tickets[1]; ?> </a></div>
					<div class="description"><br>Reported by
					
					<a href="employee.php?Id=<?php  echo $row_sql_assignee[0];?>"> <?php echo $row_sql_assignee[1]." ".$row_sql_assignee[13]; ?> </a>

					
					the
					<?php echo date_format($row_sql_tickets[9], 'm/d/Y').""; ?> </div>				</td>
				<td>
					<?php
					$sql_status = "select TOP 10 * from Ticket_Status where Id='$row_sql_tickets[4]'";
					$result_sql_status = sqlsrv_query( $conn, $sql_status); 
					$row_sql_status = sqlsrv_fetch_array($result_sql_status); 
					
					echo $row_sql_status[1]; 
					?>				</td>
				<td>
					<?php 
					$sql_priority = "select TOP 10 * from Priority where Id='$row_sql_tickets[5]'";
					$result_sql_priority = sqlsrv_query( $conn, $sql_priority); 
					$row_sql_priority = sqlsrv_fetch_array($result_sql_priority); 
					
					echo $row_sql_priority[1]; 
					?>				</td>	
				<td>
					<?php 
					$sql_assignee = "select TOP 10 * from Employees where Id='$row_sql_tickets[7]'";
					$result_sql_assignee = sqlsrv_query( $conn, $sql_assignee); 
					$row_sql_assignee = sqlsrv_fetch_array($result_sql_assignee); ?>
					
					
					<a href="employee.php?Id=<?php  echo $row_sql_assignee[0];?>" > <?php echo $row_sql_assignee[1]." ".$row_sql_assignee[13]; ?> </a>			  </td>
				<td>
					<?php 
					$sql_cust = "select TOP 10 * from Customers where Id='$row_sql_tickets[8]'";
					$result_sql_cust = sqlsrv_query( $conn, $sql_cust); 
					$row_sql_cust = sqlsrv_fetch_array($result_sql_cust); 
					?>
				
					<?php
					echo $row_sql_cust[1]; 
					?>				</td>
				
				<td><?php 
					$sql_div = "select TOP 10 * from System_Type where Id='$row_sql_tickets[14]'";
					$result_sql_div = sqlsrv_query( $conn, $sql_div); 
					$row_sql_div = sqlsrv_fetch_array($result_sql_div); 
					?>
                 
                  <?php
					echo $row_sql_div[1]; 
					?></td>
			    <td><?php //echo date_format($row_sql_tickets[9], 'm/d/Y').""; ?>
                  <?php  echo date_format($row_sql_tickets[10], 'm/d/Y')."";  ?></td>
			</tr>
		
		<?php
		}
		?>	
		</tbody>
	  </table>
	</div>
	

	  <div align="center" class="style40 style40"></div>
	 
	  <div id="2_list" name="2_list" style="display:none;">
	  <div align="center" class="style51">Tickets Issued </div>
	  <table width="98%" height="15" align="center" cellpadding="0" cellspacing="0"  id="myTable" class="tablesorter">
        <thead>
          <tr>
            <th width="78" class="voice" align="left"><a href="ticket.php?Id=<?php echo $row_sql_tickets[0]; ?>"></a>Ticket N. </th>
            <th width="189" class="voice" align="left">Summary</th>
            <th width="71" class="voice" align="left">Status</th>
            <th width="71" class="voice" align="left">Priority</th>
            <th width="115" class="voice" align="left">Assignee to </th>
            <th width="141" class="voice" align="left">Customer</th>
            <th width="76" class="voice" align="left">Division</th>
            <th width="99" class="voice" align="left">Updated</th>
          </tr>
        </thead>
        <tbody>
          <?php 			
	    $filter_id = $_GET[Id];
	    $filter_search_ticket = $_GET[search_ticket];		
		//echo $filter_search_ticket;

		       if ( isset($filter_search_ticket)) 	{ 

		            $sql_tickets = "select TOP 10 * from Tickets WHERE Ticket_Title  LIKE '%$filter_search_ticket%'  OR Ticket_Post LIKE '%$filter_search_ticket%' and Deleted_Ticket is null";
					//echo "aca";
					
					} else {

					if ($filter_id == "my") 	{ 
					
						$sql_tickets = "select TOP 10 * from Tickets WHERE Creator = $row[0] and Deleted_Ticket is null ORDER BY Id DESC ";
						//echo "aca";
						} else {
						
						$Id_cust = $_GET[Id_customer];
						if ($filter_id == 1) { 
						    
							
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 1 and Id_Customer = $Id_cust and Deleted_Ticket is null ";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 1 and Deleted_Ticket is null ";
				              }
						    }
						   
						if ($filter_id == 2) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 2 and Id_Customer = $Id_cust and Deleted_Ticket is null ";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 2  and Deleted_Ticket is null";
				              }
						    }
							
						
						   
						if ($filter_id == 3) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 3 and Id_Customer = $Id_cust and Deleted_Ticket is null ";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 3 and Deleted_Ticket is null ";
				              }
						    }


						if ($filter_id == 4) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 4 and Id_Customer = $Id_cust and Deleted_Ticket is null";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 4 and Deleted_Ticket is null  ";
				              }
						    }
						if ($filter_id == 5) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 5 and Id_Customer = $Id_cust and Deleted_Ticket is null ";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 5 and Deleted_Ticket is null ";
				              }
						    }							

						if ($filter_id == 6) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 6 and Id_Customer = $Id_cust and Deleted_Ticket is null ";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 6  and Deleted_Ticket is null ";
				              }
						    }



						if ($filter_id == 7) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 7 and Id_Customer = $Id_cust and Deleted_Ticket is null ";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 7 and Deleted_Ticket is null ";
				              }
						    }
							
							
						if ($filter_id == 16) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Id_Customer = $Id_cust and Deleted_Ticket is null";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 3 and Deleted_Ticket is null ";
				              }
						    }							


						if ($filter_id == 8) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 1 and Id_Customer = $Id_cust and Deleted_Ticket is null";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 1 and Deleted_Ticket is null ";
				              }
						    }

						if ($filter_id == 9) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 2 and Id_Customer = $Id_cust and Deleted_Ticket is null ";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 2 and Deleted_Ticket is null ";
				              }
						    }


						if ($filter_id == 10) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 3 and Id_Customer = $Id_cust and Deleted_Ticket is null ";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 3  and Deleted_Ticket is null ";
				              }
						    }
						if ($filter_id == 11) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 4 and Id_Customer = $Id_cust and Deleted_Ticket is null ";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 4 and Deleted_Ticket is null  ";
				              }
						    }							

						if ($filter_id == 12) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select  TOP 10 * from Tickets WHERE Id_System = 5 and Id_Customer = $Id_cust and Deleted_Ticket is null";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 5  and Deleted_Ticket is null";
				              }
						    }
							
						if ($filter_id == 13) { 
							$sql_tickets = "select TOP 10 * from Tickets WHERE Priority = 1 and Deleted_Ticket is null";
						  }
						if ($filter_id == 14) { 
							$sql_tickets = "select TOP 10 * from Tickets WHERE Priority = 2 and Deleted_Ticket is null";
						   }
						if ($filter_id == 15) { 
							$sql_tickets = "select TOP 10 * from Tickets WHERE Priority = 3 and Deleted_Ticket is null";
						   }	
	
	
							if ($filter_id == 20) { 
							$sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 1 and Deleted_Ticket is null";
						   }						   
							if ($filter_id == 21) { 
							$sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 2 and Deleted_Ticket is null";
						   }						   
							if ($filter_id == 22) { 
							$sql_tickets = "select TOP 10 * from Tickets WHERE  Id_System = 3 and Deleted_Ticket is null";
						   }
							if ($filter_id == 23) { 
							$sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 4 and Deleted_Ticket is null";
						   }						   
							if ($filter_id == 24) { 
							$sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 5 and Deleted_Ticket is null";
						   }						   
							if ($filter_id == 25) { 
							$sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 6 and Deleted_Ticket is null";
						   }						   
							if ($filter_id == 26) { 
							$sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 7 and Deleted_Ticket is null";
						   }
						   
						   						   						   	   
						 }  



						} 		   
						
						
						$result_sql_tickets = sqlsrv_query( $conn, $sql_tickets); 
						while($row_sql_tickets = sqlsrv_fetch_array($result_sql_tickets)) { 
		
		?>
          <tr bgcolor="#FFFFFF" onMouseOver="cambiar_color_over(this)" onMouseOut="cambiar_color_out(this)">
            <td valign="middle"><?php 
										$sql_quotes = "SELECT TOP 10 * FROM Posts_Update where Id_Ticket = $row_sql_tickets[0] AND Author = '$snom' ";
										$result_quotes = sqlsrv_query( $conn, $sql_quotes, array(), array( "Scrollable" => 'static' ) );
										$row_quotes = sqlsrv_num_rows($result_quotes);		
										If ($row_quotes > 0 ) { ?>
                <a href="ticket.php?Id=<?php echo $row_sql_tickets[0]; ?>"> <img src="images/new.jpg" width="63" height="20" border="0"> </a>
                <?php } ?>
                <?php echo "# ".$row_sql_tickets[0]; ?><a href="ticket.php?Id=<?php echo $row_sql_tickets[0]; ?>"></a> </td>
            <td><?php
					$sql_assignee = "select TOP 10 * from Employees where Id='$row_sql_tickets[3]'";
					$result_sql_assignee = sqlsrv_query( $conn, $sql_assignee); 
					$row_sql_assignee = sqlsrv_fetch_array($result_sql_assignee); 
					?>
                <div class="voice " > <a href="ticket.php?Id=<?php echo $row_sql_tickets[0]; ?>"><?php echo $row_sql_tickets[1]; ?> </a></div>
              <div class="description"><br>
                Reported by <a href="employee.php?Id=<?php  echo $row_sql_assignee[0];?>"> <?php echo $row_sql_assignee[1]." ".$row_sql_assignee[13]; ?> </a> the <?php echo date_format($row_sql_tickets[9], 'm/d/Y').""; ?> </div></td>
            <td><?php
					$sql_status = "select TOP 10 * from Ticket_Status where Id='$row_sql_tickets[4]'";
					$result_sql_status = sqlsrv_query( $conn, $sql_status); 
					$row_sql_status = sqlsrv_fetch_array($result_sql_status); 
					
					echo $row_sql_status[1]; 
					?>            </td>
            <td><?php 
					$sql_priority = "select TOP 10 * from Priority where Id='$row_sql_tickets[5]'";
					$result_sql_priority = sqlsrv_query( $conn, $sql_priority); 
					$row_sql_priority = sqlsrv_fetch_array($result_sql_priority); 
					
					echo $row_sql_priority[1]; 
					?>            </td>
            <td><?php 
					$sql_assignee = "select TOP 10 * from Employees where Id='$row_sql_tickets[7]'";
					$result_sql_assignee = sqlsrv_query( $conn, $sql_assignee); 
					$row_sql_assignee = sqlsrv_fetch_array($result_sql_assignee); ?>
              <a href="employee.php?Id=<?php  echo $row_sql_assignee[0];?>" > <?php echo $row_sql_assignee[1]." ".$row_sql_assignee[13]; ?> </a> </td>
            <td><?php 
					$sql_cust = "select  TOP 10 * from Customers where Id='$row_sql_tickets[8]'";
					$result_sql_cust = sqlsrv_query( $conn, $sql_cust); 
					$row_sql_cust = sqlsrv_fetch_array($result_sql_cust); 
					?>
                <?php
					echo $row_sql_cust[1]; 
					?>            </td>
            <td><?php 
					$sql_div = "select TOP 10 * from System_Type where Id='$row_sql_tickets[14]'";
					$result_sql_div = sqlsrv_query( $conn, $sql_div); 
					$row_sql_div = sqlsrv_fetch_array($result_sql_div); 
					?>
                <?php
					echo $row_sql_div[1]; 
					?></td>
            <td><?php //echo date_format($row_sql_tickets[9], 'm/d/Y').""; ?>
                <?php  echo date_format($row_sql_tickets[10], 'm/d/Y')."";  ?></td>
          </tr>
          <?php
		}
		?>
        </tbody>
      </table>
	  </div>
	
	  <div align="center" class="style40"></div>
	
	   <div id="3_list" name="3_list" style="display:none;">
	  <div align="center" class="style51">Tickets Commented </div>
	  <table width="98%" height="15" align="center" cellpadding="0" cellspacing="0"  id="myTable" class="tablesorter">
        <thead>
          <tr>
            <th width="78" class="voice" align="left"><a href="ticket.php?Id=<?php echo $row_sql_tickets[0]; ?>"></a>Ticket N. </th>
            <th width="663" class="voice" align="left">Post</th>
            <th width="99" class="voice" align="left">Date</th>
          </tr>
        </thead>
        <tbody>
          <?php 			
	    $filter_id = $_GET[Id];
	    $filter_search_ticket = $_GET[search_ticket];		
		//echo $filter_search_ticket;

		       if ( isset($filter_search_ticket)) 	{ 

		            $sql_tickets = "select TOP 10 * from Tickets WHERE Ticket_Title  LIKE '%$filter_search_ticket%'  OR Ticket_Post LIKE '%$filter_search_ticket%' and Deleted_Ticket is null ";
					//echo "aca";
					
					} else {

					if ($filter_id == "my") 	{ 
					
						$sql_tickets = "select TOP 10 * from Posts WHERE Author = $row[0]  ORDER BY Id DESC ";
						//print_r($sql_tickets);
						//echo "aca";
						} else {
						
						$Id_cust = $_GET[Id_customer];
						if ($filter_id == 1) { 
						    
							
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 1 and Id_Customer = $Id_cust  and Deleted_Ticket is null";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 1 and Deleted_Ticket is null";
				              }
						    }
						   
						if ($filter_id == 2) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 2 and Id_Customer = $Id_cust and Deleted_Ticket is null ";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 2 and Deleted_Ticket is null ";
				              }
						    }
							
						
						   
						if ($filter_id == 3) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 3 and Id_Customer = $Id_cust and Deleted_Ticket is null ";
				              }else
				
				              {
				              $sql_tickets = "select  TOP 10 * from Tickets WHERE Status = 3 and Deleted_Ticket is null ";
				              }
						    }


						if ($filter_id == 4) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 4 and Id_Customer = $Id_cust and Deleted_Ticket is null ";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 4 and Deleted_Ticket is null  ";
				              }
						    }
						if ($filter_id == 5) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 5 and Id_Customer = $Id_cust and Deleted_Ticket is null ";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 5 and Deleted_Ticket is null ";
				              }
						    }							

						if ($filter_id == 6) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 6 and Id_Customer = $Id_cust and Deleted_Ticket is null ";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 6 and Deleted_Ticket is null ";
				              }
						    }



						if ($filter_id == 7) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 7 and Id_Customer = $Id_cust and Deleted_Ticket is null ";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 7 and Deleted_Ticket is null ";
				              }
						    }
							
							
						if ($filter_id == 16) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Id_Customer = $Id_cust and Deleted_Ticket is null";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 3  and Deleted_Ticket is null";
				              }
						    }							


						if ($filter_id == 8) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 1 and Id_Customer = $Id_cust and Deleted_Ticket is null";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 1 and Deleted_Ticket is null ";
				              } 
						    }

						if ($filter_id == 9) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select  TOP 10 * from Tickets WHERE Id_System = 2 and Id_Customer = $Id_cust and Deleted_Ticket is null ";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 2  and Deleted_Ticket is null ";
				              }
						    }


						if ($filter_id == 10) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 3 and Id_Customer = $Id_cust and Deleted_Ticket is null ";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 3  and Deleted_Ticket is null ";
				              }
						    }
						if ($filter_id == 11) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 4 and Id_Customer = $Id_cust and Deleted_Ticket is null ";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 4 and Deleted_Ticket is null ";
				              }
						    }							

						if ($filter_id == 12) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 5 and Id_Customer = $Id_cust and Deleted_Ticket is null";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 5  and Deleted_Ticket is null ";
				              }
						    }
							
						if ($filter_id == 13) { 
							$sql_tickets = "select TOP 10 * from Tickets WHERE Priority = 1 and Deleted_Ticket is null";
						  }
						if ($filter_id == 14) { 
							$sql_tickets = "select TOP 10 * from Tickets WHERE Priority = 2 and Deleted_Ticket is null";
						   }
						if ($filter_id == 15) { 
							$sql_tickets = "select TOP 10 * from Tickets WHERE Priority = 3 and Deleted_Ticket is null";
						   }	
	
	
							if ($filter_id == 20) { 
							$sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 1 and Deleted_Ticket is null";
						   }						   
							if ($filter_id == 21) { 
							$sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 2 and Deleted_Ticket is null";
						   }						   
							if ($filter_id == 22) { 
							$sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 3 and Deleted_Ticket is null";
						   }
							if ($filter_id == 23) { 
							$sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 4 and Deleted_Ticket is null";
						   }						   
							if ($filter_id == 24) { 
							$sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 5 and Deleted_Ticket is null";
						   }						   
							if ($filter_id == 25) { 
							$sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 6 and Deleted_Ticket is null";
						   }						   
							if ($filter_id == 26) { 
							$sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 7 and Deleted_Ticket is null";
						   }
						   
						   						   						   	   
						 }  



						} 		   
						
						
						$result_sql_tickets = sqlsrv_query( $conn, $sql_tickets); 
						while($row_sql_tickets = sqlsrv_fetch_array($result_sql_tickets)) { 
		
		?>
          <tr bgcolor="#FFFFFF" onMouseOver="cambiar_color_over(this)" onMouseOut="cambiar_color_out(this)">
            <td valign="middle">
               <a href="ticket.php?Id=<?php echo $row_sql_tickets[1]; ?>"> <?php echo "# ".$row_sql_tickets[1]; ?></a> </td>
            <td>
                <div > <?php echo nl2br($row_sql_tickets[2]); ?> </div>
              <div class="description"><br>
              </div></td>
            <td><?php //echo date_format($row_sql_tickets[9], 'm/d/Y').""; ?>
                <?php  echo date_format($row_sql_tickets[4], 'm/d/Y')."";  ?></td>
          </tr>
          <?php
		}
		?>
        </tbody>
      </table>
	 </div>
	  
	
	  <div align="center" class="style40"></div>
	
	  
	  <div id="4_list" name="4_list" style="display:none;">
	  <div align="center" class="style51">Unread Post </div>
	  <table width="98%" height="15" align="center" cellpadding="0" cellspacing="0"  id="myTable" class="tablesorter">
        <thead>
          <tr>
            <th width="78" class="voice" align="left"><a href="ticket.php?Id=<?php echo $row_sql_tickets[0]; ?>"></a>Ticket N. </th>
            <th width="663" class="voice" align="left">Post</th>
            <th width="99" class="voice" align="left">Date</th>
          </tr>
        </thead>
        <tbody>
          <?php 			
	    $filter_id = $_GET[Id];
	    $filter_search_ticket = $_GET[search_ticket];		
		//echo $filter_search_ticket;

		       if ( isset($filter_search_ticket)) 	{ 

		            $sql_tickets = "select TOP 10 * from Tickets WHERE Ticket_Title  LIKE '%$filter_search_ticket%'  OR Ticket_Post LIKE '%$filter_search_ticket%' and Deleted_Ticket is null";
					//echo "aca";
					
					} else {

					if ($filter_id == "my") 	{ 
					
						$sql_tickets = "select TOP 10 * from Posts_Update WHERE Author = $row[0]  ORDER BY Id_Ticket DESC ";
						 //echo $row[0]."aca";
						} else {
						
						$Id_cust = $_GET[Id_customer];
						if ($filter_id == 1) { 
						    
							
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 1 and Id_Customer = $Id_cust and Deleted_Ticket is null";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 1 and Deleted_Ticket is null ";
				              }
						    }
						   
						if ($filter_id == 2) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 2 and Id_Customer = $Id_cust and Deleted_Ticket is null ";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 2 and Deleted_Ticket is null ";
				              }
						    }
							
						
						   
						if ($filter_id == 3) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 3 and Id_Customer = $Id_cust and Deleted_Ticket is null ";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 3  and Deleted_Ticket is null";
				              }
						    }


						if ($filter_id == 4) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 4 and Id_Customer = $Id_cust and Deleted_Ticket is null ";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 4  and Deleted_Ticket is null";
				              }
						    }
						if ($filter_id == 5) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 5 and Id_Customer = $Id_cust and Deleted_Ticket is null";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 5 and Deleted_Ticket is null ";
				              }
						    }							

						if ($filter_id == 6) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 6 and Id_Customer = $Id_cust and Deleted_Ticket is null";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 6 and Deleted_Ticket is null ";
				              }
						    }



						if ($filter_id == 7) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 7 and Id_Customer = $Id_cust and Deleted_Ticket is null ";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 7 and Deleted_Ticket is null ";
				              }
						    }
							
							
						if ($filter_id == 16) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Id_Customer = $Id_cust and Deleted_Ticket is null ";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Status = 3 and Deleted_Ticket is null ";
				              }
						    }							


						if ($filter_id == 8) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 1 and Id_Customer = $Id_cust and Deleted_Ticket is null ";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 1 and Deleted_Ticket is null  ";
				              }
						    }

						if ($filter_id == 9) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 2 and Id_Customer = $Id_cust and Deleted_Ticket is null ";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 2 and Deleted_Ticket is null ";
				              }
						    }


						if ($filter_id == 10) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 3 and Id_Customer = $Id_cust and Deleted_Ticket is null ";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 3   and Deleted_Ticket is null";
				              }
						    }
						if ($filter_id == 11) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 4 and Id_Customer = $Id_cust and Deleted_Ticket is null ";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 4 and Deleted_Ticket is null  ";
				              }
						    }							

						if ($filter_id == 12) { 
						
						     if (isset($Id_cust)) {
				                 $sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 5 and Id_Customer = $Id_cust and Deleted_Ticket is null ";
				              }else
				
				              {
				              $sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 5 and Deleted_Ticket is null  ";
				              }
						    }
							
						if ($filter_id == 13) { 
							$sql_tickets = "select TOP 10 * from Tickets WHERE Priority = 1 and Deleted_Ticket is null";
						  }
						if ($filter_id == 14) { 
							$sql_tickets = "select TOP 10 * from Tickets WHERE Priority = 2 and Deleted_Ticket is null";
						   }
						if ($filter_id == 15) { 
							$sql_tickets = "select TOP 10 * from Tickets WHERE Priority = 3 and Deleted_Ticket is null";
						   }	
	
	
							if ($filter_id == 20) { 
							$sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 1 and Deleted_Ticket is null";
						   }						   
							if ($filter_id == 21) { 
							$sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 2 and Deleted_Ticket is null";
						   }						   
							if ($filter_id == 22) { 
							$sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 3 and Deleted_Ticket is null ";
						   }
							if ($filter_id == 23) { 
							$sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 4 and Deleted_Ticket is null";
						   }						   
							if ($filter_id == 24) { 
							$sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 5 and Deleted_Ticket is null";
						   }						   
							if ($filter_id == 25) { 
							$sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 6 and Deleted_Ticket is null ";
						   }						   
							if ($filter_id == 26) { 
							$sql_tickets = "select TOP 10 * from Tickets WHERE Id_System = 7 and Deleted_Ticket is null";
						   }
						   
						   						   						   	   
						 }  



						} 		   
						
						
						$result_sql_tickets = sqlsrv_query( $conn, $sql_tickets); 
						while($row_sql_tickets = sqlsrv_fetch_array($result_sql_tickets)) { 
		
		?>
          <tr bgcolor="#FFFFFF" onMouseOver="cambiar_color_over(this)" onMouseOut="cambiar_color_out(this)">
            <td valign="middle">
               <a href="ticket.php?Id=<?php echo $row_sql_tickets[0]; ?>"> <?php echo "# ".$row_sql_tickets[0]; ?></a> </td>
            <td>
                <div > <?php echo $row_sql_tickets[1]; ?> </div>
              <div class="description"><br>
              </div></td>
            <td><?php //echo date_format($row_sql_tickets[9], 'm/d/Y').""; ?>
                <?php  echo date_format($row_sql_tickets[3], 'm/d/Y')."";  ?></td>
          </tr>
          <?php
		}
		?>
        </tbody>
      </table>
	  </div>
	  
	  <div align="center"><br>
	  </div></td>
    <td width="4" background="images/borde_der.jpg" background-repeat: repeat-y; ></td>
  </tr>
</table>
<table width="850" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="images/borde_abajo.jpg" width="850" height="20" /></td>
  </tr>
</table>
<br />

<map name="Map">
<area shape="rect" coords="80,0,139,12" href="tickets.php">
<area shape="rect" coords="2,2,70,12" href="ticket_filter.php?Id=my">
</map>


<table width="850" height="22" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="951" align="center"></td>
  </tr>
  <tr>
    <td align="right" valign="middle"  ><div align="right" >
      <div align="center" >&copy; Elettric 80 Inc Convergence |  <a href="report_bug.php" class="style5"> Bug Report</a><br />
            <br />
      </div>
    </div></td>
  </tr>
</table>
</body>
</html>
