/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 4000 
	   --slt.[Dbf_s_Name]
	   [Dbf_l_Id_StockUnit]
      ,sk.Dbf_s_Name sku
      ,su.Dbf_s_LPN
      --,[Dbf_l_Id_OldStoragePosition]
      --,[Dbf_l_Id_StoragePosition]
      --,[Dbf_dt_MoveTime]
      --,[Dbf_l_ReasonCode]
      ,[Dbf_s_Notes]
      ,[Dbf_f_Heigth]
      ,sl2.Dbf_s_Name as oldPosition
      ,sp2.[Dbf_l_Level] as oldLevel
      ,sl1.Dbf_s_Name as newPosition
      ,sp1.[Dbf_l_Level] as newLevel
     
      --,[Dbf_f_DistanceFromEnd]
  FROM [WMS_CC819_PROD].[Storage].[Tbl_StockUnitMovement]
  
  join storage.[Tbl_StoragePosition] sp1 ON sp1.[Dbf_l_Id]=[Dbf_l_Id_StoragePosition]
  join storage.[Tbl_StoragePosition] sp2 ON sp2.[Dbf_l_Id]=[Dbf_l_Id_OldStoragePosition]
  join storage.Tbl_StorageLocation   sl1 ON sl1.[Dbf_l_Id]=sp1.[Dbf_l_Id_StorageLocation]
  join storage.Tbl_StorageLocation   sl2 ON sl2.[Dbf_l_Id]=sp2.[Dbf_l_Id_StorageLocation]
  --join Storage.Tbl_StorageLocationType slt on slt.Dbf_l_Id=sl1.Dbf_l_Id_LocationType
  JOIN [Inventory].[Tbl_StockUnit]   su ON su.Dbf_l_Id=[Dbf_l_Id_StockUnit] --------------to get LPN
  JOIN [Inventory].[Tbl_SKU]		 sk	ON su.[Dbf_l_Id_SKU]=sk.Dbf_l_Id
  
    where (sk.[Dbf_s_Name]='15400/17' or sk.[Dbf_s_Name]='15420/17')
    and sl2.Dbf_l_Id_LocationType in (4,15) -- Dbf_s_Name like '%st%'
    and sp2.[Dbf_l_Level]>1