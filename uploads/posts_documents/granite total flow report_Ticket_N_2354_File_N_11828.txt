declare @StartDate		datetime = '2014-06-27' --null
declare @EndDate		datetime = '2014-06-28' --null
declare @LgvId			int = 0
declare @GroupType		nvarchar(10) = 'hour' --null -- possible value TOT / LGV / DAY / HOUR
declare @ReportName		nvarchar(50) = 'TotalFlow'


DECLARE @tblGroupStation TABLE		
(
	PickupGroupId bigint, 
	DropGroupId bigint ,
	FieldName   nvarchar(255)
	,OutputType nvarchar(100)
	,Id bigint 
	,ShowOrder bit			
)

DECLARE @ReportData TABLE
(
	FieldName nvarchar(255),
	--,FieldLabel nvarchar(255),
	FieldValue nvarchar(255),
	IntervalString nvarchar(255)--,	
	--OutputType nvarchar(255),
	--ChartDisplay bit,
	--ShowOrder int				
)

DECLARE @tmpContractualFlow TABLE (
	PickupGroupId int,
	DropGroupId int,
	IntervalString nvarchar (255),
	Value decimal(18,2) 
)

DECLARE @IntervalTable TABLE 
(
	Id int  Primary Key, --Identity(1,1),
	--,Interval dateTime
	IntervalString nvarchar (255)
)
--------------------------------------------------------------------------------------------------
IF (@LgvId =0) SET @LgvId = NULL
IF (@GroupType = 'HOUR' OR @GroupType ='DAY')
	BEGIN
		INSERT INTO @IntervalTable	
		SELECT * FROM [Reporting].[ufnInterval] (@StartDate,@EndDate,@GroupType)
	END
ELSE IF (@GroupType = 'LGV')
	BEGIN
	   INSERT INTO @IntervalTable	
		-- X BARILLA SELECT Number , Number from Lgvcs.Lgv --where ISNULL(Disabled,0)=0
		SELECT LGVId , LGVId from Lgvcs.Lgv where ISNULL(Disabled,0)=0
		 --FROM [Reporting].[ufnInterval] (@StartDate,@EndDate,@GroupType)
	END
ELSE
	BEGIN 
     INSERT INTO @IntervalTable	
		SELECT 1 , 'TOT' 
    END 		   
--------------------------------------------------------------------------------------------------  
;with cteReportFieldDefinitions as
(
SELECT
	[Id]
    ,[ReportId]
    ,[PickupGroupId]
    ,[DropGroupId]
    ,[FieldName]
    ,[FieldLabel]
    ,[OutputType]
    ,[Enabled]
    ,[ChartDisplay]
    ,[ShowOrder]
    FROM [Reporting].[ReportFieldDefinitions]
    
UNION ALL
    
SELECT
	fld.[Id]
    ,[ReportId]
    ,[PickupGroupId]
    ,[DropGroupId]
    ,[FieldName]+'-Pallets'
    ,[FieldLabel]+'-Pallets'
    ,'NPAL'
    ,fld.[Enabled]
    ,[ChartDisplay]
    ,[ShowOrder]
  FROM [Reporting].[ReportFieldDefinitions] fld
  INNER JOIN [Reporting].Reports rpt on fld.ReportId = rpt.Id 
  WHERE rpt.Name = 'TotalFlow' and fld.Enabled = 1 AND fld.OutputType = 'NR' )
    
    	
,cteGroupStation as (--INSERT INTO @tblGroupStation 
	SELECT fld.PickupGroupId,fld.DropGroupId, pg.StationId as PickUpStationId,
	dg.StationId as DropStationId ,  pg.StationTypeId as PickStationTypeId, dg.StationTypeId as DropStationTypeId
	, fld.FieldName, fld.OutputType,
		fld.Id,fld.ShowOrder
			from cteReportFieldDefinitions fld
			join [Reporting].Reports rpt on fld.ReportId = rpt.Id 
			left join Reporting.StationGroups pg on fld.PickupGroupId = pg.GroupId
			left join Reporting.StationGroups dg on fld.DropGroupId =dg.GroupId
			where rpt.Name = @ReportName and fld.Enabled = 1
) --this wasn't there before.

,cteTmpMissionData as (
select  t.MissionId, g.OutputType, g.FieldName, t.MissionTime, t.PalletCount, t.LgvId, 					
         CASE 
         WHEN  @GroupType= 'HOUR' THEN           
           LTRIM(SUBSTRING(CONVERT( VARCHAR(20), t.ReleaseTime, 22), 10, 2) +':00' + RIGHT(CONVERT(VARCHAR(20), t.ReleaseTime, 22), 3)) 
           --CAST( CONVERT(datetime,CONVERT(char(8), DATEADD(hour, DATEPART(hh,t.ReleaseTime), ''), 114),120) as nvarchar(255))
		 WHEN  @GroupType= 'DAY' THEN  LEFT(CONVERT(CHAR(19), t.ReleaseTime, 0), 11) 
					--CAST(cast(cast(t.ReleaseTime as date) as nvarchar)as nvarchar(255))
		 WHEN  @GroupType= 'LGV' THEN CAST(t.LgvId as nvarchar(50))
		 --WHEN  @groupBy= 'TOT' THEN 
		 ELSE 'TOT'
		 END as GroupBy
		, g.Id --,g.ShowOrder
		, g.PickupGroupId
		, g.DropGroupId
from Reporting.ConsolidatedMissions t
inner join cteGroupStation g
on (t.PickupStationId = g.PickUpStationId OR ((g.PickStationTypeId IS NOT NULL AND g.PickStationTypeId = t.PickupStationTypeId)
 OR ISNULL(g.PickupGroupId, 0) = 0)
)				
--inner join @tblGroupStation dropGroup
AND(t.DropStationId = g.DropStationId OR ((g.DropStationTypeId IS NOT NULL AND g.DropStationTypeId = t.DropStationTypeId)
 OR ISNULL(g.DropGroupId, 0) = 0)
)
where 
-- t.ReadyToPickupTime BETWEEN @StartDate AND @EndDateFilter 
--and t.DropDoneTime BETWEEN @StartDate AND @EndDateFilter 		
t.ReleaseTime BETWEEN @StartDate AND @EndDate
and ISNULL(@LgvId,t.LgvId) = t.LgvId 
)

,CTE as (
SELECT FieldName, 
   MissionTime,
   GroupBy,
   ROW_NUMBER() OVER(PARTITION BY FieldName, GroupBy ORDER BY MissionTime ASC) AS rn,   
   COUNT(MissionTime) OVER(PARTITION BY FieldName, GroupBy) AS cn
FROM cteTmpMissionData 
WHERE MissionTime IS NOT NULL AND OutputType = 'MED'
)
, cteMedianValues as (-- @tmpMedianValues  
SELECT FieldName,
	   AVG(CASE WHEN 2 * rn - cn BETWEEN 0 AND 2 THEN MissionTime END) AS Median,
       GroupBy as IntervalString
from CTE
GROUP BY FieldName, GroupBy
)
--------------------------------------------------------------------------------
INSERT INTO  @ReportData 
select m.FieldName , --fld.FieldLabel,
       case when (m.OutputType = 'NR') then COUNT(m.MissionId)  		
         when m.OutputType = 'NPAL' then SUM(ISNULL(m.PalletCount,0))		
		 when m.OutputType = 'AVG' then AVG(m.MissionTime)
		 when m.OutputType = 'MED' then AVG(v.Median) --still same value
		 --when m.OutputType = 'CFL' then AVG(cf.Value) --still same value
		 else SUM(m.MissionTime) 
		 end as FieldValue 
	    ,m.GroupBy	
FROM
	cteTmpMissionData m  
	left join 
	cteMedianValues v on m.FieldName = v.FieldName and m.GroupBy = v.IntervalString
	--left join 
	--@tmpContractualFlow cf on cf.PickupGroupId = m.PickupGroupId AND cf.DropGroupId = m.DropGroupId AND cf.IntervalString = m.GroupBy
group by m.FieldName, m.GroupBy, m.OutputType--,fld.FieldLabel	,fld.ChartDisplay,fld.ShowOrder	
option (OPTIMIZE FOR UNKNOWN )
--option (OPTIMIZE FOR UNKNOWN )
--select * from CTE
--select * from @tmpMedianValues

-- CONSTRUCT CONTRACTUAL FLOW TEMP TABLE
DECLARE @DateDiff decimal 
SET @DateDiff = DATEDIFF(SECOND, @StartDate, @EndDate) / 3600.00;
-- Avoid subtracting value if this condition happens
IF ( @DateDiff = 0 ) SET @DateDiff = 1;
IF ( (CONVERT(DATE,@StartDate) < CONVERT(DATE,@EndDate)) AND @DateDiff <= 24.00 )
	SET @DateDiff = 48.00;

;with cteI as (
select it.IntervalString, rd.FieldName, rd.PickupGroupId, rd.DropGroupId  
from @IntervalTable it
cross join [Reporting].ReportFieldDefinitions rd 
where rd.Enabled = 1 AND 
	  rd.ReportId = (select top(1) Id FROM [Reporting].Reports where Name = @ReportName) AND 
	  rd.OutputType = 'CFL' 
)
INSERT INTO @tmpContractualFlow
select cf.PickupGroupId, cf.DropGroupId, cteI.IntervalString
	   ,CASE 
         WHEN  @GroupType= 'HOUR' THEN            
         (
			ROUND((cf.Value * (SELECT TOP 1 value FROM [Reporting].GetRangeIntersection(
													  CONVERT(TIME, cteI.IntervalString), 
													  DATEADD(SECOND, 59, DATEADD(MINUTE, 59, CONVERT(TIME, cteI.IntervalString))), 
			                                          (SELECT TOP 1 e1s FROM [Reporting].GetRangeIntersection ( CONVERT(TIME, @StartDate), CONVERT(TIME, '23:59:59'), cf.StartOperationTime, cf.EndOperationTime)),
			                                          (SELECT TOP 1 e1e FROM [Reporting].GetRangeIntersection ( CONVERT(TIME, @StartDate), CONVERT(TIME, '23:59:59'), cf.StartOperationTime, cf.EndOperationTime))
			                                          )) / 3600.00) +
			       (cf.Value * (SELECT TOP 1 value FROM [Reporting].GetRangeIntersection(
													  CONVERT(TIME, cteI.IntervalString), 
													  DATEADD(SECOND, 59, DATEADD(MINUTE, 59, CONVERT(TIME, cteI.IntervalString))), 
			                                          (SELECT TOP 1 e1s FROM [Reporting].GetRangeIntersection ( CONVERT(TIME, '00:00:00'), CONVERT(TIME, @EndDate), cf.StartOperationTime, cf.EndOperationTime)),
			                                          (SELECT TOP 1 e1e FROM [Reporting].GetRangeIntersection ( CONVERT(TIME, '00:00:00'), CONVERT(TIME, @EndDate), cf.StartOperationTime, cf.EndOperationTime))
			                                          )) / 3600.00) +
			       (cf.Value * (SELECT TOP 1 value FROM [Reporting].GetRangeIntersection(
													  CONVERT(TIME, cteI.IntervalString), 
													  DATEADD(SECOND, 59, DATEADD(MINUTE, 59, CONVERT(TIME, cteI.IntervalString))), 
			                                          cf.StartOperationTime, 
			                                          cf.EndOperationTime)) * (CEILING(@DateDiff / 24.00) - 2) / 3600.00),2) 
         )
         WHEN  @GroupType= 'DAY' THEN  
         (
			--Same StartDate and EndDate 
			case when CONVERT(DATE, @StartDate) = CONVERT(DATE, @EndDate) then
					ROUND(cf.Value * (SELECT TOP 1 value FROM [Reporting].GetRangeIntersection(CONVERT(TIME, @StartDate), CONVERT(TIME, @EndDate), cf.StartOperationTime, cf.EndOperationTime)) / 3600.00,2) 
			     --Different StartDate and EndDate 
			     else
			     (
					--StartDate case
					case when CONVERT(DATE, cteI.IntervalString) = CONVERT(DATE, @StartDate) then 
							ROUND(cf.Value * (SELECT TOP 1 value FROM [Reporting].GetRangeIntersection(CONVERT(TIME, @StartDate), CONVERT(TIME, '23:59:59'), cf.StartOperationTime, cf.EndOperationTime)) / 3600.00,2)
					     --EndDate case
						 when CONVERT(DATE, cteI.IntervalString) = CONVERT(DATE, @EndDate) then 
						    ROUND(cf.Value * (SELECT TOP 1 value FROM [Reporting].GetRangeIntersection(CONVERT(TIME, '00:00:00'), CONVERT(TIME, @EndDate), cf.StartOperationTime, cf.EndOperationTime)) / 3600.00,2) 
						 else 
							ROUND(cf.Value * (DATEDIFF(SECOND, cf.StartOperationTime, cf.EndOperationTime)+1) / 3600.00, 2)
			        end
				 )
			end 
         )
		 WHEN  @GroupType= 'LGV' THEN 0
		 ELSE 
		 (
			ROUND((cf.Value * (DATEDIFF(SECOND, cf.StartOperationTime, cf.EndOperationTime)+1) * (CEILING(@DateDiff / 24.00) - 2) / 3600.00) +
				  (cf.Value * (SELECT TOP 1 value FROM [Reporting].GetRangeIntersection(CONVERT(TIME, @StartDate), CONVERT(TIME, '23:59:59'), cf.StartOperationTime, cf.EndOperationTime)) / 3600.00) +
				  (cf.Value * (SELECT TOP 1 value FROM [Reporting].GetRangeIntersection(CONVERT(TIME, '00:00:00'), CONVERT(TIME, @EndDate), cf.StartOperationTime, cf.EndOperationTime)) / 3600.00), 2 ) 
		 )
		 END as Value  
from Reporting.ContractualFlow as cf
INNER JOIN cteI on (cf.PickupGroupId = cteI.PickupGroupId OR (ISNULL(cf.PickupGroupId, 0) = 0 AND ISNULL(cteI.PickupGroupId, 0) = 0)) AND
                   (cf.DropGroupId = cteI.DropGroupId OR (ISNULL(cf.DropGroupId, 0) = 0 AND ISNULL(cteI.DropGroupId, 0) = 0))
option (OPTIMIZE FOR UNKNOWN )




-- FINAL SELECT
;with cteI as (
select it.Id as IdInterval, it.IntervalString, rd.*  
from @IntervalTable it
cross join 
( 
select ChartDisplay, DropGroupId, Enabled, FieldLabel, FieldName,
       Id, OutputType, PickupGroupId, ReportId, ShowOrder 
from [Reporting].ReportFieldDefinitions --rd 
union all
select ChartDisplay, DropGroupId, Enabled, FieldLabel+'-Pallets', FieldName+'-Pallets',
       Id, 'NPAL', PickupGroupId, ReportId, ShowOrder
from [Reporting].ReportFieldDefinitions --rd2
where OutputType = 'NR'
) as rd
where rd.Enabled = 1 AND ReportId = (select top(1) Id FROM  [Reporting].Reports where Name = @ReportName) 
)  
select  c.FieldName,
	    c.FieldLabel,
		CASE WHEN c.OutputType = 'CFL' THEN cf.Value
			 else rd.FieldValue
			 end as FieldValue,
	    --rd.FieldValue as FieldValue,
        c.IntervalString as GroupBy,
        c.OutputType,
        c.ChartDisplay,
        c.ShowOrder,
        c.IdInterval as RowOrderBy
from 
cteI c 
left join 
@ReportData rd on c.IntervalString = rd.IntervalString and c.FieldName = rd.FieldName 
left join 
@tmpContractualFlow cf on --cf.PickupGroupId = c.PickupGroupId AND cf.DropGroupId = c.DropGroupId AND cf.IntervalString = c.IntervalString 
							(cf.PickupGroupId = c.PickupGroupId OR (ISNULL(cf.PickupGroupId, 0) = 0 AND ISNULL(c.PickupGroupId, 0) = 0)) AND
							(cf.DropGroupId = c.DropGroupId OR (ISNULL(cf.DropGroupId, 0) = 0 AND ISNULL(c.DropGroupId, 0) = 0))AND 
							cf.IntervalString = c.IntervalString

