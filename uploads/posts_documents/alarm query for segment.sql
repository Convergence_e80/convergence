/****** Script for SelectTopNRows command from SSMS  ******/
declare @start datetime='2013-04-01 9:00:00'
declare @end datetime='2013-07-23 11:07:00'

SELECT 
       Eq.Dbf_s_Name, Def.Dbf_s_Name, Ev.Dbf_dt_StartDate, Ev.Dbf_s_Text
  FROM [LGVCS_CROSSETT].[dbo].[Tbl_AlarmsEvents] Ev 
	inner join [LGVCS_CROSSETT].[dbo].[Tbl_AlarmDefintion] Def 
	on Ev.Dbf_l_AlarmID=Def.Dbf_l_ID
	inner join [LGVCS_CROSSETT].[dbo].[Tbl_Equipments] Eq
	on Ev.Dbf_l_EquipmentID=Eq.Dbf_l_ID
  where Dbf_dt_StartDate > @start and Dbf_dt_StartDate < @end 
	and Dbf_s_Text like '%Segment: 3778%'
	or Dbf_s_Text like '%Segment: 3775%'
	or Dbf_s_Text like '%Segment: 3877%'
	or Dbf_s_Text like '%Segment: 3876%'
	or Dbf_s_Text like '%Segment: 3288%'
	or Dbf_s_Text like '%Segment: 3287%'
	
 
 