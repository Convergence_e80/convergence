
Active Connections

  Proto  Local Address          Foreign Address        State           PID
  TCP    0.0.0.0:80             0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:135            0.0.0.0:0              LISTENING       908
  TCP    0.0.0.0:445            0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:3389           0.0.0.0:0              LISTENING       1420
  TCP    0.0.0.0:5357           0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:6129           0.0.0.0:0              LISTENING       1680
  TCP    0.0.0.0:6160           0.0.0.0:0              LISTENING       1536
  TCP    0.0.0.0:8288           0.0.0.0:0              LISTENING       1456
  TCP    0.0.0.0:49152          0.0.0.0:0              LISTENING       532
  TCP    0.0.0.0:49153          0.0.0.0:0              LISTENING       988
  TCP    0.0.0.0:49154          0.0.0.0:0              LISTENING       1092
  TCP    0.0.0.0:49155          0.0.0.0:0              LISTENING       604
  TCP    0.0.0.0:49172          0.0.0.0:0              LISTENING       588
  TCP    0.0.0.0:49175          0.0.0.0:0              LISTENING       3512
  TCP    0.0.0.0:49520          0.0.0.0:0              LISTENING       1536
  TCP    127.0.0.1:2559         0.0.0.0:0              LISTENING       4416
  TCP    127.0.0.1:6129         127.0.0.1:49256        ESTABLISHED     1680 dwrcs
  TCP    127.0.0.1:49256        127.0.0.1:6129         ESTABLISHED     5368 dwrcst
  TCP    192.97.54.141:135      192.123.244.172:65506  ESTABLISHED     908
  TCP    192.97.54.141:139      0.0.0.0:0              LISTENING       4
  TCP    192.97.54.141:49172    192.123.244.172:65507  ESTABLISHED     588
  TCP    192.97.54.141:49246    192.97.26.23:445       ESTABLISHED     4
  TCP    192.97.54.141:49273    146.238.17.59:5061     ESTABLISHED     5884
  TCP    192.97.54.141:49395    192.97.108.222:443     ESTABLISHED     4312
  TCP    192.97.54.141:49396    192.97.108.222:443     ESTABLISHED     4312
  TCP    192.97.54.141:49397    192.97.108.222:443     ESTABLISHED     4312
  TCP    192.97.54.141:49398    192.97.108.222:443     ESTABLISHED     4312
  TCP    192.97.54.141:49403    192.97.108.222:443     ESTABLISHED     4312
  TCP    192.97.54.141:49404    192.97.108.222:443     ESTABLISHED     4312
  TCP    192.97.54.141:49495    146.238.36.134:23      ESTABLISHED     2988
  TCP    192.97.54.141:49502    146.238.36.134:23      ESTABLISHED     4156
  TCP    192.97.54.141:49559    192.97.108.222:443     ESTABLISHED     5148
  TCP    192.97.54.141:50076    192.97.108.222:443     TIME_WAIT       0
  TCP    192.97.54.141:50120    192.97.0.11:1433       ESTABLISHED     4916 client
  TCP    192.97.54.141:50121    192.97.46.21:4100      ESTABLISHED     4916
  TCP    192.97.54.141:50123    192.97.0.11:1433       ESTABLISHED     4916
  TCP    192.97.54.141:50124    192.97.46.21:1433      ESTABLISHED     4916
  TCP    192.97.54.141:50125    192.97.0.11:1433       ESTABLISHED     4916
  TCP    192.97.54.141:50126    192.97.108.222:443     TIME_WAIT       0
  TCP    192.97.54.141:50145    146.236.201.187:8000   TIME_WAIT       0
  TCP    192.97.54.141:50151    146.236.201.187:8000   ESTABLISHED     6044
  TCP    192.97.54.141:50153    192.97.108.222:443     ESTABLISHED     5884
  TCP    192.97.54.141:50156    192.97.108.222:443     ESTABLISHED     5148
  TCP    192.97.54.141:50158    192.97.46.21:1433      ESTABLISHED     4916
  TCP    [::]:80                [::]:0                 LISTENING       4
  TCP    [::]:135               [::]:0                 LISTENING       908
  TCP    [::]:445               [::]:0                 LISTENING       4
  TCP    [::]:3389              [::]:0                 LISTENING       1420
  TCP    [::]:5357              [::]:0                 LISTENING       4
  TCP    [::]:6129              [::]:0                 LISTENING       1680
  TCP    [::]:8288              [::]:0                 LISTENING       1456
  TCP    [::]:49152             [::]:0                 LISTENING       532
  TCP    [::]:49153             [::]:0                 LISTENING       988
  TCP    [::]:49154             [::]:0                 LISTENING       1092
  TCP    [::]:49155             [::]:0                 LISTENING       604
  TCP    [::]:49172             [::]:0                 LISTENING       588
  TCP    [::]:49175             [::]:0                 LISTENING       3512
  TCP    [::]:49520             [::]:0                 LISTENING       1536
  UDP    0.0.0.0:123            *:*                                    1280
  UDP    0.0.0.0:500            *:*                                    1092
  UDP    0.0.0.0:3702           *:*                                    3288
  UDP    0.0.0.0:3702           *:*                                    3288
  UDP    0.0.0.0:3702           *:*                                    3288
  UDP    0.0.0.0:3702           *:*                                    1848
  UDP    0.0.0.0:3702           *:*                                    3288
  UDP    0.0.0.0:3702           *:*                                    1848
  UDP    0.0.0.0:4500           *:*                                    1092
  UDP    0.0.0.0:5355           *:*                                    1420
  UDP    0.0.0.0:6129           *:*                                    1680
  UDP    0.0.0.0:8289           *:*                                    1456
  UDP    0.0.0.0:49309          *:*                                    1536
  UDP    0.0.0.0:61799          *:*                                    1848
  UDP    127.0.0.1:1900         *:*                                    1848
  UDP    127.0.0.1:10000        *:*                                    1716
  UDP    127.0.0.1:48000        *:*                                    4416
  UDP    127.0.0.1:48001        *:*                                    3612
  UDP    127.0.0.1:49532        *:*                                    5148
  UDP    127.0.0.1:50321        *:*                                    1172
  UDP    127.0.0.1:56193        *:*                                    1848
  UDP    127.0.0.1:56275        *:*                                    1092
  UDP    127.0.0.1:58903        *:*                                    5504
  UDP    127.0.0.1:60261        *:*                                    2832
  UDP    127.0.0.1:62700        *:*                                    604
  UDP    127.0.0.1:64347        *:*                                    1420
  UDP    127.0.0.1:65251        *:*                                    6044
  UDP    192.97.54.141:137      *:*                                    4
  UDP    192.97.54.141:138      *:*                                    4
  UDP    192.97.54.141:1900     *:*                                    1848
  UDP    192.97.54.141:56192    *:*                                    1848
  UDP    [::]:123               *:*                                    1280
  UDP    [::]:500               *:*                                    1092
  UDP    [::]:3540              *:*                                    3236
  UDP    [::]:3702              *:*                                    3288
  UDP    [::]:3702              *:*                                    3288
  UDP    [::]:3702              *:*                                    1848
  UDP    [::]:3702              *:*                                    1848
  UDP    [::]:3702              *:*                                    3288
  UDP    [::]:3702              *:*                                    3288
  UDP    [::]:4500              *:*                                    1092
  UDP    [::]:5355              *:*                                    1420
  UDP    [::]:6129              *:*                                    1680
  UDP    [::]:8289              *:*                                    1456
  UDP    [::]:61800             *:*                                    1848
  UDP    [::1]:1900             *:*                                    1848
  UDP    [::1]:56191            *:*                                    1848
  UDP    [fe80::611e:2ca9:f6c8:8be8%11]:1900  *:*                                    1848
  UDP    [fe80::611e:2ca9:f6c8:8be8%11]:56190  *:*                                    1848
