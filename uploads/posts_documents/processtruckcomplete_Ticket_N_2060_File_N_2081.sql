USE [WMS_KRAFT_2014_03_26]
GO
/****** Object:  StoredProcedure [WMS_Kraft].[ProcessTruckCompleteMessage]    Script Date: 03/26/2014 08:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	Processing of Truck Complete Message.
-- =============================================
ALTER PROCEDURE [WMS_Kraft].[ProcessTruckCompleteMessage]
	--INPUT PARAMETERS
	@TransactionDate nvarchar(8),
	@TransactionTime nvarchar(9),
	@ShipmentTruckNumber nvarchar(7),
	
	--OTHER INPUTS PARAMETER
	@EnableSavePoint bit,
	
	-- OUTPUT PARAMETERS	
	@ResultCode				INT				=	NULL OUTPUT,
	@ResultMsg				NVARCHAR(255)	=	NULL OUTPUT,			
	@ResultTimeStamp		Datetime		=	NULL OUTPUT,
	@ResultErrorCode		nvarchar(10)	=	NULL OUTPUT
AS
BEGIN 



DECLARE @preTranCount			int,		
		@error					int,
		@counter				int,		
		@CurrentTimeStamp		DateTime,
		@errorCode				nvarchar(2)		=	null,
		@savePointDefined				bit		=	0

SET NOCOUNT ON;



BEGIN TRY
	
	SET	@ResultCode			=	0;			-- Default value = 0 --> NO RECORD MODIFIED.
	SET @Resultmsg			=	NULL;
	SET @counter			=	0;
	SET	@error				=	0;	
	set	@CurrentTimeStamp	=	GETDATE();
	SET	@ResultTimeStamp	=	@CurrentTimeStamp
	
	
	if ( XACT_STATE() < 0 )		
	begin
		-- If XACT_STATE() < 0 -->  No writes or changes can be executed: only
		--							a complete ROLLBACK by the owner of the transaction.
		--							This procedure is not the owner, so it can
		--							only exit.
		return @ResultCode;
	end
							
	SET		@preTranCount		=	@@trancount; 	
		
	
	---------------------	OPERATIVE STEPS	-----------------------------------------
	--1. CSearch for a shipment if not found create a new one
	
	--SA
	DECLARE @ShipmentID BIGINT = NULL
	
	declare @TicketsToDelete Table
	(
		idx				int primary key identity (1,1),
		Id_StockUnit	bigint
	)
	declare @palCounter int = 0
	declare @palIndex	int = 0
	declare @palId		bigint = 0
	declare @TransportOrders table
	(
		Id_TransportOrder	bigint
	)
	
	
	--I CHECK IF THE SHIPMENT TRUCK NUMBER IS VALID
	IF ISNULL(@ShipmentTruckNumber,'')<>''
	BEGIN
		--I FIND ALL THE SHIPMENTS WITH THIS TRUCK NUMBER
		SELECT @ShipmentID = S.Id FROM [WMS.Shipping].Shipment S
		WHERE S.ShipmentNr = @ShipmentTruckNumber	
		
		--IF A SHIPMENT IS FOUND, ALL PALLET DATA GETS DELETED
		IF (@ShipmentID IS NOT NULL)
			BEGIN
				insert into @TicketsToDelete
				Select COALESCE(Id_ConnectedStockUnit, Id_RequestedStockUnit) from [WMS.Handling].TransportOrder
				where ShipmentNr = @ShipmentTruckNumber and Id_TransportOrderStatus = (select Id from [WMS.Handling].TransportOrderStatus where Name = 'Closed')
				set @palCounter = @@ROWCOUNT
				
				-- We set a description in stockunits going to be deleted.
				UPDATE	KS
				SET		LastUpdateDescription	=	( 'StockUnit is going to be deleted due to TruckComplete for shipment ' + @ShipmentTruckNumber ),
						LastUpdateTimeStamp		=	@CurrentTimeStamp
				FROM	WMS_KRAFT.STOCKUNIT AS KS
				JOIN	@TicketsToDelete TTD
				ON		TTD.Id_StockUnit = KS.ID
				
				while (@palIndex < @palCounter)
				begin
					set @palIndex = @palIndex + 1
					select @palId = Id_StockUnit from @TicketsToDelete where idx = @palIndex
					--AM 2014-03-26 Ticket #2060
					if EXISTS( (select sup.id_stockunit from [wms.storage].StorageType st
					inner join [WMS.Storage].StorageLocation sl on sl.Id_StorageType = st.Id
					inner join [WMS.Storage].StoragePosition sp on sp.Id_StorageLocation = sl.Id
					inner join [WMS.Storage].StockUnitPosition sup on sup.Id_StoragePosition = sp.Id
					where sup.Id_StockUnit = @palId AND st.name = 'StagingLane') )
					begin
						DELETE FROM [WMS.Storage].StockUnitPosition WHERE Id_StockUnit = @palId AND 
							Id_StoragePosition in (SELECT SP.id FROM [WMS.Storage].StorageLocation SL 
								inner join [WMS.Storage].StoragePosition SP ON SP.Id_StorageLocation = SL.id 
								inner join [WMS.Storage].StorageType ST ON ST.Id = SL.Id_StorageType
									WHERE ST.Name = 'StagingLane' )
						
						--need to add a check also on connected stock unit												
						insert into @TransportOrders
						select TransportOrder.Id FROM [WMS.Handling].TransportOrder
						WHERE Id_RequestedStockUnit = @palId or Id_ConnectedStockUnit = @palId
						if (select COUNT(*) from @TransportOrders) > 0
						begin
							DELETE FROM [WMS.Handling].PickAdvice WHERE Id_TransportOrder in (select Id_TransportOrder from @TransportOrders)
							DELETE FROM [WMS.Handling].StockUnitTransport WHERE Id_TransportOrder in (select Id_TransportOrder from @TransportOrders) AND Id_StockUnit = @palId
							DELETE FROM [WMS.Handling].StockUnitAllocation WHERE Id_TransportOrder in (select Id_TransportOrder from @TransportOrders) AND Id_StockUnit = @palId
							DELETE FROM [WMS.Storage].StockUnitMovement WHERE Id_TransportOrder in (select Id_TransportOrder from @TransportOrders)
							
							UPDATE	TOR
							SET		LastUpdateDescription = 'Deletion request due to TruckComplete ',
									LastUpdateTimeStamp	  = @currentTimeStamp	
							FROM	[WMS.Handling].TransportOrder TOR
							WHERE	TOR.ID IN (select Id_TransportOrder from @TransportOrders) 
							DELETE FROM [WMS.Handling].TransportOrder WHERE Id in (select Id_TransportOrder from @TransportOrders)
						end
						
						IF NOT EXISTS(select sup.id_stockunit from [wms.storage].StorageType st
							inner join [WMS.Storage].StorageLocation sl on sl.Id_StorageType = st.Id
							inner join [WMS.Storage].StoragePosition sp on sp.Id_StorageLocation = sl.Id
							inner join [WMS.Storage].StockUnitPosition sup on sup.Id_StoragePosition = sp.Id
							where sup.Id_StockUnit = @palId AND st.name <> 'StagingLane')
						BEGIN 		
							DELETE FROM WMS_KRAFT.AUTOPrnTicket WHERE ID = @palId
							DELETE FROM WMS_KRAFT.MANUPrn WHERE ID = @palId
							DELETE FROM WMS_KRAFT.GRAIAssociations WHERE StockUnitID = @palId 										
							DELETE FROM WMS_KRAFT.StockUnit WHERE ID = @palId					
							DELETE FROM [WMS.Inventory].StockUnitBlockingReason WHERE Id_StockUnit = @palId
							DELETE FROM [WMS.Inventory].StockUnitCustomProperties WHERE Id_StockUnit = @palId					
							DELETE FROM [WMS.Inventory].StockUnitPart WHERE Id_StockUnit = @palId
							DELETE FROM [WMS.Inventory].StockUnit WHERE ID = @palId	
						END
						
	
					end
				end
								
				update [WMS.Shipping].Shipment set ShipDate = GETDATE(), Id_ShipmentStatus = (Select Id from [WMS.Shipping].ShipmentStatus where Name = 'Closed') where Id = @ShipmentID
				
				-- Delete all Transport Order connected to the shipment in Validating Status
				-- and not only. We delete all the remaining RET transportOrders related
				-- to the shipment.
				DELETE FROM @TransportOrders
				
				INSERT INTO @TransportOrders(Id_TransportOrder )
				select	tord.id from [WMS.Handling].TransportOrder tord 
				join	[WMS.Handling].TransportOrderType tordt on tordt.Id = tord.Id_TransportOrderType				 
				where	tordt.Name = 'RET'	
				and		tord.ShipmentNr = @ShipmentTruckNumber
				
				-- We set the fields for journal scope.
				UPDATE	TOR
				SET		LastUpdateDescription = 'Deletion request due to TruckComplete ',
						LastUpdateTimeStamp	  = @currentTimeStamp	
				FROM	[WMS.Handling].TransportOrder TOR
				WHERE	TOR.ID IN (select Id_TransportOrder from @TransportOrders) 
				
				
				-- We delete the StockUnitAllocation first in case of some pending StockUnit
				DELETE FROM [WMS.Handling].StockUnitAllocation WHERE Id_TransportOrder in (select Id_TransportOrder from @TransportOrders)
				-- We delete the transport Orders.
				DELETE FROM [WMS.Handling].TransportOrder WHERE Id in (select Id_TransportOrder from @TransportOrders)
	
				-- We remove all the staging lanes assigned to the shipment.
				DELETE	SSGL
				FROM	[WMS.Shipping].ShipmentStagingLane SSGL
				WHERE	SSGL.Id_Shipment = @ShipmentID
			END
		ELSE
			BEGIN
				-- NO VALID SHIPMENT TRUCK NUMBER IS FOUND
				SET @ResultCode = ''
				SET @ResultMsg = 'Shipment Information does not exist in E80'
				SET @ResultErrorCode = 'SA'		
			END				
	END
	
		
	
	---------------------	FINAL STEPS		-----------------------------------------		
	FinalSteps:				
	

END TRY

BEGIN CATCH
	declare @Error_number		int,			@Error_severity int
	declare @Error_procedure	nvarchar(126),	@Error_message	nvarchar(2048)
	declare @Error_line			int,			@Error_state	int
	declare @xstate				smallint,		@actTranCount	integer
	select	@Error_number		=	ERROR_NUMBER(),
			@Error_severity		=	ERROR_SEVERITY(),
			@Error_state		=	ERROR_STATE(),
			@Error_procedure	=	ERROR_PROCEDURE(),
			@Error_line			=	ERROR_LINE(),
			@Error_message		=	ERROR_MESSAGE(),
			@xstate				=	XACT_STATE(),
			@actTranCount		=	@@TRANCOUNT
	
		
	EXECUTE [WMS.Common].[LogProcedureError]
	
	raiserror ('%s: %d: %s', 16, 1, @Error_Procedure, @Error_number, @Error_Message) ;			 		
	SET @Resultmsg = SUBSTRING(@Error_message,1,255)			
END CATCH	

RETURN SELECT @ResultCode, @ResultMsg, @ResultTimeStamp, @ResultErrorCode;

END
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       