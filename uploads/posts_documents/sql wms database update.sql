SELECT     slc.LocationType, slc.GroupType,sl.Classification, sl.Name, sl.Clearance as locationclearence,max (sp.[Level]),max(sp.Clearence) as positionclearence  
FROM         [WMS.Storage].StorageLocation AS sl INNER JOIN
                      [WMS.Storage].StorageLocationClassification AS slc ON sl.Classification = slc.Classification INNER JOIN
                      [WMS.Storage].StoragePosition AS sp ON sl.Id = sp.Id_StorageLocation
WHERE     (slc.GroupType is  NULL) and  --sl.Classification <> '(52 - 2800 - 12)- (106 - 999999 - 6)' and
slc.LocationType = 'Drivein' and sl.Name not like 'D18%'
Group by slc.LocationType ,slc.GroupType ,sl.Name ,sl.Clearance,sl.Classification  -- sp.Level,,sp.Clearence 
ORDER BY slc.LocationType

SELECT     slc.LocationType, slc.GroupType,sl.Classification, sl.Name, sl.Clearance as locationclearence,sp.[Level],sp.Clearence as positionclearence  
FROM         [WMS.Storage].StorageLocation AS sl INNER JOIN
                      [WMS.Storage].StorageLocationClassification AS slc ON sl.Classification = slc.Classification INNER JOIN
                      [WMS.Storage].StoragePosition AS sp ON sl.Id = sp.Id_StorageLocation
WHERE     (slc.GroupType is  NULL) and  --sl.Classification <> '(52 - 2800 - 12)- (106 - 999999 - 6)' and
slc.LocationType = 'Drivein' and sl.Name = 'D59T16'
Group by slc.LocationType ,slc.GroupType ,sl.Name ,sl.Clearance,sl.Classification  ,sp.Level,sp.Clearence 
ORDER BY slc.LocationType

--update [WMS.Storage].StorageLocationClassification set GroupType = 'BS-(D)' where LocationType = 'BlockStorage' and GroupType is null
--update [WMS.Storage].StorageLocationClassification set GroupType = 'Sel' where LocationType = 'SelectiveRack' and GroupType is null
--update [WMS.Storage].StorageLocationClassification  set GroupType = 'DI-100-50-50-(T)' where LocationType = 'Drivein' and Classification = ' (52 - 2800 - 12)- (106 - 999999 - 6)' and GroupType is  null
--update [WMS.Storage].StorageLocationClassification  set GroupType = 'DI-100-50-50-(T)' where LocationType = 'Drivein' and Classification = ' (52 - 2800 - 10)- (106 - 999999 - 5)' and GroupType is  null
--update [WMS.Storage].StorageLocationClassification  set GroupType = 'DI-110-50-(U)' where LocationType = 'Drivein' and Classification = ' (52 - 2800 - 3)- (108 - 999999 - 3)' and GroupType is  null
--update [WMS.Storage].StorageLocationClassification  set GroupType = 'DI-100-50-50-(T)' where LocationType = 'Drivein' and Classification = ' (52 - 2800 - 4)- (53 - 2800 - 4)- (106 - 999999 - 4)' and GroupType is  null
--update [WMS.Storage].StorageLocationClassification  set GroupType = 'DI-100-50-50-(T)' where LocationType = 'Drivein' and Classification = ' (52 - 2800 - 5)- (53 - 2800 - 5)- (106 - 999999 - 5)' and GroupType is  null
--update [WMS.Storage].StorageLocationClassification  set GroupType = 'DI-100-100-(A)' where LocationType = 'Drivein' and Classification = ' (105 - 999999 - 5)- (108 - 2800 - 5)' and GroupType is  null
--update [WMS.Storage].StorageLocationClassification  set GroupType = 'DI-100-50-50-(T)' where LocationType = 'Drivein' and Classification = ' (52 - 2800 - 3)- (53 - 2800 - 6)- (106 - 999999 - 6)' and GroupType is  null
--update [WMS.Storage].StorageLocationClassification  set GroupType = 'DI-100-100-(A)' where LocationType = 'Drivein' and Classification = ' (82 - 999999 - 1)- (87 - 999999 - 5)- (109 - 2800 - 6)' and GroupType is  null
--update [WMS.Storage].StorageLocationClassification  set GroupType = 'DI-100-50-50-(T)' where LocationType = 'Drivein' and Classification = ' (50 - 2800 - 5)- (53 - 2800 - 5)- (105 - 999999 - 4)- (106 - 999999 - 1)' and GroupType is  null
--update [WMS.Storage].StorageLocationClassification  set GroupType = 'DI-100-50-50-(T)' where LocationType = 'Drivein' and Classification = ' (50 - 2800 - 3)- (53 - 2800 - 6)- (105 - 999999 - 2)- (106 - 999999 - 4)' and GroupType is  null
--update [WMS.Storage].StorageLocationClassification  set GroupType = 'DI-100-50-50-(T)' where LocationType = 'Drivein' and Classification = ' (52 - 2800 - 6)- (53 - 2800 - 6)- (106 - 999999 - 6)' and GroupType is  null
--update [WMS.Storage].StorageLocationClassification  set GroupType = 'DI-100-50-50-(T)' where LocationType = 'Drivein' and Classification = ' (52 - 2800 - 8)- (53 - 2800 - 8)- (106 - 999999 - 8)' and GroupType is  null
--update [WMS.Storage].StorageLocationClassification  set GroupType = 'DI-100-50-50-(T)' where LocationType = 'Drivein' and Classification = ' (0 - 2800 - 3)- (49 - 2800 - 5)- (101 - 999999 - 4)' and GroupType is  null
--update [WMS.Storage].StorageLocationClassification  set GroupType = 'DI-100-50-50-(T)' where LocationType = 'Drivein' and Classification = ' (52 - 2800 - 16)- (106 - 999999 - 8)' and GroupType is  null
--update [WMS.Storage].StorageLocationClassification  set GroupType = 'DI-100-50-50-(T)' where LocationType = 'Drivein' and Classification = ' (52 - 2800 - 20)- (106 - 999999 - 10)' and GroupType is  null