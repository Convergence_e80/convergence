declare @PickUpDepositStation	varchar(10) -- Stazione di prelievo nel caso in cui non ci siano Shipping Orders prossimi; Shipping Rows nel caso in cui ci siano Shipping Orders prossimi
declare @NotThisStation			varchar(10) -- Escludo questa stazione dalla ricerca
declare @Product				varchar(30) -- Prodotto
declare @PalletType				varchar(30) -- Tipo Paletta
declare @ProductionLine			varchar(30) -- Linea di Produzione
declare @ABC_CLASS_TXT			varchar(4)	 -- Classe del prodotto
declare @LGVType				int          -- Tipo LGV


set @PickUpDepositStation = 'PCK_ST_A_E'
set @NotThisStation = ''
set @Product = '96402513'
set @PalletType = ''
set @ABC_CLASS_TXT = 'C'
set @LGVType = 1


				   								


DECLARE @bMaxHeight			int

--RP DEC 2011 - last check before finding destination if we need to update the pallet type
-------------------------------------------------------------------------
DECLARE @PalletLength       numeric(15,8)
DECLARE @PalletTypetmp         varchar(30)
DECLARE @testsku int
    
--get the pallet type from the pallet length and compare it with the current pallet type
--if it is not correct then change the load unit pallet type.
select @PalletLength = LGTH from tblProducts where ITEM_NUM = @Product

--Testing check if the product is being tested -------------------------------------
--set @testsku = 0
--select @testsku = COUNT(*) from tblTest where test_sku = @Product
-------------------------------------------------------------------------------------
	--if(@testsku > 0)
    if(@PalletLength is not null)
	begin
	   select @PalletTypetmp = stp.PalletTypeName from tblloadUnitSizeType stp where @PalletLength > stp.MinPalletLength and @PalletLength <= stp.MaxPalletLength		   

		-- get the pallet type from the pallet length and compare it with the current pallet type
		--if it is not correct then change the load unit pallet type. This will be used to be able to housekeep
		--or get pallets from the inbound and correct the old pallets with the wrong pallet type
		   if(@PalletType <> @PalletTypeTmp and @PalletTypetmp is not null)
		   begin
               set @PalletType = @PalletTypetmp		
		   end
	 end
-------------------------------------------------------------------------
-------------------------------------------------------------------------



select t1.StationId,t1.StoreId,1,1
from TblStations t1  
inner join-- determino la migliore area di deposito a seconda della stazione @PickUpDepositStation
tblStationBestOperation t2
on t2.IdAreaBestOperations = t1.idAreaBestOperations
and t2.IdStationBestOperations = @PickUpDepositStation
inner join		-- determino la migliore area di deposito a seconda della classe @ABC_CLASS_TXT del prodotto
tblProductsBestOperation t3 on t1.idAreaProductPriority = t3.isAreaProductPriority
and t3.ABC_CLASS_TXT = @ABC_CLASS_TXT
left outer join -- determino il numero di good pallet contenenti lo stesso prodotto per area e sottoarea
(
	select idAreaBestOperations,idSubAreaBestOperations,sum(LUPickupNumber)	as iGoodPalletNumber
	from TblStations
	where ((LUPickupNumber > 0) and (LUPickupNumber is not null))
	and StationType = 'GOOD_PALLET'
	and item_num = @Product
	and EnabledPut = 1 --and EnabledGet = 1
	and Enabled = 1 and Blocked = 0
	and AllowedPalletType like '%' + @PalletType + '%'	
	and LGVType = @LGVType 
	group by idAreaBestOperations,idSubAreaBestOperations
	having sum(LUPickupNumber) is not null
) as t4			------------------------------------------------------------------------------------------
on t1.idAreaBestOperations = t4.idAreaBestOperations 
and t1.idSubAreaBestOperations = t4.idSubAreaBestOperations 
left outer join -- determino il numero di missioni di deposito per corridoio 
(
	select t1.HallWay,ISNULL(count(distinct t2.Cod_MX),0) as iMissionsNumber
	from tblStations t1 inner join tblMissions t2 on t1.StationId = t2.StationId_End
	and t1.StationType = 'GOOD_PALLET' and t2.deleting = 0
	group by t1.HallWay	
	having (count(distinct t2.Cod_MX) is not null) and (count(distinct t2.Cod_MX) >0)
) as t5		   -------------------------------------------------------------------------------------------	
on t1.HallWay = t5.HallWay
left outer join -- determino il numero di good pallet contenenti lo stesso prodotto per corridoio
(
	select HallWay,sum(LUPickupNumber) as iGoodPalletNumber
	from TblStations where ((LUPickupNumber > 0) and (LUPickupNumber is not null))
	and StationType = 'GOOD_PALLET' and item_Num = @Product and EnabledPut = 1 --and EnabledGet = 1
	and Enabled = 1 and Blocked = 0 and AllowedPalletType like '%' + @PalletType + '%'	
	and LGVType = @LGVType 
	group by HallWay having (sum(LUPickupNumber) is not null) and (sum(LUPickupNumber) >0)
) as t6			----------------------------------------------------------------------------------
on t1.HallWay = t6.HallWay
left outer join -- determino il numero di missioni di prelievo per corridoio
(	
	select t1.HallWay,ISNULL(count(distinct t2.Cod_MX),0) as iMissionsNumber,
	isnull(max(cast(right(t1.Stationid,3)as int)),0) as MaxNumStation
	from tblStations t1 inner join tblMissions t2 on t1.StationId = t2.StationID_Start
	and t1.StationType = 'GOOD_PALLET' and t2.deleting = 0 and t2.phase < 3
	group by t1.HallWay	
	having (count(distinct t2.Cod_MX) is not null) and (count(distinct t2.Cod_MX) >0)
) as t8			------------------------------------------------------------------------------------------------------------
on t1.Hallway = t8.hallway
left outer join -- determino il numero di stazioni con prodotto per corridoio
(
	select t95.hallway,Isnull(count(distinct t95.StationID),0) as iPalletNumber
	from tblStations t95
	where t95.StationType = 'GOOD_PALLET'
	--and t95.EnabledGet = 1
	and t95.EnabledPut = 1
	and t95.LGVType = @LGVType	
	and ((item_num is not null) and len(item_num) > 0)
	group by t95.hallway	
	having count(distinct t95.StationID) is not null		
) as t11			-----------------------------------------------------------------------------------------------------------
on t1.Hallway = t11.Hallway
left outer join
(
	select StationID_End,Isnull(count(distinct StationID_End),0) as NumMiss from 
	tblMissions where Item_Num <> @Product and deleting = 0 
	group by StationID_End	
) as t10
on t1.StationId = t10.StationId_End
left Outer Join -- parte aggiunta per i nuovi controlli
(
	select Q1.PriorityPick,Q1.Hallway
	from tblStations Q1 where Q1.item_num = @Product
	and StationType = 'GOOD_PALLET'
	group by Q1.PriorityPick,Q1.Hallway
) K2
on t1.Hallway = K2.Hallway
and t1.PriorityPick < (K2.PriorityPick + 6)
and t1.PriorityPick > (K2.PriorityPick - 6)                     -- fine parte aggiunta per i nuovi controlli
where 
t1.LGVType = @LGVType											-- tipo Lgv
and t1.StationType = 'GOOD_PALLET'								-- tipo stazione
and t1.AllowedPalletType like '%' + @PalletType + '%'			-- tipo pallet consentito
and ((t1.Item_num is null) or (len(t1.Item_num)=0))				-- nessun articolo nella fila
and t1.Enabled = 1												-- fila abilitata
and t1.Blocked = 0												-- fila non bloccata
and t1.EnabledPut = 1											-- fila abilitata la deposito
and t1.StationId <> @NotThisStation								-- fila da escludere nella ricerca
-- la differenza tra le posizioni vuote presenti e quelle che stanno andando l� con lo stesso prodotto
and ((t1.EmptyPositionsNum is null) 
or ((t1.EmptyPositionsNum  > 
(select IsNull(count(distinct t30.Cod_MX),0) from 
tblMissions t30 where t30.StationID_End = t1.StationId 	
and t30.Item_Num = @Product and t30.Deleting = 0)
) and t1.EmptyPositionsNum > 0))
and ((t1.AllLoadUnitNum = 0) or (t1.AllLoadUnitNum is null))
and (t10.NumMiss = 0 or t10.NumMiss is null)
group by t1.StationId,t1.StoreId,t2.Priority,t3.Priority,t4.iGoodPalletNumber,t6.iGoodPalletNumber,
t11.iPalletNumber,t5.iMissionsNumber,t8.iMissionsNumber,t1.PriorityHallWay,t1.PriorityPick,t1.PriorityDep,t8.MaxNumStation
order by 
t2.Priority desc,				-- priorit� pi� alta di deposito per area e sottoarea
t3.Priority desc,				-- priorit� pi� alta di deposito per classe di prodotto
t4.iGoodPalletNumber asc,		-- numero minore di good pallet con lo stesso prodotto nella sottoarea
t6.iGoodPalletNumber asc,		-- numero minore di good pallet con lo stesso prodotto nel corridoio
count(t1.HallWay) asc,          -- numero minore di stazioni con lo stesso prodotto in un raggio di 5 stazioni
t5.iMissionsNumber asc,			-- numero minore di missioni di deposito per corridoio
(t8.MaxNumStation - cast(right(t1.StationId,3) as int)) desc, 
t11.iPalletNumber asc,          -- numero minore di pallet con prodotto per corridoio
--t5.iMissionsNumber asc,		-- numero minore di missioni di deposito per corridoio
--t8.iMissionsNumber asc,       -- numero minore di missioni di prelievo per corridoio
t8.iMissionsNumber desc,        -- numero maggiore di missioni di prelievo per corridoio
t1.PriorityHallWay desc,		-- priorit� del corridoio
t1.PriorityDep desc, 			-- priorit� pi� alta di deposito definito da layout
t1.PriorityPick desc    		-- priorit� di prelievo

