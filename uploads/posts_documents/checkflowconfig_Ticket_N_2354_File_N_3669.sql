DECLARE @ReportName nvarchar(50) = 'TotalFlow'

-- 1. Get report flows
;with cteRFD as (
	SELECT fld.*
	FROM [Reporting].ReportFieldDefinitions fld
	JOIN [Reporting].Reports rpt on fld.ReportId = rpt.Id
	where rpt.Name = @ReportName
)
-- 2. GET STATIONS AND STATION TYPES 
,cteFilteredStationsGroups as (
	select *
	from Reporting.StationGroups sg
	where [Enabled] = 1
)
-- 3. GET FLOWS GROUPS/STATION/TYPES LINKS
,cteGroupStation as ( 
	SELECT t.PickupGroupId, t.DropGroupId,
           pg.StationId as PickUpStationId, dg.StationId as DropStationId, 
	       pg.StationTypeId as PickStationTypeId, dg.StationTypeId as DropStationTypeId
	FROM
		(
		SELECT fld.PickupGroupId, fld.DropGroupId
		from cteRFD fld
		join [Reporting].Reports rpt on fld.ReportId = rpt.Id 
		where rpt.Name = @ReportName
		) t
		left join cteFilteredStationsGroups pg on t.PickupGroupId = pg.GroupId
		left join cteFilteredStationsGroups dg on t.DropGroupId = dg.GroupId
	)
-- 4. GET ERRORS
select t.*,
       g1.Name as SourceGroupName,
       g2.Name as DestGroupName
from
(
	select *, 
		   COUNT(*) OVER(PARTITION BY PickUpStationId, DropStationId, PickStationTypeId, DropStationTypeId ) AS Cnt
	from cteGroupStation 
) t
inner join Reporting.Groups g1 on t.PickupGroupId = g1.Id
inner join Reporting.Groups g2 on t.DropGroupId = g2.Id
where t.Cnt > 1