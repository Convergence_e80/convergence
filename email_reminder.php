<!--%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

VERSION: 	

- 1.1

COMMENTS:	(add comment describing what has been changed)

-  
- 

DATE:

- 03/14/13

AUTHOR:

- Jose Hermida

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%-->

<?php 

 ini_set('display_errors','on');
 
function create_email_format ($ticket_title,$ticket_creator,$ticket_status,$ticket_priority,$ticket_CC,$ticket_assignee,$ticket_customer,$ticket_date_creation,$ticket_name_contact,$ticket_cellphone_contact,$ticket_email_contact, $ticket) {

include("connection.php");



$query_author = "select * from Employees where Id=$ticket_creator";
$result_query_author = sqlsrv_query($conn,$query_author);
$row_query_author = sqlsrv_fetch_array($result_query_author);

$author=$row_query_author[14];

$query_status = "select * from Ticket_Status where Id=$ticket_status";
$result_query_status = sqlsrv_query($conn,$query_status);
$row_query_status = sqlsrv_fetch_array($result_query_status);

$status=$row_query_status[1];

$query_priority = "select * from Priority where Id=$ticket_priority";
$result_query_priority = sqlsrv_query($conn,$query_priority);
$row_query_priority = sqlsrv_fetch_array($result_query_priority);

$priority = $row_query_priority[1];

$query_customer = "select * from Customers where Id=$ticket_customer";
$result_query_customer = sqlsrv_query($conn,$query_customer);
$row_query_customer = sqlsrv_fetch_array($result_query_customer);

$customer = $row_query_customer[1];

$query_cc = "select * from CustomersEquipment where Id=$ticket_CC";
$result_query_cc = sqlsrv_query($conn,$query_cc);
$row_query_cc = sqlsrv_fetch_array($result_query_cc);

$cc = $row_query_cc[2];

$date = $ticket_date_creation;

//$tikpot = nl2br($ticket_post);

$text="


<body>
<img src='http://www.elettric80inc.com/Convergence/images/email_top.jpg' /> <BR>
<BR>
<div class=WordSection1>

<br />
<table width='800' border='0' cellpadding='0' cellspacing='0'>
  <tr>
    <td width='85'><div align='right'><b style='mso-bidi-font-weight:bold; font-family:Verdana;font-size: 12px;'><span style='color:#000000;
  mso-themecolor:text2'>Author:
    </span></b></div>      <b style='mso-bidi-font-weight:bold; font-family:Verdana;font-size: 12px;'><span style='color:#000000;
  mso-themecolor:text2'><o:p></o:p>
    </span></b></td>
    <td width='7'>&nbsp;</td>
    <td width='427'><span class='MsoNormal' style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal; font-family:Verdana;font-size: 12px; '>$author</span></td>
    <td width='10'>&nbsp;</td>
    <td width='78'><div align='right'><b style='mso-bidi-font-weight:bold; font-family:Verdana;font-size: 12px;'><span style='color:#000000;
  mso-themecolor:text2'>Contac 
    </span></b></div> </td>
    <td width='9'><b style='mso-bidi-font-weight:bold; font-family:Verdana;font-size: 12px;'><span style='color:#000000;
  mso-themecolor:text2'>t</span></b></td>
    <td width='186'><b style='mso-bidi-font-weight:bold; font-family:Verdana;font-size: 12px;'><span style='color:#000000;
  mso-themecolor:text2'>Information</span></b></td>
  </tr>
  <tr>
    <td><div align='right'><b style='mso-bidi-font-weight:bold; font-family:Verdana;font-size: 12px;'><span style='color:#000000;
  mso-themecolor:text2'>Status:
    </span></b></div>      <b style='mso-bidi-font-weight:bold; font-family:Verdana;font-size: 12px;'><span style='color:#000000;
  mso-themecolor:text2'><o:p></o:p>
    </span></b></td>
    <td>&nbsp;</td>
    <td><span class='MsoNormal' style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal;font-family:Verdana;font-size: 12px;'> $status </span></td>
    <td>&nbsp;</td>
    <td><div align='right' class='style2'></div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align='right'><b style='mso-bidi-font-weight:bold;font-family:Verdana;font-size: 12px;'><span style='color:#000000;
  mso-themecolor:text2'>Priority:
    </span></b></div>      <b style='mso-bidi-font-weight:bold;font-family:Verdana;font-size: 12px;'><span style='color:#000000;
  mso-themecolor:text2'><o:p></o:p>
    </span></b></td>
    <td>&nbsp;</td>
    <td><span class='MsoNormal' style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal;font-family:Verdana;font-size: 12px;'> $priority </span></td>
    <td>&nbsp;</td>
    <td><div align='right' class='style2'><div align='right'><b style='mso-bidi-font-weight:bold; font-family:Verdana;font-size: 12px;'><span style='color:#000000;
  mso-themecolor:text2'>Name:
    </span></b></div> </div></td>
    <td>&nbsp;</td>
    <td><span class='MsoNormal' style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal;font-family:Verdana;font-size: 12px;'><span class='MsoNormal' style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal; font-family:Verdana;font-size: 12px; '>$ticket_name_contact</span> </span> </td>
  </tr>
  <tr>
    <td><div align='right'><b style='mso-bidi-font-weight:bold; font-family:Verdana;font-size: 12px;'><span style='color:#000000;
  mso-themecolor:text2'>Customer:
    </span></b></div>      <b style='mso-bidi-font-weight:normal'><span style='color:#000000;
  mso-themecolor:text2'><o:p></o:p>
    </span></b></td>
    <td>&nbsp;</td>
    <td><span class='MsoNormal' style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal;font-family:Verdana;font-size: 12px;'> $customer </span></td>
    <td>&nbsp;</td>
    <td><div align='right' class='style2'><div align='right'><b style='mso-bidi-font-weight:bold; font-family:Verdana;font-size: 12px;'><span style='color:#000000;
  mso-themecolor:text2'> Phone:
    </span></b></div> 
    </div></td>
    <td>&nbsp;</td>
    <td><span class='MsoNormal' style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal;font-family:Verdana;font-size: 12px;'><span class='MsoNormal' style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal; font-family:Verdana;font-size: 12px; '>$ticket_cellphone_contact</span> </span></td>
  </tr>
  <tr>
    <td><div align='right'><b style='mso-bidi-font-weight:bold;font-family:Verdana;font-size: 12px;'><span style='color:#000000;
  mso-themecolor:text2'>CC:
    </span></b></div>      <b style='mso-bidi-font-weight:bold;font-family:Verdana;font-size: 12px;'><span style='color:#000000;
  mso-themecolor:text2'><o:p></o:p>
    </span></b></td>
    <td>&nbsp;</td>
    <td><span class='MsoNormal' style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal;font-family:Verdana;font-size: 12px;'> $cc </span></td>
    <td>&nbsp;</td>
    <td><div align='right' class='style2'><div align='right'><b style='mso-bidi-font-weight:bold; font-family:Verdana;font-size: 12px;'><span style='color:#000000;
  mso-themecolor:text2'>E-Mail:
    </span></b></div> </div></td>
    <td>&nbsp;</td>
    <td><span class='MsoNormal' style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal;font-family:Verdana;font-size: 12px;'><span class='MsoNormal' style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal; font-family:Verdana;font-size: 12px; '>$ticket_email_contact</span> </span></td>
  </tr>
  <tr>
    <td><div align='right'><b style='mso-bidi-font-weight:bold; font-family:Verdana;font-size: 12px;'><span style='color:#000000;
  mso-themecolor:text2'>Created:
    </span></b></div>      <b style='mso-bidi-font-weight:normal'><span style='color:#000000;
  mso-themecolor:text2'><o:p></o:p>
    </span></b></td>
    <td>&nbsp;</td>
    <td><span class='MsoNormal' style='margin-bottom:0in;margin-bottom:.0001pt;line-height:

  normal;font-family:Verdana;font-size: 12px;'> $date </span></td>
    <td>&nbsp;</td>
    <td><div align='right'></div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align='right'><b style='mso-bidi-font-weight:bold;font-family:Verdana;font-size: 12px;'><span style='color:#000000;
  mso-themecolor:text2'>Title:
    </span></b></div>      <b style='mso-bidi-font-weight:bold;font-family:Verdana;font-size: 12px;'><span style='color:#000000;
  mso-themecolor:text2'><o:p></o:p>
    </span></b></td>
    <td>&nbsp;</td>
    <td><span class='MsoNormal' style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal;font-family:Verdana;font-size: 12px;'> $ticket_title 
        <o:p></o:p>
    </span></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td valign='top'><div align='right'><b style='mso-bidi-font-weight:bold;font-family:Verdana;font-size: 12px;'><span style='color:#000000;
  mso-themecolor:text2'>
    </span></b></div>      
      <b style='mso-bidi-font-weight:bold;font-family:Verdana;font-size: 12px;'><span style='color:#000000;
  mso-themecolor:text2'><o:p></o:p>
    </span></b></td>
    <td>&nbsp;</td>
    <td><span class='MsoNormal' style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal;font-family:Verdana;font-size: 12px;'>  </span></td>
    <td>&nbsp;</td>
    <td><div align='right'><b style='mso-bidi-font-weight:bold; font-family:Verdana;font-size: 12px;'><span style='color:#000000;
  mso-themecolor:text2'>No. Ticket: </span></b></div></td>
    <td>&nbsp;</td>
    <td><span class='MsoNormal' style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal; font-family:Verdana;font-size: 12px; '># $ticket </span></td>
  </tr>
</table>
<p class=MsoNormal><o:p>&nbsp;</o:p></p>
 <img src='http://www.elettric80inc.com/Convergence/images/email_btom.jpg' width='1000' /> <BR>
<p class=MsoNormal><span style='font-size:7.0pt;line-height:115%;font-family:Verdana;'>Go and check
the ticket <a href='http://192.168.2.3/ticket.php?Id=$ticket'>here</a>. Make sure you are in the <span
class=GramE>Intranet,</span> otherwise get connected to the network throughout
VPN.<o:p></o:p> </span></p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

</div>

</body>



";

return $text;

}

function send_mail_ticket($ticket_title,$ticket_creator,$ticket_status,$ticket_priority,$ticket_CC,$ticket_assignee,$ticket_customer,$ticket_date_creation,$ticket_name_contact,$ticket_cellphone_contact,$ticket_email_contact, $ticket) 
{	

include("connection.php");

$body = create_email_format($ticket_title,$ticket_creator,$ticket_status,$ticket_priority,$ticket_CC,$ticket_assignee,$ticket_customer,$ticket_date_creation,$ticket_name_contact,$ticket_cellphone_contact,$ticket_email_contact,$ticket);

$altbody= create_email_format($ticket_title,$ticket_creator,$ticket_status,$ticket_priority,$ticket_CC,$ticket_assignee,$ticket_customer,$ticket_date_creation,$ticket_name_contact,$ticket_cellphone_contact,$ticket_email_contact,$ticket);

/// En esta parte se selecciona el Email para envuar el email...

$query_assignee = "select * from Employees where Id=$ticket_assignee";
$result_query_assignee = sqlsrv_query($conn,$query_assignee);
$row_query_assignee = sqlsrv_fetch_array($result_query_assignee);

//$ticket_email_contact
//send_mail($row_query_assignee[7],$ticket_email_contact, $row_query_assignee[1], "Ticket No. ".$ticket, $body, $altbody);
//$email_ticket = "hermida.j@elettric80.it";
$email_ticket = $row_query_assignee[7];

send_mail($email_ticket,$ticket_email_contact, $row_query_assignee[1], "Ticket No. ".$ticket, $body, $altbody);

}

//$cc = "jherq@hotmail.com";


function send_mail($email,$ticket_email_contact, $name, $subject, $body, $altbody) {
	require("../customerservice/phpmailer/class.phpmailer.php");
	$mail = new PHPMailer(); 
	$mail->IsSMTP(); // send via SMTP
	//IsSMTP(); // send via SMTP
	$mail->SMTPAuth = false; // turn on SMTP authentication
	
	 //$mail->Username = "convergence.noreply@gmail.com"; // SMTP username
	 //$mail->Password = "Elettric80Inc"; // SMTP password
	 //$webmaster_email = "HelpDeskINC@elettric80.it"; //Reply to this email ID

	 $mail->Username = "HelpDeskINC"; // SMTP username
	 $mail->Password = "StandardE80"; // SMTP password
	 $webmaster_email = "HelpDeskINC@elettric80.it"; //Reply to this email ID


	
	//$email="checconrg@hotmail.com"; // Recipients email ID
	//$name="Testa di Cazzo"; // Recipient's name
	$mail->From = $webmaster_email;
	$mail->FromName = "Ticket reminder - You have a ticket in process";
	$mail->AddAddress($email,$name);
	
	if ( $_SESSION['email_copy'] == "true") {
	if (empty($ticket_email_contact)) {
	  
	}else {
	$mail->AddCC($ticket_email_contact, 'copia');
	 }
	}
	
	$mail->AddReplyTo($webmaster_email,"Webmaster");
	$mail->WordWrap = 50; // set word wrap
	//$mail->AddAttachment("/var/tmp/file.tar.gz"); // attachment
	//$mail->AddAttachment("/tmp/image.jpg", "new.jpg"); // attachment
	$mail->IsHTML(true); // send as HTML
	
	//$mail->Subject = "This is the subject";
	$mail->Subject = $subject;
	//$mail->Body = "Hi,
//	This is the HTML BODY "; //HTML Body
	$mail->Body = $body;
	//$mail->AltBody = "This is the body when user views in plain text format"; //Text Body
	$mail->AltBody = $altbody;
	
	if(!$mail->Send())
	{
	echo "Mailer Error: " . $mail->ErrorInfo;
	}
	else
	{
	
    $Id_Ticket = $_GET[Id_Ticket];
    $IdTicket = $Id_Ticket + 1; 
    //echo $IdTicket;
	header("Location: reminderok.php?Id_Ticket=$IdTicket");

	}
}
?>