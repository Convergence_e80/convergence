<!--%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

VERSION: 	

- 1.1

COMMENTS:	(add comment describing what has been changed)

-  
- 

DATE:

- 03/14/13

AUTHOR:

- Jose Hermida

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%-->
<?php
ini_set('display_errors','Off');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<link href="favicon.ico" type="image/x-icon" rel="shortcut icon"> 

<link type="text/css" rel="stylesheet" href="style.css" />
<link type="text/css" rel="stylesheet" href="bootstrap.css" />



<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<title>Elettric 80 Inc - Customer Service</title>


	<link rel="stylesheet" href="loading/css/modal-message.css" type="text/css">
	<script type="text/javascript" src="loading/js/ajax.js"></script>
	<script type="text/javascript" src="loading/js/modal-message.js"></script>
	<script type="text/javascript" src="loading/js/ajax-dynamic-content.js"></script>
	<script type="text/javascript" src="js/bootstrap-carousel.js"></script>
	
	
	<link rel="stylesheet" href="buttons.css">
	<style> 
.background1 { 
background-image: url(images/fondo_login.jpg); 
background-repeat: no-repeat; 
} 



.orange {
border-style:solid;
border-width:3px;
border-color:#FE9900;

}

.tb11 {
	background:#FFFFFF no-repeat 4px 4px;
	padding:4px 4px 4px 22px;
	border:1px solid #CCCCCC;
	width:350px;
	height:29px;
	font-size: 13px
}
.tb12 {
	background:#FFFFFF no-repeat 4px 4px;
	padding:4px 4px 4px 22px;
	border:1px solid #CCCCCC;
	width:350px;
	height:150px;
	font-size: 13px
}
a:link {
	color: #000000;
}
a:visited {
	color: #000000;
}
a:hover {
	color: #000000;
}
body {
	background-color: #FFFFFF;
}
    .style4 {font-size: 13px}
    .style5 {font-family: Arial, Helvetica, sans-serif}
    .style7 {font-size: 13px; font-family: Arial, Helvetica, sans-serif; }
    .style8 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 25px;
	color: #0061A8;
}
    .style9 {font-family: Arial, Helvetica, sans-serif; font-size: 12px; }
    .style10 {font-size: 12px}
    .style11 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 15px;
	color: #000000;
}
    </style>	

	
 

<script type="text/javascript">
<!--
<!--
function submitform()
{
  document.form1.submit();
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
</head>

<body onLoad="MM_preloadImages('images/images_top/home_ov.jpg','images/images_top/help_desk_ov.jpg','images/images_top/spare_parts_ov.jpg','images/images_top/maintenance_ov.jpg','images/images_top/products_ov.jpg','images/images_top/training_ov.jpg','images/images_top/contact_ov.jpg')"    >
<table width="950" height="45" border="0" align="center" cellpadding="0" cellspacing="0"  >
  <tr>
    <td width="638"><div align="right"><img src="images/images_top/phone.jpg" width="24" height="24"></div></td>
    <td width="10">&nbsp;</td>
    <td width="140"><span class="style7">USA: +(847) 329-7717</span></td>
    <td width="28"><div align="right"><img src="images/images_top/Mail.jpg" width="24" height="16"></div></td>
    <td width="9">&nbsp;</td>
    <td width="127"><a href="mailto:usahelp@elettric80.it" class="style4 style5">usahelp@elettric80.it</a></td>
  </tr>
</table>
<table width="950" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr>
    <td width="617"><table width="950" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="576"><img src="images/logo_after_sales.jpg" width="538" height="73"></td>
          <td width="31">&nbsp;</td>
          <td width="169">&nbsp;</td>
          <td width="174"><div align="center" class="style7">
		  <a href="#" onClick="displayMessage('loading/login.php');return false"> <br>
            <br>
            <br>
          Login</a> | <a href="request">Request Account</a> </div></td>
        </tr>
      </table>
<script type="text/javascript">
messageObj = new DHTML_modalMessage();	// We only create one object of this class
messageObj.setShadowOffset(5);	// Large shadow

function displayMessage(url)
{ 
	
	messageObj.setSource(url);
	messageObj.setCssClassMessageBox(false);
	messageObj.setSize(400,200);
	messageObj.setShadowDivVisible(true);	// Enable shadow for these boxes
	messageObj.display();
}

function displayStaticMessage(messageContent,cssClass)
{
	messageObj.setHtmlContent(messageContent);
	messageObj.setSize(300,150);
	messageObj.setCssClassMessageBox(cssClass);
	messageObj.setSource(false);	// no html source since we want to use a static message here.
	messageObj.setShadowDivVisible(false);	// Disable shadow for these boxes	
	messageObj.display();
	
	
}



function closeMessage()
{
	messageObj.close();	
}


</script>
	  
    </td>
  </tr>
  <tr>
    <td><div align="center"> <br>
      <table width="944" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="135"><a href="index_new.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('home','','images/images_top/home_ov.jpg',1)"><img src="images/images_top/home.jpg" name="home" width="135" height="55" border="0"></a> </td>
          <td width="134"><a href="help_desk.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('help_desk','','images/images_top/help_desk_ov.jpg',1)"><img src="images/images_top/help_desk.jpg" name="help_desk" width="135" height="55" border="0"></a></td>
          <td width="135"><a href="spare_parts.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('spare_parts','','images/images_top/spare_parts_ov.jpg',1)"><img src="images/images_top/spare_parts.jpg" name="spare_parts" width="135" height="55" border="0"></a></td>
          <td width="135"><a href="maintenance.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('maintenance','','images/images_top/maintenance_ov.jpg',1)"><img src="images/images_top/maintenance.jpg" name="maintenance" width="135" height="55" border="0"></a></td>
          <td width="135"><a href="products.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('products','','images/images_top/products_ov.jpg',1)"><img src="images/images_top/products.jpg" name="products" width="135" height="55" border="0"></a></td>
          <td width="135"><a href="training.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('training','','images/images_top/training_ov.jpg',1)"><img src="images/images_top/training.jpg" name="training" width="135" height="55" border="0"></a></td>
          <td width="135"><a href="contact.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contact','','images/images_top/contact_ov.jpg',1)"><img src="images/images_top/contact.jpg" name="contact" width="135" height="55" border="0"></a></td>
        </tr>
      </table>
      </div></td>
  </tr>
  <tr>
    <td>   </td>
  </tr>
  <tr>
    <td>
	
<table width="946" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td><br>
        <br>
            <br>
            <table width="944" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td><span class="style8">Contact Us <br>
          </span></td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      </table>
              <br>
            <table width="944" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td width="250" valign="top"><div class="style9"><strong>Elettric 80 Inc. - USA <br>
            <br>
            </strong>8100 Monticello Avenue<br>
            Skokie - IL 60076<br>
            USA (north &amp; central America)<br>
            Ph +1 847 329 7717<br>
            Fax +1 847 329 9923<br>
  <a href="mailto:e80.usa@elettric80.it">e80.usa@elettric80.it</a></div></td>
          <td width="603" valign="top"><table width="458" height="346" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td width="103" valign="top"><div align="right" class="style11"></div></td>
              <td width="14">&nbsp;</td>
              <td width="213" valign="top"><div align="right">
                <select name="select" class="tb11">
                  <option>Select Contact</option>
                  <option>After Sales Director</option>
                  <option>After Sales Techn. Manager</option>
                  <option>Help Desk</option>
				  <option>Maintenance</option>
				  <option>Spare Parts</option>
				  
                </select>
              </div></td>
            </tr>
            <tr>
              <td valign="top"><div align="right" class="style11">Name</div></td>
              <td>&nbsp;</td>
              <td valign="top"><div align="right">
                <input name="txtuser" type="text" class="tb11"  id="txtuser" placeholder=" Name " />
              </div></td>
            </tr>
            <tr>
              <td valign="top"><div align="right" class="style11">Email</div></td>
              <td>&nbsp;</td>
              <td valign="top"><div align="right">
                <input name="txtuser2" type="text" class="tb11"  id="txtuser2" placeholder=" Email " />
              </div></td></tr>
            <tr>
              <td valign="top"><div align="right" class="style11">Message</div></td>
              <td>&nbsp;</td>
              <td valign="top"><div align="right">
                <textarea name="txtuser3" rows="5" class="tb12" id="txtuser3" placeholder=" Message "></textarea>
              </div></td>
            </tr>
            <tr>
              <td valign="top">&nbsp;</td>
              <td>&nbsp;</td>
              <td valign="top"><div align="right">
                <input class="button yellow large" name="submit" type="submit" value="Submit">
              </div></td>
            </tr>
          </table></td>
          <td width="47">&nbsp;</td>
        </tr>
      </table>
          <br>
          <br>
          <br></td>
  </tr>
</table>	  </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><img src="images/images_top/bar.jpg" width="948" height="45"></td>
  </tr>
</table>
<table width="816" height="22" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="951" align="center"></td>
  </tr>
  <tr>
    <td align="right" valign="middle"  >
	<br>
<div align="right" >
      <div align="center" class="style5" ><a href="http://www.elettric80.it">www.elettric80.it</a> | &copy;  Elettric 80 Inc  | <strong>
      <?php
          include("contador2.php");
        ?>
      <span id="result_box" lang="en">visits</span></strong><br />
        </div>
    </div></td>
  </tr>
</table>
<script type="text/javascript">
messageObj = new DHTML_modalMessage();	// We only create one object of this class
messageObj.setShadowOffset(5);	// Large shadow

function displayStaticMessage(messageContent,cssClass)
{
	messageObj.setHtmlContent(messageContent);
	messageObj.setSize(300,150);
	messageObj.setCssClassMessageBox(cssClass);
	messageObj.setSource(true);	// no html source since we want to use a static message here.
	messageObj.setShadowDivVisible(false);	// Disable shadow for these boxes	
	messageObj.display();
	
	
}

function closeMessage()
{
	messageObj.close();	
}


</script>
<br />
</body>
</html>
