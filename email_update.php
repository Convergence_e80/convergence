<!--%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

VERSION: 	

- 1.1

COMMENTS:	(add comment describing what has been changed)

-  
- 

DATE:

- 03/14/13

AUTHOR:

- Jose Hermida

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%-->

<?php 
/// Get Email
session_start(); 

$query_email = "select * from Employees where Id = $id_creator";
$result_query_email = sqlsrv_query($conn,$query_email);
$row_query_email = sqlsrv_fetch_array($result_query_email);

//$email_ticket = $row_query_email[7];

$_SESSION['email_ticket']= $row_query_email[7];


//echo "El email es  1".$_SESSION['email_ticket']. "<BR><BR>" ;


$post_content2 = nl2br($post_content);



function create_email_format  (  
                                $id_ticket,
								$ticke,
								$post_content2,
								$author,
								$date
							   ) {
								
    include("../connection.php");

session_start(); 

$query_ticket = "select * from Tickets where Id = '$id_ticket' ";
	$result_query_ticket = sqlsrv_query($conn,$query_ticket);
	$row_query_ticket = sqlsrv_fetch_array($result_query_ticket);


	  $ticket_title = $row_query_ticket[1];
	  $ticket_post = $row_query_ticket[2];
	  $ticket_creator = $row_query_ticket[3];
	  $ticket_status = $row_query_ticket[4]; 
	  $ticket_priority = $row_query_ticket[5];
	  $ticket_CC = $row_query_ticket[6];	  
	  $ticket_assignee = $row_query_ticket[7];
	  $ticket_customer = $row_query_ticket[8];
	  $date_update = $row_query_ticket[10];
	  $ticket_name_contact = $row_query_ticket[11];
	  $ticket_cellphone_contact = $row_query_ticket[12];
	  $ticket_email_contact = $row_query_ticket[13];	

$creator = $_SESSION["user"];

$query_author = "select * from Employees where Id=$ticket_creator";
$result_query_author = sqlsrv_query($conn,$query_author);
$row_query_author = sqlsrv_fetch_array($result_query_author);

session_start();
$author = $_SESSION["user"];

$query_status = "select * from Ticket_Status where Id=$ticket_status";
$result_query_status = sqlsrv_query($conn,$query_status);
$row_query_status = sqlsrv_fetch_array($result_query_status);

$status=$row_query_status[1];

$query_priority = "select * from Priority where Id=$ticket_priority";
$result_query_priority = sqlsrv_query($conn,$query_priority);
$row_query_priority = sqlsrv_fetch_array($result_query_priority);

$priority = $row_query_priority[1];

$query_customer = "select * from Customers where Id=$ticket_customer";
$result_query_customer = sqlsrv_query($conn,$query_customer);
$row_query_customer = sqlsrv_fetch_array($result_query_customer);

$customer = $row_query_customer[1];

$query_cc = "select * from CustomersEquipment where Id=$ticket_CC";
$result_query_cc = sqlsrv_query($conn,$query_cc);
$row_query_cc = sqlsrv_fetch_array($result_query_cc);

$cc = $row_query_cc[2];

ob_start();

?> 

<body>

  <p class='style2'>The purpose of this email is to communicate a new update for  the ticket shown below. Please review the information shown below, and contact  us if you have any questions, comments, or any additional information regarding  this ticket. Please also feel free to contact the assigned technical support  specialist shown below, or the help desk at <a href='mailto:usahelp@elettric80.it'>usahelp@elettric80.it</a>, to arrange any activities such as  equipment/system tests, to modify implementation plans, or if the technical  support specialist assigned has requested any further information.</p>

  <table cellspacing="0" cellpadding="0" style="border: none;width: 100%;">
    <tr class="header">
        <td colspan="2" style="font-family: Arial;font-size: 12px;border-bottom: 5px solid #FF9834;padding-bottom: 20px;"><img class="logo" src="http://www.elettric80inc.com/Convergence/images/email_top.jpg"></td><td style="font-family: Arial;font-size: 12px;border-bottom: 5px solid #FF9834;padding-bottom: 20px;">
    </td></tr>
    <tr class="row">
      <td class="label" valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px;width: 20%;height: 20px;"><div style="background-color: rgb(249, 232, 203);border: 1px solid rgb(237, 218, 198);color: rgb(168, 131, 90);text-align: center;font-size: 10px;font-weight: bold;">Ticket No.</div></td>
      <td class="content" style="font-family: Arial;font-size: 12px;padding: 5px;width: 80%;"># <?php echo isset($id_ticket) ? $id_ticket : "-"; ?> </td>
    </tr>
    <tr class="row">
      <td class="label" valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px;width: 20%;height: 20px;"><div style="background-color: rgb(249, 232, 203);border: 1px solid rgb(237, 218, 198);color: rgb(168, 131, 90);text-align: center;font-size: 10px;font-weight: bold;">Assignee</div></td>
      <td class="content" style="font-family: Arial;font-size: 12px;padding: 5px;width: 80%;"> <?php echo isset($author) ? $author : "-"; ?> </td>
    </tr>
    <tr class="row">
      <td class="label" valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px;width: 20%;height: 20px;"><div style="background-color: rgb(249, 232, 203);border: 1px solid rgb(237, 218, 198);color: rgb(168, 131, 90);text-align: center;font-size: 10px;font-weight: bold;">Status</div></td>
      <td class="content" style="font-family: Arial;font-size: 12px;padding: 5px;width: 80%;"> <?php echo isset($status) ? $status : "-"; ?> </td>
    </tr>
    <tr class="row">
      <td class="label" valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px;width: 20%;height: 20px;"><div style="background-color: rgb(249, 232, 203);border: 1px solid rgb(237, 218, 198);color: rgb(168, 131, 90);text-align: center;font-size: 10px;font-weight: bold;">Priority</div></td>
      <td class="content" style="font-family: Arial;font-size: 12px;padding: 5px;width: 80%;"> <?php echo isset($priority) ? $priority : "-"; ?> </td>
    </tr>
    <tr class="row">
      <td class="label" valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px;width: 20%;height: 20px;"><div style="background-color: rgb(249, 232, 203);border: 1px solid rgb(237, 218, 198);color: rgb(168, 131, 90);text-align: center;font-size: 10px;font-weight: bold;">Customer</div></td>
      <td class="content" style="font-family: Arial;font-size: 12px;padding: 5px;width: 80%;"> <?php echo isset($customer) ? $customer : "-"; ?> </td>
    </tr>
    <tr class="row">
      <td class="label" valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px;width: 20%;height: 20px;"><div style="background-color: rgb(249, 232, 203);border: 1px solid rgb(237, 218, 198);color: rgb(168, 131, 90);text-align: center;font-size: 10px;font-weight: bold;">Title</div></td>
      <td class="content" style="font-family: Arial;font-size: 12px;padding: 5px;width: 80%;"> <?php echo isset($ticke) ? $ticke : "-"; ?> </td>
    </tr>
    <tr class="row ticket_content" bgcolor="#F9E8CB">
      <td valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px 3px 5px 3px;">
        <center>
          <img src="http://www.elettric80inc.com/convergence/images/ticket.png" width="70px">
        </center>
      </td>
      <td style="font-family: Arial;font-size: 12px;padding: 5px 3px 5px 3px;">
        <?php echo isset($ticket_post) ? $ticket_post : "-"; ?>
      </td>
    </tr>
   <tr class="row ticket_post" bgcolor="#CBE1F9">
      <td valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px 3px 5px 3px;">
        <center>
            <img src="http://www.elettric80inc.com/convergence/images/arrow.png" width="70px">
        </center>
      </td>
      <td style="font-family: Arial;font-size: 12px;padding: 5px 3px 5px 3px;">
        <?php echo isset($post_content2) ? $post_content2 : "-"; ?>
      </td>
    </tr>
    <tr class="row">
      <td class="label" valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px;width: 20%;height: 20px;"><div style="background-color: rgb(249, 232, 203);border: 1px solid rgb(237, 218, 198);color: rgb(168, 131, 90);text-align: center;font-size: 10px;font-weight: bold;">Contact Name</div></td>
      <td class="content" style="font-family: Arial;font-size: 12px;padding: 5px;width: 80%;"> <?php echo isset($ticket_name_contact) ? $ticket_name_contact : "-"; ?> </td>
    </tr>
    <tr class="row">
      <td class="label" valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px;width: 20%;height: 20px;"><div style="background-color: rgb(249, 232, 203);border: 1px solid rgb(237, 218, 198);color: rgb(168, 131, 90);text-align: center;font-size: 10px;font-weight: bold;">Contact Phone</div></td>
      <td class="content" style="font-family: Arial;font-size: 12px;padding: 5px;width: 80%;"><?php echo isset($ticket_cellphone_contact) ? $ticket_cellphone_contact : "-"; ?></td>
    </tr>
    <tr class="row">
      <td class="label" valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px;width: 20%;height: 20px;"><div style="background-color: rgb(249, 232, 203);border: 1px solid rgb(237, 218, 198);color: rgb(168, 131, 90);text-align: center;font-size: 10px;font-weight: bold;">Contact Email</div></td>
      <td class="content" style="font-family: Arial;font-size: 12px;padding: 5px;width: 80%;">
        <a href="mailto:$cc?>"><?php echo isset($cc) ? $cc : "-"; ?></a> 
      </td>
    </tr>
    <tr class="footer">
      <td colspan="2" style="font-family: Arial;font-size: 12px;border-top: 5px solid #FF9834;padding-top: 20px;">
        Go and check/reply the ticket <a href="http://www.elettric80inc.com/customerservice/help_desk_ticket.php?Id=<?php echo isset($ticket) ? $ticket : ''; ?>">here</a>.
        Make sure you are in the Intranet, otherwise get connected to the network throughout VPN. 
      </td>
    
  </tr></table>
</body>

<?php 

$text = ob_get_clean();

return $text;

}

function send_mail_ticket(      
                                $id_ticket,
								$post_content2,
								$author,
								$date) 
{	

    include("../connection.php");

$body = create_email_format(    
                                $id_ticket,
								$post_content2,
								$author,
								$date);

$altbody= create_email_format(  
                                $id_ticket,
								$post_content2,
								$author,
								$date);

$query_assignee = "select * from Employees where Id=$author";

$result_query_assignee = sqlsrv_query($conn,$query_assignee);
$row_query_assignee = sqlsrv_fetch_array($result_query_assignee);









//send_mail("hermida.j@elettric80.it", $row_query_assignee[1], "You've got a new update for ticket # $id_ticket " , $body, $altbody);

echo "La session es: ".$_SESSION['email_ticket'];

send_mail($_SESSION['email_ticket'], $row_query_assignee[1], "You've got a new update for ticket # $id_ticket " , $body, $altbody);



}

//$email_ticket = "hermida.j@elettric80.it";

function send_mail($email_ticket, $name, $subject, $body, $altbody) {




	//require("../convergence/phpmailer/class.phpmailer.php");
	require_once('../customerservice/phpmailer/class.phpmailer.php');
	
	//echo "Es: ".$email_ticket;
	
	$mail = new PHPMailer(); 
	
	$mail->IsSMTP(); // send via SMTP
	
	//IsSMTP(); // send via SMTP
    $mail->Host       = "127.0.0.1"; // SMTP server
    $mail->SMTPDebug  = 1;                     // enables SMTP debug information (for testing)
	
	$mail->SMTPAuth = false; // turn on SMTP authentication
	
	
	//$mail->SetFrom('helpdesk@elettric80.it', 'Help Desk E80');

//	 $mail->Username = "helpdesk"; // SMTP username
//	 $mail->Password = "StandardE80"; // SMTP password
//	 $webmaster_email = "helpdesk@elettric80inc.com"; //Reply to this email ID
	//$email="checconrg@hotmail.com"; // Recipients email ID
	//$name="Testa di Cazzo"; // Recipient's name
	$mail->From = $email_ticket;
	$mail->FromName = "Elettric80 - Ticketing System";
	
	//$1email = 'jherq@hotmail.com, pepe.hermida@gmail.com, hermida.j@elettric80.it';

    //$email_ticket =  $1email;

    //$mail->AddAddress($email_ticket,"Subject");
	
	
	 $mail->AddAddress($email_ticket,$name);
	
	///////////////  Copy Post - Start ///////////////////////
    if ( $_SESSION['copy_post'] == "on") {
	

	$customer_email = $_SESSION['email_customer'];
 	$mail->AddCC($customer_email, 'copia');

	}
	///////////////  Copy Post - End ///////////////////////	
	
	///////////////  Copy Post - Start ///////////////////////
    if ( $_SESSION['copy_post_2'] == "on") {
     
	 $Account_manager_email = $_SESSION['email_customer_2'];
	 //echo $Account_manager_email;
	 $mail->AddBCC($Account_manager_email, 'Account Manager');
 
     // More than one BCC, just keep adding them!
     //$mailer->AddBCC('recipient2@domain.com', 'Second Person');
     //$mailer->AddBCC('recipient3@domain.com', 'Third Person');

    
	}
	
	 session_start();
	//$mail->AddReplyTo($webmaster_email,"Webmaster");
	
	$mail->WordWrap = 50; // set word wrap
	
	$mail->AddAttachment("uploads/posts_documents/".$_SESSION['file1']); // attachment
	$mail->AddAttachment("uploads/posts_documents/".$_SESSION['file2']); // attachment
	$mail->AddAttachment("uploads/posts_documents/".$_SESSION['file3']); // attachment
	$mail->AddAttachment("uploads/posts_documents/".$_SESSION['file4']); // attachment
				
	//echo "File is:  ".$_SESSION['file'];
	
	//$mail->AddAttachment("/tmp/image.jpg", "new.jpg"); // attachment
	$mail->IsHTML(true); // send as HTML
	//$mail->Subject = "This is the subject";
	$mail->Subject = $subject;
	//$mail->Body = "Hi,
//	This is the HTML BODY "; //HTML Body
	$mail->Body = $body;
	//$mail->AltBody = "This is the body when user views in plain text format"; //Text Body
	$mail->AltBody = $altbody;
	//echo "El email es".$email_ticket;
	if(!$mail->Send())
	{
	echo "Mailer Error: " . $mail->ErrorInfo;
	}
	else
	{
	echo "<script>alert('an emailll notification has been sent to ".$email_ticket."');</script>";
	}
}
?>