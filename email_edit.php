<!--%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

VERSION:  

- 1.1

COMMENTS: (add comment describing what has been changed)

-  
- 

DATE: 

- 03/14/13

AUTHOR:
 
- Jose Hermida

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%-->

<?php 

function create_email_format ($ticket_title,$ticket_post,$ticket_creator,$ticket_status,$ticket_priority,$ticket_CC,$ticket_assignee,$ticket_customer,$ticket_date_creation,$ticket_name_contact,$ticket_cellphone_contact,$ticket_email_contact, $ticket) {

include("connection.php");

$query_author = "select * from Employees where Id=$ticket_creator";
$result_query_author = sqlsrv_query($conn,$query_author);
$row_query_author = sqlsrv_fetch_array($result_query_author);

session_start();
$author = $_SESSION["user"];

$query_status = "select * from Ticket_Status where Id=$ticket_status";
$result_query_status = sqlsrv_query($conn,$query_status);
$row_query_status = sqlsrv_fetch_array($result_query_status);

$status=$row_query_status[1];

$query_priority = "select * from Priority where Id=$ticket_priority";
$result_query_priority = sqlsrv_query($conn,$query_priority);
$row_query_priority = sqlsrv_fetch_array($result_query_priority);

$priority = $row_query_priority[1];

$query_customer = "select * from Customers where Id=$ticket_customer";
$result_query_customer = sqlsrv_query($conn,$query_customer);
$row_query_customer = sqlsrv_fetch_array($result_query_customer);

$customer = $row_query_customer[1];

$query_cc = "select * from CustomersEquipment where Id=$ticket_CC";
$result_query_cc = sqlsrv_query($conn,$query_cc);
$row_query_cc = sqlsrv_fetch_array($result_query_cc);

$cc = $row_query_cc[2];

$date = $ticket_date_creation;


ob_start();

?> 

<body>

  This ticket has been updated.

  <table cellspacing="0" cellpadding="0" style="border: none;width: 100%;">
    <tr class="header">
        <td colspan="2" style="font-family: Arial;font-size: 12px;border-bottom: 5px solid #FF9834;padding-bottom: 20px;"><img class="logo" src="http://www.elettric80inc.com/Convergence/images/email_top.jpg"></td><td style="font-family: Arial;font-size: 12px;border-bottom: 5px solid #FF9834;padding-bottom: 20px;">
    </td></tr>
    <tr class="row">
      <td class="label" valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px;width: 20%;height: 20px;"><div style="background-color: rgb(249, 232, 203);border: 1px solid rgb(237, 218, 198);color: rgb(168, 131, 90);text-align: center;font-size: 10px;font-weight: bold;">Ticket No.</div></td>
      <td class="content" style="font-family: Arial;font-size: 12px;padding: 5px;width: 80%;"># <?php echo isset($ticket) ? $ticket : "-"; ?> </td>
    </tr>
    <tr class="row">
      <td class="label" valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px;width: 20%;height: 20px;"><div style="background-color: rgb(249, 232, 203);border: 1px solid rgb(237, 218, 198);color: rgb(168, 131, 90);text-align: center;font-size: 10px;font-weight: bold;">Assignee</div></td>
      <td class="content" style="font-family: Arial;font-size: 12px;padding: 5px;width: 80%;"> <?php echo isset($author) ? $author : "-"; ?> </td>
    </tr>
    <tr class="row">
      <td class="label" valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px;width: 20%;height: 20px;"><div style="background-color: rgb(249, 232, 203);border: 1px solid rgb(237, 218, 198);color: rgb(168, 131, 90);text-align: center;font-size: 10px;font-weight: bold;">Status</div></td>
      <td class="content" style="font-family: Arial;font-size: 12px;padding: 5px;width: 80%;"> <?php echo isset($status) ? $status : "-"; ?> </td>
    </tr>
    <tr class="row">
      <td class="label" valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px;width: 20%;height: 20px;"><div style="background-color: rgb(249, 232, 203);border: 1px solid rgb(237, 218, 198);color: rgb(168, 131, 90);text-align: center;font-size: 10px;font-weight: bold;">Priority</div></td>
      <td class="content" style="font-family: Arial;font-size: 12px;padding: 5px;width: 80%;"> <?php echo isset($priority) ? $priority : "-"; ?> </td>
    </tr>
    <tr class="row">
      <td class="label" valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px;width: 20%;height: 20px;"><div style="background-color: rgb(249, 232, 203);border: 1px solid rgb(237, 218, 198);color: rgb(168, 131, 90);text-align: center;font-size: 10px;font-weight: bold;">Customer</div></td>
      <td class="content" style="font-family: Arial;font-size: 12px;padding: 5px;width: 80%;"> <?php echo isset($customer) ? $customer : "-"; ?> </td>
    </tr>
    <tr class="row">
      <td class="label" valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px;width: 20%;height: 20px;"><div style="background-color: rgb(249, 232, 203);border: 1px solid rgb(237, 218, 198);color: rgb(168, 131, 90);text-align: center;font-size: 10px;font-weight: bold;">Created</div></td>
      <td class="content" style="font-family: Arial;font-size: 12px;padding: 5px;width: 80%;"> <?php echo isset($date) ? $date : "-"; ?> </td>
    </tr>
    <tr class="row">
      <td class="label" valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px;width: 20%;height: 20px;"><div style="background-color: rgb(249, 232, 203);border: 1px solid rgb(237, 218, 198);color: rgb(168, 131, 90);text-align: center;font-size: 10px;font-weight: bold;">Title</div></td>
      <td class="content" style="font-family: Arial;font-size: 12px;padding: 5px;width: 80%;"> <?php echo isset($ticket_title) ? $ticket_title : "-"; ?> </td>
    </tr>
    <tr class="row ticket_content" bgcolor="#F9E8CB">
      <td valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px 3px 5px 3px;">
        <center>
          <img src="http://www.elettric80inc.com/convergence/images/ticket.png" width="70px">
        </center>
      </td>
      <td style="font-family: Arial;font-size: 12px;padding: 5px 3px 5px 3px;">
        <?php echo isset($ticket_post) ? $ticket_post : "-"; ?>
      </td>
    </tr>
    <tr class="row">
      <td class="label" valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px;width: 20%;height: 20px;"><div style="background-color: rgb(249, 232, 203);border: 1px solid rgb(237, 218, 198);color: rgb(168, 131, 90);text-align: center;font-size: 10px;font-weight: bold;">Contact Name</div></td>
      <td class="content" style="font-family: Arial;font-size: 12px;padding: 5px;width: 80%;"> <?php echo isset($ticket_name_contact) ? $ticket_name_contact : "-"; ?> </td>
    </tr>
    <tr class="row">
      <td class="label" valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px;width: 20%;height: 20px;"><div style="background-color: rgb(249, 232, 203);border: 1px solid rgb(237, 218, 198);color: rgb(168, 131, 90);text-align: center;font-size: 10px;font-weight: bold;">Contact Phone</div></td>
      <td class="content" style="font-family: Arial;font-size: 12px;padding: 5px;width: 80%;"><?php echo isset($ticket_cellphone_contact) ? $ticket_cellphone_contact : "-"; ?></td>
    </tr>
    <tr class="row">
      <td class="label" valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px;width: 20%;height: 20px;"><div style="background-color: rgb(249, 232, 203);border: 1px solid rgb(237, 218, 198);color: rgb(168, 131, 90);text-align: center;font-size: 10px;font-weight: bold;">Contact Email</div></td>
      <td class="content" style="font-family: Arial;font-size: 12px;padding: 5px;width: 80%;">
        <a href="mailto:<?php echo $ticket_email_contact; ?>"><?php echo isset($ticket_email_contact) ? $ticket_email_contact : "-"; ?></a> 
      </td>
    </tr>
    <tr class="footer">
      <td colspan="2" style="font-family: Arial;font-size: 12px;border-top: 5px solid #FF9834;padding-top: 20px;">
        Go and check/reply the ticket <a href="http://www.elettric80inc.com/customerservice/help_desk_ticket.php?Id=<?php echo isset($ticket) ? $ticket : ''; ?>">here</a>.
        Make sure you are in the Intranet, otherwise get connected to the network throughout VPN. 
      </td>
    
  </tr></table>
</body>

<?php 

$text = ob_get_clean();

return $text;

}

function send_mail_ticket($ticket_title,$ticket_post,$ticket_creator,$ticket_status,$ticket_priority,$ticket_CC,$ticket_assignee,$ticket_customer,$ticket_date_creation,$ticket_name_contact,$ticket_cellphone_contact,$ticket_email_contact, $ticket) 
{ 

include("connection.php");

$body = create_email_format($ticket_title,$ticket_post,$ticket_creator,$ticket_status,$ticket_priority,$ticket_CC,$ticket_assignee,$ticket_customer,$ticket_date_creation,$ticket_name_contact,$ticket_cellphone_contact,$ticket_email_contact,$ticket);

$altbody= create_email_format($ticket_title,$ticket_post,$ticket_creator,$ticket_status,$ticket_priority,$ticket_CC,$ticket_assignee,$ticket_customer,$ticket_date_creation,$ticket_name_contact,$ticket_cellphone_contact,$ticket_email_contact,$ticket);

$query_assignee = "select * from Employees where Id=$ticket_assignee";
$result_query_assignee = sqlsrv_query($conn,$query_assignee);
$row_query_assignee = sqlsrv_fetch_array($result_query_assignee);

//$ticket_email_contact
send_mail($row_query_assignee[7],$ticket_email_contact, $row_query_assignee[1],"Ticket No. ".$ticket, $body, $altbody);
}



function send_mail($email,$ticket_email_contact, $name, $subject, $body, $altbody) {
  require_once('../customerservice/phpmailer/class.phpmailer.php');
  $mail = new PHPMailer(); 
  $mail->IsSMTP(); // send via SMTP
  //IsSMTP(); // send via SMTP
  $mail->SMTPAuth = false; // turn on SMTP authentication
  
   $mail->Username = "HelpDeskINC"; // SMTP username
   $mail->Password = "StandardE80"; // SMTP password
   $webmaster_email = "HelpDeskINC@elettric80.it"; //Reply to this email ID

  
  //$email="checconrg@hotmail.com"; // Recipients email ID
  //$name="Testa di Cazzo"; // Recipient's name
  $mail->From = $webmaster_email;
  $mail->FromName = "Elettric80 - Ticketing System";
  $mail->AddAddress($email,$name);
  
  if ( $_SESSION['email_copy'] == "true") {
  if (empty($ticket_email_contact)) {
    
  }else {
  $mail->AddCC($ticket_email_contact, 'copia');
   }
  }
  
  $mail->AddReplyTo($webmaster_email,"Webmaster");
  $mail->WordWrap = 50; // set word wrap
  //$mail->AddAttachment("/var/tmp/file.tar.gz"); // attachment
  //$mail->AddAttachment("/tmp/image.jpg", "new.jpg"); // attachment
  $mail->IsHTML(true); // send as HTML
  
  //$mail->Subject = "This is the subject";
  $mail->Subject = $subject;
  //$mail->Body = "Hi,
//  This is the HTML BODY "; //HTML Body
  $mail->Body = $body;
  //$mail->AltBody = "This is the body when user views in plain text format"; //Text Body
  $mail->AltBody = $altbody;
  
  if(!$mail->Send())
  {
  echo "Mailer Error: " . $mail->ErrorInfo;
  }
  else
  {
  echo "<script>alert('an email notification has been sent to ".$email."');</script>";
  }
}
?>