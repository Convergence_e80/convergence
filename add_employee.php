<!--%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

VERSION: 	

- 1.1

COMMENTS:	(add comment describing what has been changed)

-  
- 

DATE:

- 03/14/13

AUTHOR:

- Jose Hermida

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%-->

<?php
include("check_connection.php");	// import file for checking the session of the login


?>

<html>
<head>
<link href="favicon.ico" type="image/x-icon" rel="shortcut icon">
<link type="text/css" rel="stylesheet" href="style.css" />

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Elettric 80 Inc - Data Base</title>

<script language="JavaScript">
var amarillo="#FFFF00",blanco="#FFFFFF",color="";
var CVALIDAR=new Array(0,1);
function A(f)
{
var txAlerta=" Not empty fields ";
var err=0;
for(var q=0;q<CVALIDAR.length;q++)
 {
 color=blanco;
 if(f[CVALIDAR[q]].value.length<=0)
  {
  color=amarillo;
  err++;
  txAlerta;
  }
 f[CVALIDAR[q]].style.backgroundColor=color;
 }
if(err<=0) {/*envia el formulario*/ document.a.submit() }
else {/*hay campos vac�os*/alert(txAlerta);}
}
</script>


</head>

<body>
<br />
<?php include_once('header.php');?>

<table width="850" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="images/borde_arriba.jpg" width="850" height="20" /></td>
  </tr>
</table>
<table width="850" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="4"  background="images/borde_izq.jpg" background-repeat: repeat-y; >&nbsp;</td>
    <td width="842" bgcolor="#FFFFFF"><div align="center">
        <?php 
	         $user = $_GET[Id];
             $tsql_em = "select * from Employees WHERE Id = '$user'";
             $result_emp = sqlsrv_query( $conn, $tsql_em);
			 $row_emp = sqlsrv_fetch_array($result_emp);
       ?>
      <br />
      <table width="817" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="585"><a href="employees.php"><img src="images/employees_top.jpg" border="0" /></a></td>
          <td width="220">
		  <BR>
		  <form id="form1" name="form1" method="get" action="employees_search.php">
            <input name="search_user" type="text"  id="search_user" />
              <input name="search" type="submit"  id="search" value="Search" />
          </form></td>
          <td width="12">&nbsp;</td>
        </tr>
      </table>
      </div>
      <br />
      <table width="800" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="22" ></td>
        <td width="37" valign="top" >&nbsp;</td>
        <td width="632" >
		
		
     <form id="form2" name="a" method="post" action="add_employee_save.php" enctype="multipart/form-data" >
       <table width="615" height="20" border="0" align="center" cellpadding="0" cellspacing="0">
         <tr>
           <td><strong>User Information _____________________________________________________________ </strong></td>
         </tr>
       </table>
       <br>
       <table width="615" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td width="164"><div align="right">Name:</div></td>
            <td width="17">&nbsp;</td>
            <td width="526" ><input name="emp_name" type="text"  id="emp_name" size="40" maxlength="50" /></td>
          </tr>
          <tr>
            <td><div align="right"></div></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="right">Department: </div></td>
            <td>&nbsp;</td>
            <td>
			
			<select name="emp_dep"  id="emp_dep">
        <?php 
             $tsql_em_dep = "SELECT * FROM Employee_Departments ORDER BY Department";
             $result_emp_dep = sqlsrv_query( $conn, $tsql_em_dep);
			 //$row_emp_dep = sqlsrv_fetch_array($result_emp_dep);

            while($row_emp_dep = sqlsrv_fetch_array($result_emp_dep)) {
       ?>
            <option value="<?php echo $row_emp_dep[0]; ?>">  <?php echo $row_emp_dep[1]; ?> </option>
	   <?php
		    }
	   ?>
            </select>	            </td>
          </tr>
          <tr>
            <td><div align="right"></div></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="right">Title:</div></td>
            <td>&nbsp;</td>
            <td>
			
			<select name="emp_tit"  id="emp_tit">
        <?php 
             $tsql_em_dep = "select * from Employee_Titles order by 'Title'";
             $result_emp_dep = sqlsrv_query( $conn, $tsql_em_dep);
			 //$row_emp_dep = sqlsrv_fetch_array($result_emp_dep);

            while($row_emp_dep = sqlsrv_fetch_array($result_emp_dep)) {
       ?>
            <option value="<?php echo $row_emp_dep[0]; ?>">  <?php echo $row_emp_dep[1]; ?> </option>
	   <?php
		    }
	   ?>
            </select>			</td>
          </tr>
          <tr>
            <td><div align="right"></div></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="right">Phone:</div></td>
            <td>&nbsp;</td>
            <td><span >
              <input name="emp_pho" type="text"  id="emp_pho" />
            </span></td>
          </tr>
          <tr>
            <td><div align="right"></div></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="right">Extension:</div></td>
            <td>&nbsp;</td>
            <td><span >
              <input name="emp_ext" type="text"  id="emp_ext" maxlength="3" />
            </span></td>
          </tr>
          <tr>
            <td><div align="right"></div></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="right">Speed Dial: </div></td>
            <td>&nbsp;</td>
            <td><span >
              <input name="emp_dial" type="text"  id="emp_dial" maxlength="3" />
            </span></td>
          </tr>
          <tr>
            <td><div align="right"></div></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="right">E-mail:</div></td>
            <td>&nbsp;</td>
            <td><span >
              <input name="emp_email" type="text"  id="emp_email" />
            </span></td>
          </tr>
        </table>
          
          <br>
          <table width="615" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td width="143"><div align="right">Division:</div></td>
              <td width="13">&nbsp;</td>
              <td width="286"><select name="Division"  id="Division"  >
                  <?php 
			   
			 $tsql_system_type = "SELECT * FROM System_Type ORDER BY Id";
             $result_system_type = sqlsrv_query( $conn, $tsql_system_type);
				
             
			 //$row_emp_dep = sqlsrv_fetch_array($result_emp_dep);

				while($row_system_type = sqlsrv_fetch_array($result_system_type)) {
		   ?>
                  <option value="<?php echo $row_system_type[1]; ?>" 
			   <?php //if ($row_emp_dep[0]==$row_emp[2]) echo "selected"; ?>> <?php echo $row_system_type[1]; ?> </option>
                  <?php
				}
		   ?>
              </select></td>
              <td width="110"><div align="right">Team Leader:</div></td>
              <td width="12">&nbsp;</td>
              <td width="51"><div align="center">
                  <input name="Team_Leader" type="checkbox" id="Team_Leader" value="1">
              </div></td>
            </tr>
          </table>
          <br>
          <br>
          <table width="615" height="20" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td><strong>User Level __________________________________________________________________ </strong></td>
            </tr>
          </table>
          <br>
          <table width="615" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td width="130"><div align="right">Edit Customer: </div></td>
              <td width="11">&nbsp;</td>
              <td width="53"><input name="Edit_Customer" type="checkbox" id="Edit_Customer" value="1"></td>
              <td width="17">&nbsp;</td>
              <td width="169"><div align="right">Delete Ticket:</div></td>
              <td width="16">&nbsp;</td>
              <td width="100"><input name="Delete_Ticket" type="checkbox" id="Delete_Ticket" value="1"></td>
            </tr>
            <tr>
              <td><div align="right"></div></td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td><div align="right"></div></td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td><div align="right">Edit Employee:</div></td>
              <td>&nbsp;</td>
              <td><input name="Edit_Employee" type="checkbox" id="Edit_Employee" value="1"></td>
              <td>&nbsp;</td>
              <td><div align="right">Edit General Ticket:</div></td>
              <td>&nbsp;</td>
              <td><input name="Edit_General_Ticket" type="checkbox" id="Edit_General_Ticket" value="1"></td>
            </tr>
            <tr>
              <td><div align="right"></div></td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td><div align="right"></div></td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td><div align="right">Edit Equipment:</div></td>
              <td>&nbsp;</td>
              <td><input name="Edit_Equipment" type="checkbox" id="Edit_Equipment" value="1"></td>
              <td>&nbsp;</td>
              <td><div align="right">Edit Reassign Ticket:</div></td>
              <td>&nbsp;</td>
              <td><input name="Edit_Reassign_Ticket" type="checkbox" id="Edit_Reassign_Ticket" value="1"></td>
            </tr>
            <tr>
              <td><div align="right"></div></td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
          </table>
          <br>
          <br>
          <table width="615" height="20" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td><div align="right">
                <input name="save2" type="button"  id="save2" value="Save" onClick="A(this.form);" />
              </div></td>
            </tr>
          </table>
          <br>
     </form>          </td>
        <td width="109" >&nbsp;</td>
      </tr>
    </table>
  
      <br />
    <br /></td>
    <td width="4" background="images/borde_der.jpg" background-repeat: repeat-y; ></td>
  </tr>
</table>
<table width="850" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="images/borde_abajo.jpg" width="850" height="20" /></td>
  </tr>
</table>
<br />
<table width="850" height="22" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="951" align="center"></td>
  </tr>
  <tr>
    <td align="right" valign="middle"  ><div align="right" >
      <div align="center" > <span >&copy; Elettric 80 Inc Data Base </span><br />
            <br />
      </div>
    </div></td>
  </tr>
</table>
</body>
</html>
