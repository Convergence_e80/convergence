<!--%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

VERSION: 	

- 1.1

COMMENTS:	(add comment describing what has been changed)

-  
- 

DATE:

- 03/14/13

AUTHOR:

- Jose Hermida

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%-->
<?php




?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<link href="../favicon.ico" type="image/x-icon" rel="shortcut icon"> 

<link type="text/css" rel="stylesheet" href="../style.css" />


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<title>Elettric 80 Inc - Ticketing System</title>


	<link rel="stylesheet" href="../loading/css/modal-message.css" type="text/css">
	<script type="text/javascript" src="../loading/js/ajax.js"></script>
	<script type="text/javascript" src="../loading/js/modal-message.js"></script>
	<script type="text/javascript" src="../loading/js/ajax-dynamic-content.js"></script>
	
	<link rel="stylesheet" href="../buttons.css">
	
    <style type="text/css">
<!--
.style1 {
	color: #FF0000;
	font-weight: bold;
}
-->
    </style>

<style> 
.background1 { 
background-image: url(../images/fondo_login.jpg); 
background-repeat: no-repeat; 
} 



.orange {
border-style:solid;
border-width:3px;
border-color:#FE9900;

}

.style2 {
	color: #013E99;
	font-size: 10px;
	font-weight: bold;
}

.tb11 {
	background:#FFFFFF no-repeat 4px 4px;
	padding:4px 4px 4px 22px;
	border:1px solid #CCCCCC;
	width:211px;
	height:20px;
	font-size: 17px
}
a:link {
	color: #000000;
}
a:visited {
	color: #000000;
}
a:hover {
	color: #000000;
}
.style4 {font-size: 13px}
.style5 {font-family: Arial, Helvetica, sans-serif}
.style7 {font-size: 13px; font-family: Arial, Helvetica, sans-serif; }
</style>	

	
<script type="text/JavaScript">
<!--



function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
//-->
</script>

<script type="text/javascript">
<!--
function submitform()
{
  document.form1.submit();
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
</head>

<body onLoad="MM_preloadImages('../images/sing_in_ov.jpg')" >
<p>&nbsp;</p>
<table width="820" height="45" border="0" align="center" cellpadding="0" cellspacing="0"  >
  <tr>
    <td width="495"><div align="right"><img src="../customerservice/images/phone_2.jpg" width="30" height="30"></div></td>
    <td width="10">&nbsp;</td>
    <td width="152"><span class="style7">USA: +(847) 329-7717</span></td>
    <td width="27"><div align="right"><img src="../customerservice/images/Mail.jpg" width="30" height="21"></div></td>
    <td width="8">&nbsp;</td>
    <td width="128"><a href="mailto:usahelp@elettric80.it" class="style4 style5">usahelp@elettric80.it</a></td>
  </tr>
</table>
<br>
<table width="820" align="center" cellpadding="0" cellspacing="0" bgcolor="#F5F5F5" class="orange">
  <tr>
    <td><br>
      <table width="777" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="409"><img src="images/image.jpg" width="489" height="344"></td>
        <td width="35">&nbsp;</td>
        <td width="333"  \><form id="form1" name="form1" method="post" action="login.php">
            <table width="241" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr>
                <td><div align="center"><img src="images/e80logo.jpg" width="236" height="72"></div></td>
              </tr>
            </table>
          <br>
            <br>
            <table width="241" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr>
                <td width="310"><div align="right">
                  <input name="txtuser" type="text" class="tb11"  id="txtuser" value="<?php echo $_COOKIE["cookie_user"];?>" placeholder=" User " />
                </div></td>
              </tr>
              <tr>
                <td><input name="e80" type="hidden" id="e80" value="elettric80\">
                  <br></td>
              </tr>
              <tr>
                <td><div align="right">
                  <input name="txtpass" type="password" class="tb11"  id="txtpass" value="<?php echo $_COOKIE["cookie_password"];?>" size="*" placeholder=" Password " />
                </div></td>
              </tr>
              <tr>
                <td>
                  <table width="132" height="40" border="0" align="right" cellpadding="0" cellspacing="0">
                    <tr>
                      <td width="143"><div align="right"> <br>
                        <input class="button yellow large" name="submit" type="submit" value="Sign in">
					  
					  <span ><span >
                          <?php
	 if ($_GET) { 
	 
		  if ($_GET['error_1']==="yes") {
		  $error_msg="User and Password necessary";}
		  else
		  if ($_GET['error_2']==="yes") {
		  $error_msg="Incorrect User and/or Password"; }
		  else
		  if ($_GET['error_3']==="yes") {
		  $error_msg="Blocked SQL injection... peace!"; }
    }
  ?>
                      </span></span></div></td>
                    </tr>
                </table></td>
              </tr>
            </table>
            <div align="center"><br>
                <span class="style1">
                <?php  if ($_GET) {  echo $error_msg;  ?>
                </span> <br>
				  <?php  } ?>
            </div>
            <table width="241" height="67" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr>
                <td valign="middle">
				
				<?php if ($_COOKIE["cookie_check"] == 1) { ?>
				  <input name="checkbox" type="checkbox" value="1" checked>
				<?php } else {  ?>
				  <input name="checkbox" type="checkbox" value="1" >
				<?php }  ?>
                    <label for="rememberUn">Remember User </label>
                    <br>
                    <br>
                    <br></td>
              </tr>
              <tr>
                <td><div id="forgot"> <a href="ex"> Forgot your password?</a>&nbsp;|&nbsp;<a href="ex">Request Account  </a></div></td>
              </tr>
            </table>
          <br>
        </form></td>
      </tr>
    </table>
      <table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td width="182">&nbsp;</td>
          <td width="324">&nbsp;</td>
          <td width="254"><div align="center"></div></td>
        </tr>
      </table>    </td>
  </tr>
</table>
<br>
<br>
<br>
<table width="816" height="22" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="951" align="center"></td>
  </tr>
  <tr>
    <td align="right" valign="middle"  ><div align="right" >
      <div align="center" ><span ><a href="http://www.elettric80.it">www.elettric80.it</a> | &copy;  Elettric 80 Inc</span>  - Convergence | 
       <strong>
	    <?php
          include("counter.php");
        ?>
		
      <span id="result_box" lang="en">visits</span></strong><br />
        </div>
    </div></td>
  </tr>
</table>
<script type="text/javascript">
messageObj = new DHTML_modalMessage();	// We only create one object of this class
messageObj.setShadowOffset(5);	// Large shadow

function displayStaticMessage(messageContent,cssClass)
{
	messageObj.setHtmlContent(messageContent);
	messageObj.setSize(300,150);
	messageObj.setCssClassMessageBox(cssClass);
	messageObj.setSource(true);	// no html source since we want to use a static message here.
	messageObj.setShadowDivVisible(false);	// Disable shadow for these boxes	
	messageObj.display();
	
	
}

function closeMessage()
{
	messageObj.close();	
}


</script>
<br />
</body>
</html>
