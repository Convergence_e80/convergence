<?php 
	
	require 'PHPMailer-master/PHPMailerAutoload.php';

	class Email {

		// there are several type of emails:
		// 							To Customer 		To Assignee
		// 1. New Ticket			template_new		template_new
		// 2. New Post 				template_post 		template_post
		// 3. Edit Details 			none 				template_edit
		// 4. Edit to w.f.f. 		template_wff 		template_wff
		// 5. Edit to closed 		template_closed 	template_closed

		static function send_email(
			$template, 
			array $content, 
			$subject = "Convergence - Ticket", 
			array $to = null,
			array $cc = null, 
			array $reply_to = array('Ticketing System' => 'HelpDeskINC@elettric80.it')) 
		{
			
			//Create a new PHPMailer instance
			$mail = new PHPMailer;

			//Tell PHPMailer to use SMTP
			$mail->isSMTP();

			//Enable SMTP debugging
			// 0 = off (for production use)
			// 1 = client messages
			// 2 = client and server messages
			$mail->SMTPDebug = 2;

			//Ask for HTML-friendly debug output
			$mail->Debugoutput = 'html';

			//Set the hostname of the mail server

			// $mail->Host = '108.60.206.186';
			$mail->Host = 'smtp.gmail.com';
			// $mail->Host = "mail.elettric80inc.com";

			//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
			// $mail->Port = 587;

			//Set the encryption system to use - ssl (deprecated) or tls
			$mail->SMTPSecure = 'tls';

			//Whether to use SMTP authentication
			$mail->SMTPAuth = true;

			 // $mail->Username = "HelpDeskINC"; // SMTP username
			 // $mail->Password = "StandardE80"; // SMTP password

			//Username to use for SMTP authentication - use full email address for gmail
			$mail->Username = "biggyapple@gmail.com";

			//Password to use for SMTP authentication
			$mail->Password = "klsByS6tYnophfwuwvPk";

			//Set who the message is to be sent from
			// $mail->setFrom('iamfrancescomeli@gmail.com', 'Francesco Meli');
			$mail->setFrom('biggyapple@gmail.com','E80 Ticketing System');

			foreach ($cc as $name => $email) {
				$mail->addCC($email, $name);
			}

			//Set an alternative reply-to address
			foreach ($reply_to as $name => $email) {
				$mail->addReplyTo($email, $name);
			}

			//Set who the message is to be sent to
			foreach ($to as $name => $email) {
				$mail->addAddress($email, $name);
			}

			//Set the subject line
			$mail->Subject = $subject;

			//Read an HTML message body from an external file, convert referenced images to embedded,
			//convert HTML into a basic plain-text alternative body
			$mail->msgHTML(self::$template($content));

			//Replace the plain text body with one created manually
			// $mail->AltBody = 'This is a plain-text message body';

			//Attach an image file
			// $mail->addAttachment('images/phpmailer_mini.png');

			//send the message, check for errors
			if (!$mail->send()) {
			    echo "Mailer Error: " . $mail->ErrorInfo;
			} else {
			    echo "Message sent!";
			}
		}			

		////////////////////////////////////////////////////////////////////////////////////
		/// NEW POST TEMPLATE 
		/// content :
		/// 	id_ticket
		/// 	author
		/// 	assignee
		/// 	status
		/// 	priority
		/// 	customer
		/// 	title
		/// 	ticket_content
		/// 	ticket_post
		/// 	contact_name
		/// 	contact_phone
		/// 	contact_email

		////////////////////////////////////////////////////////////////////////////////////

		static function template_post($content) 
		{
			ob_start();

			?> 

			<body>

			  <table cellspacing="0" cellpadding="0" style="border: none;width: 100%;">
			    <tr class="header">
			        <td colspan="2" style="font-family: Arial;font-size: 12px;border-bottom: 5px solid #FF9834;padding-bottom: 20px;"><img class="logo" src="http://www.elettric80inc.com/Convergence/images/email_top.jpg"></td><td style="font-family: Arial;font-size: 12px;border-bottom: 5px solid #FF9834;padding-bottom: 20px;"></td>
			    </tr>
			    <tr>
			    	<td colspan="2" style="font-family: Arial;font-size: 12px;padding: 5px;"> <?php echo $content['introduction']; ?> </td>
			    </tr>
			    <tr class="row">
			      <td class="label" valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px;width: 20%;height: 20px;"><div style="background-color: rgb(249, 232, 203);border: 1px solid rgb(237, 218, 198);color: rgb(168, 131, 90);text-align: center;font-size: 10px;font-weight: bold;">Ticket No.</div></td>
			      <td class="content" style="font-family: Arial;font-size: 12px;padding: 5px;width: 80%;"># <?php echo isset($content['ticket_id']) ? $content['ticket_id'] : "-"; ?> </td>
			    </tr>
			    <tr class="row">
			      <td class="label" valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px;width: 20%;height: 20px;"><div style="background-color: rgb(249, 232, 203);border: 1px solid rgb(237, 218, 198);color: rgb(168, 131, 90);text-align: center;font-size: 10px;font-weight: bold;">Author</div></td>
			      <td class="content" style="font-family: Arial;font-size: 12px;padding: 5px;width: 80%;"> <?php echo isset($content['author']) ? $content['author'] : "-"; ?> </td>
			    </tr>
			    <tr class="row">
			      <td class="label" valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px;width: 20%;height: 20px;"><div style="background-color: rgb(249, 232, 203);border: 1px solid rgb(237, 218, 198);color: rgb(168, 131, 90);text-align: center;font-size: 10px;font-weight: bold;">Assignee</div></td>
			      <td class="content" style="font-family: Arial;font-size: 12px;padding: 5px;width: 80%;"> <?php echo isset($content['assignee']) ? $content['assignee'] : "-"; ?> </td>
			    </tr>
			    <tr class="row">
			      <td class="label" valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px;width: 20%;height: 20px;"><div style="background-color: rgb(249, 232, 203);border: 1px solid rgb(237, 218, 198);color: rgb(168, 131, 90);text-align: center;font-size: 10px;font-weight: bold;">Status</div></td>
			      <td class="content" style="font-family: Arial;font-size: 12px;padding: 5px;width: 80%;"> <?php echo isset($content['status']) ? $content['status'] : "-"; ?> </td>
			    </tr>
			    <tr class="row">
			      <td class="label" valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px;width: 20%;height: 20px;"><div style="background-color: rgb(249, 232, 203);border: 1px solid rgb(237, 218, 198);color: rgb(168, 131, 90);text-align: center;font-size: 10px;font-weight: bold;">Priority</div></td>
			      <td class="content" style="font-family: Arial;font-size: 12px;padding: 5px;width: 80%;"> <?php echo isset($content['priority']) ? $content['priority'] : "-"; ?> </td>
			    </tr>
			    <tr class="row">
			      <td class="label" valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px;width: 20%;height: 20px;"><div style="background-color: rgb(249, 232, 203);border: 1px solid rgb(237, 218, 198);color: rgb(168, 131, 90);text-align: center;font-size: 10px;font-weight: bold;">Customer</div></td>
			      <td class="content" style="font-family: Arial;font-size: 12px;padding: 5px;width: 80%;"> <?php echo isset($content['customer']) ? $content['customer'] : "-"; ?> </td>
			    </tr>
			    <tr class="row">
			      <td class="label" valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px;width: 20%;height: 20px;"><div style="background-color: rgb(249, 232, 203);border: 1px solid rgb(237, 218, 198);color: rgb(168, 131, 90);text-align: center;font-size: 10px;font-weight: bold;">Title</div></td>
			      <td class="content" style="font-family: Arial;font-size: 12px;padding: 5px;width: 80%;"> <?php echo isset($content['title']) ? $content['title'] : "-"; ?> </td>
			    </tr>
			    <tr class="row ticket_content" bgcolor="#F9E8CB">
			      <td valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px 3px 5px 3px;">
			        <center>
			          <img src="http://www.elettric80inc.com/convergence/images/ticket.png" width="70px">
			        </center>
			      </td>
			      <td style="font-family: Arial;font-size: 12px;padding: 5px 3px 5px 3px;">
			        <?php echo isset($content['ticket_content']) ? $content['ticket_content'] : "-"; ?>
			      </td>
			    </tr>
			   <tr class="row ticket_post" bgcolor="#CBE1F9">
			      <td valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px 3px 5px 3px;">
			        <center>
			            <img src="http://www.elettric80inc.com/convergence/images/arrow.png" width="70px">
			        </center>
			      </td>
			      <td style="font-family: Arial;font-size: 12px;padding: 5px 3px 5px 3px;">
			        <?php echo isset($content['ticket_post']) ? $content['ticket_post'] : "-"; ?>
			      </td>
			    </tr>
			    <tr class="row">
			      <td class="label" valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px;width: 20%;height: 20px;"><div style="background-color: rgb(249, 232, 203);border: 1px solid rgb(237, 218, 198);color: rgb(168, 131, 90);text-align: center;font-size: 10px;font-weight: bold;">Contact Name</div></td>
			      <td class="content" style="font-family: Arial;font-size: 12px;padding: 5px;width: 80%;"> <?php echo isset($content['contact_name']) ? $content['contact_name'] : "-"; ?> </td>
			    </tr>
			    <tr class="row">
			      <td class="label" valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px;width: 20%;height: 20px;"><div style="background-color: rgb(249, 232, 203);border: 1px solid rgb(237, 218, 198);color: rgb(168, 131, 90);text-align: center;font-size: 10px;font-weight: bold;">Contact Phone</div></td>
			      <td class="content" style="font-family: Arial;font-size: 12px;padding: 5px;width: 80%;"><?php echo isset($content['contact_phone']) ? $content['contact_phone'] : "-"; ?></td>
			    </tr>
			    <tr class="row">
			      <td class="label" valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px;width: 20%;height: 20px;"><div style="background-color: rgb(249, 232, 203);border: 1px solid rgb(237, 218, 198);color: rgb(168, 131, 90);text-align: center;font-size: 10px;font-weight: bold;">Contact Email</div></td>
			      <td class="content" style="font-family: Arial;font-size: 12px;padding: 5px;width: 80%;">
			        <a href="mailto:$cc?>"><?php echo isset($content['contact_email']) ? $content['contact_email'] : "-"; ?></a> 
			      </td>
			    </tr>
			    <tr class="footer">
			      <td colspan="2" style="font-family: Arial;font-size: 12px;border-top: 5px solid #FF9834;padding-top: 20px;">
			        Go and check/reply the ticket <a href="http://www.elettric80inc.com/customerservice/help_desk_ticket.php?Id=<?php echo isset($content['ticket_id']) ? $content['ticket_id'] : ''; ?>">here</a>.
			        Make sure you are in the Intranet, otherwise get connected to the network throughout VPN. 
			      </td>
			    
			  </tr></table>
			</body>

			<?php 

			$text = ob_get_clean();

			return $text;
		}
		

		////////////////////////////////////////////////////////////////////////////////////
		// NEW/EDIT TICKET TEMPLATE 
		// 	introduction
		// 	ticket_id
		// 	author
		// 	assignee
		// 	status
		// 	priority
		// 	customer
		// 	date
		// 	title
		// 	ticket_content
		// 	contact_name
		// 	contact_phone
		// 	contact_email

		////////////////////////////////////////////////////////////////////////////////////

		static function template_new_edit($content) 
		{
			ob_start();

			?> 

			<body>

			  <table cellspacing="0" cellpadding="0" style="border: none;width: 100%;">
			    <tr class="header">
			        <td colspan="2" style="font-family: Arial;font-size: 12px;border-bottom: 5px solid #FF9834;padding-bottom: 20px;"><img class="logo" src="http://www.elettric80inc.com/Convergence/images/email_top.jpg"></td><td style="font-family: Arial;font-size: 12px;border-bottom: 5px solid #FF9834;padding-bottom: 20px;"></td>
			    </tr>
			    <tr>
			    	<td colspan="2" style="font-family: Arial;font-size: 12px;padding: 5px;"> <?php echo $content['introduction']; ?> </td>
			    </tr>
			    <tr class="row">
			      <td class="label" valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px;width: 20%;height: 20px;"><div style="background-color: rgb(249, 232, 203);border: 1px solid rgb(237, 218, 198);color: rgb(168, 131, 90);text-align: center;font-size: 10px;font-weight: bold;">Ticket No.</div></td>
			      <td class="content" style="font-family: Arial;font-size: 12px;padding: 5px;width: 80%;"># <?php echo isset($content['ticket_id']) ? $content['ticket_id'] : "-"; ?> </td>
			    </tr>
			   <tr class="row">
			      <td class="label" valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px;width: 20%;height: 20px;"><div style="background-color: rgb(249, 232, 203);border: 1px solid rgb(237, 218, 198);color: rgb(168, 131, 90);text-align: center;font-size: 10px;font-weight: bold;">Author</div></td>
			      <td class="content" style="font-family: Arial;font-size: 12px;padding: 5px;width: 80%;"> <?php echo isset($content['author']) ? $content['author'] : "-"; ?> </td>
			    </tr>
			    <tr class="row">
			      <td class="label" valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px;width: 20%;height: 20px;"><div style="background-color: rgb(249, 232, 203);border: 1px solid rgb(237, 218, 198);color: rgb(168, 131, 90);text-align: center;font-size: 10px;font-weight: bold;">Assignee</div></td>
			      <td class="content" style="font-family: Arial;font-size: 12px;padding: 5px;width: 80%;"> <?php echo isset($content['assignee']) ? $content['assignee'] : "-"; ?> </td>
			    </tr>
			    <tr class="row">
			      <td class="label" valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px;width: 20%;height: 20px;"><div style="background-color: rgb(249, 232, 203);border: 1px solid rgb(237, 218, 198);color: rgb(168, 131, 90);text-align: center;font-size: 10px;font-weight: bold;">Status</div></td>
			      <td class="content" style="font-family: Arial;font-size: 12px;padding: 5px;width: 80%;"> <?php echo isset($content['status']) ? $content['status'] : "-"; ?> </td>
			    </tr>
			    <tr class="row">
			      <td class="label" valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px;width: 20%;height: 20px;"><div style="background-color: rgb(249, 232, 203);border: 1px solid rgb(237, 218, 198);color: rgb(168, 131, 90);text-align: center;font-size: 10px;font-weight: bold;">Priority</div></td>
			      <td class="content" style="font-family: Arial;font-size: 12px;padding: 5px;width: 80%;"> <?php echo isset($content['priority']) ? $content['priority'] : "-"; ?> </td>
			    </tr>
			    <tr class="row">
			      <td class="label" valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px;width: 20%;height: 20px;"><div style="background-color: rgb(249, 232, 203);border: 1px solid rgb(237, 218, 198);color: rgb(168, 131, 90);text-align: center;font-size: 10px;font-weight: bold;">Customer</div></td>
			      <td class="content" style="font-family: Arial;font-size: 12px;padding: 5px;width: 80%;"> <?php echo isset($content['customer']) ? $content['customer'] : "-"; ?> </td>
			    </tr>
			    <tr class="row">
			      <td class="label" valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px;width: 20%;height: 20px;"><div style="background-color: rgb(249, 232, 203);border: 1px solid rgb(237, 218, 198);color: rgb(168, 131, 90);text-align: center;font-size: 10px;font-weight: bold;">Title</div></td>
			      <td class="content" style="font-family: Arial;font-size: 12px;padding: 5px;width: 80%;"> <?php echo isset($content['title']) ? $content['title'] : "-"; ?> </td>
			    </tr>
			    <tr class="row ticket_content" bgcolor="#F9E8CB">
			      <td valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px 3px 5px 3px;">
			        <center>
			          <img src="http://www.elettric80inc.com/convergence/images/ticket.png" width="70px">
			        </center>
			      </td>
			      <td style="font-family: Arial;font-size: 12px;padding: 5px 3px 5px 3px;">
			        <?php echo isset($content['ticket_content']) ? $content['ticket_content'] : "-"; ?>
			      </td>
			    </tr>
			    <tr class="row">
			      <td class="label" valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px;width: 20%;height: 20px;"><div style="background-color: rgb(249, 232, 203);border: 1px solid rgb(237, 218, 198);color: rgb(168, 131, 90);text-align: center;font-size: 10px;font-weight: bold;">Contact Name</div></td>
			      <td class="content" style="font-family: Arial;font-size: 12px;padding: 5px;width: 80%;"> <?php echo isset($content['contact_name']) ? $content['contact_name'] : "-"; ?> </td>
			    </tr>
			    <tr class="row">
			      <td class="label" valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px;width: 20%;height: 20px;"><div style="background-color: rgb(249, 232, 203);border: 1px solid rgb(237, 218, 198);color: rgb(168, 131, 90);text-align: center;font-size: 10px;font-weight: bold;">Contact Phone</div></td>
			      <td class="content" style="font-family: Arial;font-size: 12px;padding: 5px;width: 80%;"><?php echo isset($content['contact_phone']) ? $content['contact_phone'] : "-"; ?></td>
			    </tr>
			    <tr class="row">
			      <td class="label" valign="middle" style="font-family: Arial;font-size: 12px;padding: 5px;width: 20%;height: 20px;"><div style="background-color: rgb(249, 232, 203);border: 1px solid rgb(237, 218, 198);color: rgb(168, 131, 90);text-align: center;font-size: 10px;font-weight: bold;">Contact Email</div></td>
			      <td class="content" style="font-family: Arial;font-size: 12px;padding: 5px;width: 80%;">
			        <a href="mailto:<?php echo $content['contact_email']; ?>"><?php echo isset($content['contact_email']) ? $content['contact_email'] : "-"; ?></a> 
			      </td>
			    </tr>
			    <tr class="footer">
			      <td colspan="2" style="font-family: Arial;font-size: 12px;border-top: 5px solid #FF9834;padding-top: 20px;">
			        Go and check/reply the ticket <a href="http://www.elettric80inc.com/customerservice/help_desk_ticket.php?Id=<?php echo isset($content['ticket_id']) ? $content['ticket_id'] : ''; ?>">here</a>.
			        Make sure you are in the Intranet, otherwise get connected to the network throughout VPN. 
			      </td>
			    
			  </tr></table>
			</body>

			<?php 

			$text = ob_get_clean();

			return $text;
		}

		////////////////////////////////////////////////////////////////////////////////////
		/// WAITING FOR FEEDBACK TEMPLATE 
		////////////////////////////////////////////////////////////////////////////////////

		static function template_wff($content) 
		{
			$template = "template_wff";
			return $template;
		}

		////////////////////////////////////////////////////////////////////////////////////
		/// CLOSED TICKET TEMPLATE 
		////////////////////////////////////////////////////////////////////////////////////

		static function template_closed($content) 
		{
			$template = "template_closed";
			return $template;
		}

	}



?>