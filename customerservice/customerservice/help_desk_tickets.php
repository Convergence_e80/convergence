<!--%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

VERSION: 	

- 1.1

COMMENTS:	(add comment describing what has been changed)

-  
- 

DATE:

- 03/14/13

AUTHOR:

- Jose Hermida

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%-->
<?php
session_cache_limiter('private, must-revalidate');
ini_set('display_errors','Off');
include("check_connection.php");	// import file for checking the session of the login

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<link href="favicon.ico" type="image/x-icon" rel="shortcut icon"> 

<link type="text/css" rel="stylesheet" href="style.css" />
<link type="text/css" rel="stylesheet" href="bootstrap.css" />



<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<title>Elettric 80 Inc - Customer Service</title>


	<link rel="stylesheet" href="loading/css/modal-message.css" type="text/css">
	<script type="text/javascript" src="loading/js/ajax.js"></script>
	<script type="text/javascript" src="loading/js/modal-message.js"></script>
	<script type="text/javascript" src="loading/js/ajax-dynamic-content.js"></script>
	<script type="text/javascript" src="js/bootstrap-carousel.js"></script>
	
	
	<link rel="stylesheet" href="buttons.css">
	<style> 
.background1 { 
background-image: url(images/fondo_login.jpg); 
background-repeat: no-repeat; 
} 



.orange {
border-style:solid;
border-width:3px;
border-color:#FE9900;

}

.tb11 {
	background:#FFFFFF no-repeat 4px 4px;
	padding:4px 4px 4px 22px;
	border:1px solid #CCCCCC;
	width:350px;
	height:29px;
	font-size: 13px
}
.tb12 {
	background:#FFFFFF no-repeat 4px 4px;
	padding:4px 4px 4px 22px;
	border:1px solid #CCCCCC;
	width:350px;
	height:150px;
	font-size: 13px
}
a:link {
	color: #000000;
}
a:visited {
	color: #000000;
}
a:hover {
	color: #000000;
}
body {
	background-color: #FFFFFF;
}
    .style4 {font-size: 13px}
    .style5 {font-family: Arial, Helvetica, sans-serif}
    .style7 {font-size: 13px; font-family: Arial, Helvetica, sans-serif; }
    .style8 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 22px;
	color: #0061A8;
}
    .style1 {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #094FA4;
}
.style31 {font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px;}
.style77 {font-size: 12px; color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif;}
.style76 {color: #0E50A4}
.tb111 {	padding:4px 4px 4px 3px;
	border:1px solid #CCCCCC;
	width:205px;
	height:30px;
}
    .style78 {font-size: 25px}
    </style>	

	
 

<script type="text/javascript">
<!--
<!--
function submitform()
{
  document.form1.submit();
}

function envio () {
document.getElementById("form1").submit();
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>

<script type="text/javascript" src="jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="jquery.tablesorter.min.js"></script>


<script language="JavaScript1.2" >
<!--

//for tablesorting
$(document).ready(function()     {         $("#myTable").tablesorter();     } ); 

//for mouse over effects
function cambiar_color_over(celda){ 
   celda.style.backgroundColor="#F9BF6B" 
} 
function cambiar_color_out(celda){ 
   celda.style.backgroundColor="#FFFFFF" 
}
//-->
</script>

</head>

<body onLoad="MM_preloadImages('images/home_ov.jpg','images/spare_parts_ov.jpg','images/maintenance_ov.jpg','images/training_ov.jpg','images/contact_ov.jpg','images/products_ov.jpg','images/help_desk_ov.jpg')"    >
<table width="950" height="45" border="0" align="center" cellpadding="0" cellspacing="0"  >
  <tr>
    <td width="638"><div align="right"><img src="images/phone.jpg" width="24" height="24"></div></td>
    <td width="10">&nbsp;</td>
    <td width="140"><span class="style7">USA: +(847) 329-7717</span></td>
    <td width="28"><div align="right"><img src="images/Mail.jpg" width="24" height="16"></div></td>
    <td width="9">&nbsp;</td>
    <td width="127"><a href="mailto:e80.usa@elettric80.it" class="style4 style5">e80.usa@elettric80.it</a></td>
  </tr>
</table>
<table width="950" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr>
    <td width="617"><table width="950" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="538"><img src="images/logo_after_sales.jpg" width="538" height="73"></td>
        <td width="21">&nbsp;</td>
        <td width="10">&nbsp;</td>
        <td width="381" valign="baseline"><?php 
	session_start();
	if  ($_SESSION["user_access"]== "access") {
  
 
      //include("../connection.php");

      $snom = $_SESSION["nom"];
	  //echo "es: ".$snom;

	  
	  $comp = $_SESSION["company_id"];

	  $tsql = "SELECT * FROM Customer_User_Login WHERE Customer_Id = '$snom'  ";	 
	  //print_r($tsql); 
      $result = sqlsrv_query( $conn, $tsql ); 
	  $row = sqlsrv_fetch_array($result);

	 $tsql_em_dep = "SELECT * FROM Customers WHERE Id = '$comp' ";
	 $result_emp_dep = sqlsrv_query( $conn, $tsql_em_dep);
	 $row_emp_dep = sqlsrv_fetch_array($result_emp_dep);	
	
	
	?>
            <br>
            <br>
            <br>
            <div align="center" class="style7">
              <div align="right">Hello,<strong> <?php echo $row[1]." ".$row[2];   $user = $row[1]." ".$row[2]; ?> </strong> |&nbsp;<a href="logout.php">logout </a></div>
            </div>
          <?php } else { ?>
            <br>
            <br>
            <br>
            <div align="center" class="style7">
              <div align="right"> <a href="ex"> </a></div>
            </div>
          <?php }   ?>
        </td>
      </tr>
    </table>
    <script type="text/javascript">
messageObj = new DHTML_modalMessage();	// We only create one object of this class
messageObj.setShadowOffset(5);	// Large shadow

function displayMessage(url)
{ 
	
	messageObj.setSource(url);
	messageObj.setCssClassMessageBox(false);
	messageObj.setSize(400,200);
	messageObj.setShadowDivVisible(true);	// Enable shadow for these boxes
	messageObj.display();
}

function displayStaticMessage(messageContent,cssClass)
{
	messageObj.setHtmlContent(messageContent);
	messageObj.setSize(300,150);
	messageObj.setCssClassMessageBox(cssClass);
	messageObj.setSource(false);	// no html source since we want to use a static message here.
	messageObj.setShadowDivVisible(false);	// Disable shadow for these boxes	
	messageObj.display();
	
	
}



function closeMessage()
{
	messageObj.close();	
}


</script>
	  
    </td>
  </tr>
  <tr>
    <td><div align="center"> <br>
      <table width="944" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="135"><a href="index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('home','','images/home_ov.jpg',1)"><img src="images/home.jpg" name="home" width="135" height="55" border="0"></a> </td>
          <td width="134"><a href="help_desk.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('help_desk','','images/help_desk_ov.jpg',1)"><img src="images/help_desk.jpg" name="help_desk" width="135" height="55" border="0"></a></td>
          <td width="135"><a href="spare_parts.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('spare_parts','','images/spare_parts_ov.jpg',1)"><img src="images/spare_parts.jpg" name="spare_parts" width="135" height="55" border="0"></a></td>
          <td width="135"><a href="maintenance.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('maintenance','','images/maintenance_ov.jpg',1)"><img src="images/maintenance.jpg" name="maintenance" width="135" height="55" border="0"></a></td>
          <td width="135"><a href="products.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Products','','images/products_ov.jpg',1)"><img src="images/products.jpg" name="Products" width="135" height="55" border="0"></a></td>
          <td width="135"><a href="training.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('training','','images/training_ov.jpg',1)"><img src="images/training.jpg" name="training" width="135" height="55" border="0"></a></td>
          <td width="135"><a href="contact.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Contact','','images/contact_ov.jpg',1)"><img src="images/contact.jpg" name="Contact" width="135" height="55" border="0"></a></td>
        </tr>
      </table>
      </div></td>
  </tr>
  <tr>
    <td>   </td>
  </tr>
  <tr>
    <td>
	
<table width="946" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td><br>
        <br>
            <br>
            <table width="944" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td width="413"><span class="style8"><span class="style78">Help Desk </span><br>
          </span></td>
          <td width="520"><span class="style8">Company: <strong>
          <?php  echo $row_emp_dep[1];	 ?>
          </strong></span></td>
          <td width="11">&nbsp;</td>
        </tr>
      </table>
            <div align="justify"><span class="style4"><br>
              </span> 
                <?php 
					$assignee  = $_POST[assignee];
					$cust  = $_POST[customer];
					$division  = $_POST[division];
					$status  = $_POST[status];	
	            ?>			  
              <form id="form1" name="form1" method="POST" action="help_desk_tickets.php">

                <table width="949" height="50" border="0" align="center"  cellpadding="0" cellspacing="0">
                  <tr>
                    <td width="115"><table width="942" height="19" border="0" align="center"  cellpadding="0" cellspacing="0">
                        <tr>
                          <td width="207"><select name="customer" class="tb111" id="customer">
                              <?php  if ($customer != 'all') { 
	         
						 $tsql_cust = "select * from Customers WHERE Id = '$comp'";
						 $result_cust = sqlsrv_query( $conn, $tsql_cust);
						 $row_cust = sqlsrv_fetch_array($result_cust);
			 			
			      ?>
                              <option value="<?php echo $row_cust[0]; ?>"> <?php echo $row_cust[1]; ?> </option>
                              <?php } ?>
                          </select></td>
                          <td width="208"><select name="assignee" class="tb111">
                              <?php  if ($assignee != 'all') { 
	         
             $tsql_em = "select * from Employees WHERE Id = '$assignee'";
             $result_emp = sqlsrv_query( $conn, $tsql_em);
			 $row_emp = sqlsrv_fetch_array($result_emp);			 
			 			
			 ?>
                              <option value="<?php echo $assignee; ?>"> <?php echo $row_emp[13]." ".$row_emp[1]; ?> </option>
                              <?php } ?>
                              <option value="all"> All Technicians </option>
                              <option value="all"> ---------------------------- </option>
                              <?php
					  
					// SQL Employee
					$sql_employee = "SELECT DISTINCT Id_Assignee FROM Tickets WHERE Id_Customer = '$comp' AND Deleted_Ticket is NULL ORDER BY Id_Assignee ASC ";
					$result_sql_employee = sqlsrv_query( $conn, $sql_employee);

					while($row_sql_employee = sqlsrv_fetch_array($result_sql_employee)) { 
					
					 
					$sql_assignee = "select Id, Name,Last_name Title from Employees where Id = '$row_sql_employee[0]'";
					$result_sql_assignee = sqlsrv_query( $conn, $sql_assignee); 
					$row_sql_assignee = sqlsrv_fetch_array($result_sql_assignee);
					
					?>
                              <option value="<?php echo $row_sql_assignee[0]; ?>"> <?php echo $row_sql_assignee[2]." ".$row_sql_assignee[1]; ?> </option>
                              <?php
					}
					?>
                          </select></td>
                          <td width="207"><select name="division" class="tb111" id="division">
                              <?php  if ($division != 'all') { 
	         
             $tsql_division = "select * FROM System_Type where Id = '$division'";
             $result_tsql_division = sqlsrv_query( $conn, $tsql_division);
			 $row_tsql_division = sqlsrv_fetch_array($result_tsql_division);			 
			 			
			 ?>
                              <option value="<?php echo $division; ?>"> <?php echo $row_tsql_division[1]; ?> </option>
                              <?php } ?>
                              <option value="all"> All Divisions </option>
                              <option value="all"> ---------------------------- </option>
                              <?php
					  
					// SQL Employee
					$sql_ticket_system = "SELECT DISTINCT Id_System FROM Tickets WHERE Id_Customer = '$comp' AND Deleted_Ticket is NULL ORDER BY Id_System ASC ";
					$result_sql_ticket_system = sqlsrv_query( $conn, $sql_ticket_system);

					 while($row_result_sql_ticket_system = sqlsrv_fetch_array($result_sql_ticket_system)) { 
					
					 
							$sql_system = "select * from System_Type where Id = '$row_result_sql_ticket_system[0]'";
							$result_sql_system = sqlsrv_query( $conn, $sql_system); 
							$row_sql_system = sqlsrv_fetch_array($result_sql_system);
					
						   ?>
                              <option value="<?php echo $row_sql_system[0]; ?>"> <?php echo $row_sql_system[1]; ?> </option>
                              <?php
					 }
				  ?>
                          </select></td>
                          <td width="214">

						  <select name="status" class="tb111" id="status">
						  
						      <?php  if ($status == 'open_tickets') { ?>
						     <option value="open_tickets"> All Open Tickets </option>
							   <?php  } ?> 
							 
							 
                              <?php  if ($status != 'all') { 
	         
             $tsql_status = "SELECT * FROM Ticket_Status WHERE Id = '$status'";
             $result_tsql_status = sqlsrv_query( $conn, $tsql_status);
			 $row_tsql_status = sqlsrv_fetch_array($result_tsql_status);			 
			 			
			 ?>
                              <option value="<?php echo $division; ?>"> <?php echo $row_tsql_status[1]; ?> </option>
                              <?php } ?>
                              <option value="all"> All Status </option>
                              <option value="all"> ---------------------------- </option>
                              <?php
					  
					// SQL Employee
					$sql_ticket_status = "SELECT DISTINCT Status FROM Tickets WHERE Id_Customer = '$comp' AND Deleted_Ticket is NULL ORDER BY Status ASC ";
					$result_sql_ticket_system = sqlsrv_query( $conn, $sql_ticket_status);

					 while($row_result_sql_ticket_status = sqlsrv_fetch_array($result_sql_ticket_system)) { 
					
					 
							$sql_status = "select * from Ticket_Status	 where Id = '$row_result_sql_ticket_status[0]'";
							$result_sql_status= sqlsrv_query( $conn, $sql_status); 
							$row_sql_status = sqlsrv_fetch_array($result_sql_status);
					
						   ?>
                              <option value="<?php echo $row_sql_status[0]; ?>"> <?php echo $row_sql_status[1]; ?> </option>
                              <?php
					 }
				  ?>      <option value="all"> ---------------------------- </option>
				          <option value="open_tickets"> All Open Tickets </option>
                          </select>
						  
						  
						  </td>
                          <td width="74"><div align="right">
                            <input class="button yellow small" name="submit" type="submit" value="Search">
                          </div></td>
                        </tr>
                    </table></td>
                  </tr>
                </table>
              </form>
              <br>
              <table width="942" height="19" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                  <td width="558"><div align="right">
                      <table width="535" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                          <td width="550"></td>
                          <td width="10">&nbsp;</td>
                        </tr>
                      </table>
                  </div></td>
                  <td width="16">&nbsp;</td>
                  <td width="5"><a href="../add_ticket.php"></a></td>
                  <td width="19">&nbsp;</td>
                  <td width="36">&nbsp;</td>
                  <td width="19"><img src="images/excel_ico.jpg" width="19" height="19"></td>
                  <td width="10">&nbsp;</td>
                  <td width="128"><a href="export_excel.php?customer=<?php echo $cust;?>&assignee=<?php echo $assignee;?>&division=<?php echo $division;?>&status=<?php echo $status;?>&user=<?php echo $row[1]." ".$row[2];?>">Export result to Excel</a></td>
                  <td width="151"><div align="left"><span class="style77">| &nbsp; (</span><span class="style1">
                    <?php 
					
					//print_r($_POST);
   // Check all
   
   if ($status == 'open_tickets')
      
	  {
	  
	    if ( $customer !== 'all' and $assignee == 'all' and  $division == 'all' and  $status == 'open_tickets' ) { 
	  
	    $sql_tickets = "select * from Tickets where Id_Customer = '$cust' and (Status = 1 or Status = 2 or Status = 3 or Status = 4 or Status = 5) AND Deleted_Ticket is null ORDER BY Id DESC";
		
	   }


	    if ( $customer !== 'all' and $assignee != 'all' and  $division == 'all' and  $status == 'open_tickets' ) { 
	  
	    $sql_tickets = "select * from Tickets where Id_Customer = '$cust'  and Id_Assignee = '$assignee' and (Status = 1 or Status = 2 or Status = 3 or Status = 4 or Status = 5) AND Deleted_Ticket is null ORDER BY Id DESC";
		
	   }


	    if ( $customer !== 'all' and $assignee != 'all' and  $division != 'all' and  $status == 'open_tickets' ) { 
	  
	    $sql_tickets = "select * from Tickets where Id_Customer = '$cust'  and Id_System = '$division' and Id_Assignee = '$assignee' and (Status = 1 or Status = 2 or Status = 3 or Status = 4 or Status = 5) AND Deleted_Ticket is null ORDER BY Id DESC";
	   }
	   
	    if ( $customer !== 'all' and $assignee == 'all' and  $division != 'all' and  $status == 'open_tickets' ) { 
	  
	    $sql_tickets = "select * from Tickets where Id_Customer = '$cust'  and Id_System = '$division'  and (Status = 1 or Status = 2 or Status = 3 or Status = 4 or Status = 5) AND Deleted_Ticket is null ORDER BY Id DESC";
	   }
	   	   	   	   

	  } else {
   
 	
	  
   // Check by employees	  
   if ($assignee != 'all' and $customer !== 'all' and  $division == 'all' and  $status == 'all' ) {
	   $sql_tickets = " SELECT * FROM Tickets WHERE  and Id_Customer = '$cust' and Id_Assignee = '$assignee' and  Deleted_Ticket is null ORDER BY Id DESC";
	 }
   // Check by Employee and Customer	 
   if ($assignee != 'all' and $customer != 'all' and  $division == 'all' and $status == 'all') {
		$sql_tickets = " SELECT * FROM Tickets WHERE Id_Assignee = '$assignee' and Id_Customer = '$cust' and  Deleted_Ticket is null ORDER BY Id DESC";
	}	
   // Check by Employee, Customer and Division
   if ($assignee != 'all' and $customer != 'all' and  $division != 'all' and $status == 'all' ) {
		$sql_tickets = "SELECT * FROM Tickets WHERE Id_Assignee = '$assignee' and Id_Customer = '$cust' and Id_System = '$division' and  Deleted_Ticket is null ORDER BY Id DESC";
	}
	// Check by Employee, Customer, Division and Status	
   if ($assignee != 'all' and $customer != 'all' and  $division != 'all' and $status != 'all' ) {
	$sql_tickets = "SELECT * FROM Tickets WHERE Id_Assignee = '$assignee' and Id_Customer = '$cust' and Id_System = '$division' and Status = '$status' and  Deleted_Ticket is null ORDER BY Id DESC";
	}							
	// Check by Customer, Division
   if ($assignee == 'all' and $customer != 'all' and  $division != 'all' and $status == 'all' ) {
	$sql_tickets = "SELECT * FROM Tickets WHERE Id_Customer = '$cust' and Id_System = '$division' and  Deleted_Ticket is null ORDER BY Id DESC";
	}		
	// Check by Customer, Status
   if ($assignee == 'all' and $customer != 'all' and  $division == 'all' and $status != 'all' ) {
	$sql_tickets = "SELECT * FROM Tickets WHERE Id_Customer = '$cust' and Status = '$status' and  Deleted_Ticket is null ORDER BY Id DESC";
	}		
	// Check by  Division and Status	
   if ($assignee == 'all' and $customer != 'all' and  $division != 'all' and $status != 'all' ) {
   
	$sql_tickets = "SELECT * FROM Tickets WHERE  Id_Customer = '$cust' and Id_System = '$division' and Status = '$status' and  Deleted_Ticket is null ORDER BY Id DESC";
	
	}		
	// Check by Employee, Status	
   if ($assignee != 'all' and $customer !== 'all' and  $division == 'all' and $status != 'all' ) {
	$sql_tickets = "SELECT * FROM Tickets WHERE Id_Customer = '$cust' and Id_Assignee = '$assignee' and Status = '$status' and  Deleted_Ticket is null ORDER BY Id DESC";
	}		
	// Check by Employee, Division	
   if ($assignee != 'all' and $customer !== 'all' and  $division != 'all' and $status == 'all' ) {
	$sql_tickets = "SELECT * FROM Tickets WHERE Id_Customer = '$cust' and Id_Assignee = '$assignee' and Id_System = '$division' and  Deleted_Ticket is null ORDER BY Id DESC";
	}	
	
	// Check by Customer
   if ($assignee == 'all' and $customer != 'all' and  $division == 'all' and $status == 'all' ) {
	$sql_tickets = "SELECT * FROM Tickets WHERE Id_Customer = '$cust' and Deleted_Ticket is null ORDER BY Id DESC";
	}	
	// Check by Status	
   if ($assignee == 'all' and $customer !== 'all' and  $division == 'all' and $status != 'all' ) {
	$sql_tickets = "SELECT * FROM Tickets WHERE Id_Customer = '$cust' and Status = '$status' and  Deleted_Ticket is null ORDER BY Id DESC";
	}	
	// Check by Division	
	
   if ($assignee == 'all' and $customer !== 'all' and  $division != 'all' and $status == 'all' ) {
	$sql_tickets = "SELECT * FROM Tickets WHERE Id_Customer = '$cust' and Id_System = '$division' and  Deleted_Ticket is null ORDER BY Id DESC";
	}		
			
			
	 }		
								
	$result_sql_tickets = sqlsrv_query( $conn, $sql_tickets, array(), array( "Scrollable" => 'static' ) );
					
	$row_quotes = sqlsrv_num_rows($result_sql_tickets);	
	echo $row_quotes;  
?>
                  </span><span class="style31">) Tickets found </span></div></td>
                </tr>
              </table>
              <br>
              <table width="949" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                  <td><table width="94%" height="15" align="center" cellpadding="0" cellspacing="0" bordercolor="#FFFFFF" class="tablesorter"  id="myTable">
                      <thead>
                        <tr>
                          <th width="57" class="voice" align="left"><a href="../ticket.php?Id=<?php echo $row_sql_tickets[0]; ?>"></a>Ticket </th>
                          <th width="243" class="voice" align="left">Summary</th>
                          <th width="94" class="voice" align="left">Status</th>
                          <th width="111" class="voice" align="left">Priority</th>
                          <th width="104" class="voice" align="left">Assigned to </th>
                          <th width="185" class="voice" align="left">Customer</th>
                          <th width="63" class="voice" align="left">Division</th>
                          <th width="90" class="voice" align="left">Updated</th>
                        </tr>
                      </thead>
                      <tbody>
					  
                        <?php 			
 		                   while($row_sql_tickets = sqlsrv_fetch_array($result_sql_tickets)) { 
		                 ?>
						 
                        <tr bgcolor="#FFFFFF" onMouseOver="cambiar_color_over(this)" onMouseOut="cambiar_color_out(this)">
                          <td valign="top"><div align="center"> <a href="ticket.php?Id=<?php echo $row_sql_tickets[0]; ?>"></a> <br>
                                  <?php echo "# ".$row_sql_tickets[0]; ?></div>
                            <a href="ticket.php?Id=<?php echo $row_sql_tickets[0]; ?>"></a></td>
                          <td><form name="form2" method="post" action="">
                          </form>
                          <?php
					$sql_assignee = "select * from Employees where Id='$row_sql_tickets[3]'";
					$result_sql_assignee = sqlsrv_query( $conn, $sql_assignee); 
					$row_sql_assignee = sqlsrv_fetch_array($result_sql_assignee); 
					?>
                              <div class="voice " > <a href="help_desk_ticket.php?Id=<?php 
							      // $row_sql_tickets[0]=md5($row_sql_tickets[0]);
							  echo $row_sql_tickets[0]; 
							  	  
							  ?>"><?php echo $row_sql_tickets[1]; ?> </a></div>
                            <div class="description"> Reported by <span class="style76"> <?php echo $row_sql_assignee[1]." ".$row_sql_assignee[13]; ?> </span> the <?php echo date_format($row_sql_tickets[9], 'm/d/Y').""; ?> 
                              <input name="id_hidden" type="hidden" id="id_hidden" value="<?php echo $row_sql_tickets[0]; ?>">
                          </div></td>
                          <td><?php
					$sql_status = "select * from Ticket_Status where Id='$row_sql_tickets[4]'";
					$result_sql_status = sqlsrv_query( $conn, $sql_status); 
					$row_sql_status = sqlsrv_fetch_array($result_sql_status); 
					
					echo $row_sql_status[1]; 
					?>
                          </td>
                          <td><div align="left">
                              <?php 
					$sql_level = "select * from Priority where Id='$row_sql_tickets[5]'";
					$result_sql_level = sqlsrv_query( $conn, $sql_level); 
					$row_sql_level = sqlsrv_fetch_array($result_sql_level); 
					echo  $row_sql_level[1]; 

					?>

                          </div></td>
                          <td><?php 
					$sql_assignee = "select * from Employees where Id='$row_sql_tickets[7]'";
					$result_sql_assignee = sqlsrv_query( $conn, $sql_assignee); 
					$row_sql_assignee = sqlsrv_fetch_array($result_sql_assignee); ?>
                              <?php echo $row_sql_assignee[1]." ".$row_sql_assignee[13]; ?> </td>
                          <td><?php 
					$sql_cust = "select * from Customers where Id='$row_sql_tickets[8]'";
					$result_sql_cust = sqlsrv_query( $conn, $sql_cust); 
					$row_sql_cust = sqlsrv_fetch_array($result_sql_cust); 
					?>
                              <?php
					echo $row_sql_cust[1]; 
					?>
                          </td>
                          <td><div align="left">
                              <?php 
					$sql_div = "SELECT * FROM System_Type WHERE Id='$row_sql_tickets[14]'";
					$result_sql_div = sqlsrv_query( $conn, $sql_div); 
					$row_sql_div = sqlsrv_fetch_array($result_sql_div); 
					?>
                              <?php
					echo $row_sql_div[1]; 
			 ?>
                          </div></td>
                          <td><?php  echo date_format($row_sql_tickets[10], 'm/d/Y')."";  ?></td>
                        </tr>
                        <?php
		    } 
		  ?>
                      </tbody>
                  </table></td>
                </tr>
              </table>
            </div>
          </td>
  </tr>
</table>	  </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><img src="images/bar.jpg" width="948" height="45"></td>
  </tr>
</table>
<table width="816" height="22" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="951" align="center"></td>
  </tr>
  <tr>
    <td align="right" valign="middle"  >
	<br>
<div align="right" >
      <div align="center" class="style5" ><span class="style7"><a href="http://www.elettric80.it">www.elettric80.it</a> | &copy;  Elettric 80 Inc<strong></strong></span><br />
        </div>
    </div></td>
  </tr>
</table>
<script type="text/javascript">
messageObj = new DHTML_modalMessage();	// We only create one object of this class
messageObj.setShadowOffset(5);	// Large shadow

function displayStaticMessage(messageContent,cssClass)
{
	messageObj.setHtmlContent(messageContent);
	messageObj.setSize(300,150);
	messageObj.setCssClassMessageBox(cssClass);
	messageObj.setSource(true);	// no html source since we want to use a static message here.
	messageObj.setShadowDivVisible(false);	// Disable shadow for these boxes	
	messageObj.display();
	
	
}

function closeMessage()
{
	messageObj.close();	
}


</script>
<br />
</body>
</html>
