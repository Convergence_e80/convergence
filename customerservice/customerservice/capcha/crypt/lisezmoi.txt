Configuration pour Cryptographp v1.4
www.cryptographp.com


Comment installer cette configuration:
--------------------------------------


- Remplacer le fichier "cryptographp.cfg.php" par celui fourni dans ce package
- Dans le r�pertoire "fonts" copier le fichier "verdana.ttf" (*)



(*) La police Verdana n'est pas un produit libre, le fichier "verdana.ttf" n'est donc pas 
    fourni dans ce package. Vous devez le copier depuis un autre ordinateur.
    Attention, le nom doit �tre en minuscules: "verdana.ttf"

