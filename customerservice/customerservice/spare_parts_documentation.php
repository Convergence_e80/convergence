<!--%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

VERSION: 	

- 1.1

COMMENTS:	(add comment describing what has been changed)

-  
- 

DATE:

- 03/14/13

AUTHOR:

- Jose Hermida

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%-->
<?php
ini_set('display_errors','Off');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<link href="favicon.ico" type="image/x-icon" rel="shortcut icon"> 

<link type="text/css" rel="stylesheet" href="style.css" />
<link type="text/css" rel="stylesheet" href="bootstrap.css" />



<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<title>Elettric 80 Inc - Customer Service</title>


	<link rel="stylesheet" href="loading/css/modal-message.css" type="text/css">
	<script type="text/javascript" src="loading/js/ajax.js"></script>
	<script type="text/javascript" src="loading/js/modal-message.js"></script>
	<script type="text/javascript" src="loading/js/ajax-dynamic-content.js"></script>
	<script type="text/javascript" src="js/bootstrap-carousel.js"></script>
	
	
	<link rel="stylesheet" href="buttons.css">
	<style> 
.background1 { 
background-image: url(images/fondo_login.jpg); 
background-repeat: no-repeat; 
} 



.orange {
border-style:solid;
border-width:3px;
border-color:#FE9900;

}

.tb11 {
	background:#FFFFFF no-repeat 4px 4px;
	padding:4px 4px 4px 22px;
	border:1px solid #CCCCCC;
	width:350px;
	height:29px;
	font-size: 13px
}
.tb12 {
	background:#FFFFFF no-repeat 4px 4px;
	padding:4px 4px 4px 22px;
	border:1px solid #CCCCCC;
	width:350px;
	height:150px;
	font-size: 13px
}
a:link {
	color: #000000;
}
a:visited {
	color: #000000;
}
a:hover {
	color: #000000;
}
body {
	background-color: #FFFFFF;
}
    .style4 {font-size: 13px}
    .style5 {font-family: Arial, Helvetica, sans-serif}
    .style7 {font-size: 13px; font-family: Arial, Helvetica, sans-serif; }
    .style8 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 22px;
	color: #0061A8;
}
    .style12 {color: #000000; font-size: 13px; font-weight: bold; }
    .orange1 {border-style:solid;
border-width:1px;
border-color:#FE9900;
}
    .style13 {font-size: 13px; color: #000000; }
    .style15 {font-family: Arial, Helvetica, sans-serif; font-size: 25px; color: #0061A8; }
.style31 {font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 15px;}
.style34 {font-size: 15px}
    .style35 {color: #000000}
    .style36 {
	font-family: Arial, Helvetica, sans-serif;
	color: #000000;
	font-size: 13px;
}
    .style16 {font-size: 13px;
	color: #00569F;
	font-family: Arial, Helvetica, sans-serif;
	font-weight: bold;
}
    </style>	

	
 

<script type="text/javascript">
<!--
<!--
function submitform()
{
  document.form1.submit();
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
</head>

<body onLoad="MM_preloadImages('images/home_ov.jpg','images/help_desk_ov.jpg','images/maintenance_ov.jpg','images/training_ov.jpg','images/contact_ov.jpg','images/products_ov.jpg')"    >
<span class="style4">
<?php session_start(); if  ($_SESSION["user_access"]== "access") { ?>
</span>
<table width="950" height="45" border="0" align="center" cellpadding="0" cellspacing="0"  >
  <tr>
    <td width="638"><div align="right"><img src="images/phone.jpg" width="24" height="24"></div></td>
    <td width="10">&nbsp;</td>
    <td width="140"><span class="style7">USA: +(847) 329-7717</span></td>
    <td width="28"><div align="right"><img src="images/Mail.jpg" width="24" height="16"></div></td>
    <td width="9">&nbsp;</td>
    <td width="127"><a href="mailto:e80.usa@elettric80.it" class="style4 style5">e80.usa@elettric80.it</a></td>
  </tr>
</table>
<span class="style4"></span><span class="style4">
<?php } else { ?>
</span><span class="style4"> </span>
<table width="950" height="36" border="0" align="center" cellpadding="0" cellspacing="0"  >
  <tr>
    <td width="485"><div align="right"><img src="images//phone.jpg" width="24" height="24"></div></td>
    <td width="11">&nbsp;</td>
    <td width="153"><span class="style7">USA: +(847) 329-7717</span></td>
    <td width="25"><div align="right"><img src="images//Mail.jpg" width="24" height="16"></div></td>
    <td width="9">&nbsp;</td>
    <td width="128"><a href="mailto:e80.usa@elettric80.it" class="style4 style5">e80.usa@elettric80.it</a></td>
    <td width="27">&nbsp;</td>
    <td width="112"><table width="112%" height="27" border="0" cellpadding="0" cellspacing="0" class="orange1">
      <tr>
        <td bgcolor="#FEBF4D"><div align="center"><a href="#login-box" class="login-window  style12"  style='text-decoration: none;' > <font color="white" > <strong> My Account </strong> </font> </a> </div></td>
      </tr>
    </table></td>
  </tr>
</table>
<span class="style4">
<?php }  ?>
</span>
<table width="950" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr>
    <td width="617"><table width="950" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="538"><img src="images/logo_after_sales.jpg" width="538" height="73"></td>
        <td width="21">&nbsp;</td>
        <td width="10">&nbsp;</td>
        <td width="381" valign="baseline"><?php 
	session_start();
	if  ($_SESSION["user_access"]== "access") {
  
 
      include("../connection.php");

      $snom = $_SESSION["nom"];
	  //echo "es: ".$snom;

	  
	  $comp = $_SESSION["company_id"];

	  $tsql = "SELECT * FROM Customer_User_Login WHERE Customer_Id = '$snom'  ";	 
	  //print_r($tsql); 
      $result = sqlsrv_query( $conn, $tsql ); 
	  $row = sqlsrv_fetch_array($result);

	 $tsql_em_dep = "SELECT * FROM Customers WHERE Id = '$comp' ";
	 $result_emp_dep = sqlsrv_query( $conn, $tsql_em_dep);
	 $row_emp_dep = sqlsrv_fetch_array($result_emp_dep);	
	
	
	?>
            <br>
            <br>
            <br>
            <div align="center" class="style7">
              <div align="right">Hello,<strong> <?php echo $row[1]." ".$row[2];   $user = $row[1]." ".$row[2]; ?> </strong> |&nbsp;<a href="logout.php">logout </a></div>
            </div>
          <?php } else { ?>
            <br>
            <br>
            <br>
            <div align="center" class="style7">
              <div align="right"> <a href="ex"> </a></div>
            </div>
          <?php }   ?>
        </td>
      </tr>
    </table>
    <script type="text/javascript">
messageObj = new DHTML_modalMessage();	// We only create one object of this class
messageObj.setShadowOffset(5);	// Large shadow

function displayMessage(url)
{ 
	
	messageObj.setSource(url);
	messageObj.setCssClassMessageBox(false);
	messageObj.setSize(400,200);
	messageObj.setShadowDivVisible(true);	// Enable shadow for these boxes
	messageObj.display();
}

function displayStaticMessage(messageContent,cssClass)
{
	messageObj.setHtmlContent(messageContent);
	messageObj.setSize(300,150);
	messageObj.setCssClassMessageBox(cssClass);
	messageObj.setSource(false);	// no html source since we want to use a static message here.
	messageObj.setShadowDivVisible(false);	// Disable shadow for these boxes	
	messageObj.display();
	
	
}



function closeMessage()
{
	messageObj.close();	
}


</script>
	  
    </td>
  </tr>
  <tr>
    <td><div align="center"> <br>
      <table width="944" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="135"><a href="index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('home','','images/home_ov.jpg',1)"><img src="images/home.jpg" name="home" width="135" height="55" border="0"></a> </td>
          <td width="134"><a href="help_desk.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('help_desk','','images/help_desk_ov.jpg',1)"><img src="images/help_desk.jpg" name="help_desk" width="135" height="55" border="0"></a></td>
          <td width="135"><img src="images/spare_parts_ov.jpg" name="spare_parts" width="135" height="55" border="0"></td>
          <td width="135"><a href="maintenance.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('maintenance','','images/maintenance_ov.jpg',1)"><img src="images/maintenance.jpg" name="maintenance" width="135" height="55" border="0"></a></td>
          <td width="135"><a href="products.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Products','','images/products_ov.jpg',1)"><img src="images/products.jpg" name="Products" width="135" height="55" border="0"></a></td>
          <td width="135"><a href="training.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('training','','images/training_ov.jpg',1)"><img src="images/training.jpg" name="training" width="135" height="55" border="0"></a></td>
          <td width="135"><a href="contact.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Contact','','images/contact_ov.jpg',1)"><img src="images/contact.jpg" name="Contact" width="135" height="55" border="0"></a></td>
        </tr>
      </table>
      </div></td>
  </tr>
  <tr>
    <td>   </td>
  </tr>
  <tr>
    <td>
	
<table width="946" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td><br>
        <br>
            <br>
            <table width="944" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td width="413" class="style15">Spare Parts Documentation <span class="style4"> </span></td>
          <td width="520" class="style8"><span class="style4">
            <?php if  ($_SESSION["user_access"]== "access") { ?>
          </span> Company: <strong>
          <?php  echo $row_emp_dep[1];	 ?>
          </strong><span class="style4">
          <?php }   ?>
          </span></td>
          <td width="11">&nbsp;</td>
        </tr>
      </table>
            <span class="style4">
            
            
            <?php 
					$sql_docs = "SELECT * FROM Documents WHERE  Second_Id = '$comp' AND Type='customer'";
					//print_r($sql_docs);
					$result_docs = sqlsrv_query( $conn, $sql_docs);
					
					$x = 0;
					while($row_docs = sqlsrv_fetch_array($result_docs)) {	
					
					$x = $x + 1;
					
           ?>
           
           
           <br>
            </span>
            <p class="style13"><br>
            </p>
               
                <div align="justify"><br>
                </div>
                <div id="cont_tab_1" name="cont_tab_1" style="display:block;">
                  <table width="900" height="31" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                      <td width="707" align="left" valign="middle" class="style36"><?php echo $row_docs[2]; ?></td>
                      <td width="59" valign="middle" class="style36"><div align="center">
                        <?php 
					      $doc = "docx";
						  $xls = "xls";
						  $xlsx = "xlsx";
						  $ppt = "ppt";
						  $pdf = "pdf";
						  $rar = "rar";		
						  $pptx = "pptx";					  

						  $zip = "zip";	
						  $easm = "easm";
						   
						  $file_ext = preg_split("/\./",$row_docs[3]); 

					      if ($file_ext[1] === $doc)  { ?>
                              <img src="../convergence/images/doc_ico.jpg" title="Download Viewer">
                        <?php  }  
						  if ($file_ext[1] === $pdf)  { ?>
                              <a href="http://get.adobe.com/reader/" target="new"><img src="../convergence/images/pdf_ico.jpg" alt="" title="Download Viewer"></a>            <?php  }   ?>
                        <?php  
						  if ($file_ext[1] === $ppt)  { ?>
                        <img src="../convergence/images/ppt_ico.jpg" title="Download Viewer">
                        <?php  }   ?>
                        <?php  
						  if ($file_ext[1] === $xls)  { ?>
                        <img src="../convergence/images/excel_ico.jpg" title="Download Viewer" />
                        <?php  }   ?>
                        <?php  
						  if ($file_ext[1] === $xlsx)  { ?>
                        <img src="../convergence/images/excel_ico.jpg" title="Download Viewer" />
                        <?php  }   ?>
                        <?php  						  
						  if ($file_ext[1] === $zip)  { ?>
                             <a href="http://www.winzip.com/win/en/index.htm" target="new"><img src="../convergence/images/zip_icon_small.gif" title="Download Viewer" /></a>
                        <?php  }   ?>
                        <?php   						  
						  if ($file_ext[1] === $rar)  { ?>
                             <a href="http://www.winzip.com/win/en/index.htm" target="new"><img src="../convergence/images/zip_icon_small.gif" title="Download Viewer" /></a>
                        <?php  }   ?>
                        <?php   					  
						  if ($file_ext[1] === $pptx)  { ?>
                        <img src="../convergence/images/ppt.jpg" title="Download Viewer" />
                        <?php  }    ?>
                        <?php   					  
						  if ($file_ext[1] === $txt)  { ?>
                        <img src="../convergence/images/txt_icon.jpg" title="Download Viewer" />
                        <?php  }   ?>
                        <?php   					  
						  if ($file_ext[1] === $easm)  { ?>
                        <a href="http://www.edrawingsviewer.com/"><img src="../convergence/images/e_ico.jpg" title="Download Viewer" /></a>
                        <?php  }   ?>
                      </div></td>



                      <td width="134">
					  
					    <?php if ($file_ext[1] === $pdf)  {      ?>
                        <a href="display_pdf.php?Id=<?php echo $row_docs[0]; ?> " target="_blank" class="style36" > Download file </a>
                        <?php  } else {    ?>
                        <a href="../convergence/uploads/customers_documents/<?php echo $row_docs[3]; ?> " target="_blank" class="style36" > Download file </a>
                      <?php  }  ?></td>
                    </tr>
                  </table>
                  <div align="center"><img src="images/linea_punteada.jpg" width="900" height="1">
                    <?php 
		    }
      ?>
                  </div>
                    <br>
                     </div>
                       <p><br>
                         
                         <?php 
   if ($x == 0) {
	   
    ?>
                         <br>
                         <br>
                       </p>
            <table width="112%" height="40" border="0" cellpadding="0" cellspacing="0" class="orange1">
                         <tr>
                           <td width="6%" bgcolor="#FAC35E"><div align="center"><img src="images/Warning-icon.jpg" width="24" height="21" alt=""/></div></td>
                           <td width="94%" bgcolor="#FAC35E"><span class="style16">
                             <?php
	  

		  
		  
		  $error_msg="No documents were found";
		  
		  
		   echo $error_msg; 
    
     ?>
                           </span></td>
              </tr>
                       </table>
            <p class="style4">
              <?php   
	      }
    ?>           
		  </p></td>
  </tr>
</table>	  </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><img src="images/bar.jpg" width="948" height="45"></td>
  </tr>
</table>
<table width="816" height="22" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="951" align="center"></td>
  </tr>
  <tr>
    <td align="right" valign="middle"  >
	<span class="style4"><br>
    </span>
	<div align="right" >
      <div align="center" class="style5" ><span class="style4"><a href="http://www.elettric80.it">www.elettric80.it</a> | &copy;  Elettric 80 Inc</span><br />
        </div>
    </div></td>
  </tr>
</table>
 
<br />

<style type="text/css">
body{
	 font:bold 13px Arial, Helvetica, sans-serif;
	 margin:0;
	 padding:0;
	 min-width:200px;
}

a { 
	text-decoration:none; 
	color:#00c6ff;
}


.container {width: 960px; margin: 0 auto; overflow: hidden;}

#content {	float: left; width: 100%;}

.post { margin: 0 auto; padding-bottom: 50px; float: left; width: 960px; }

 

.btn-sign a { color:#fff; text-shadow:0 1px 2px #161616; }

#mask {
	display: none;
	background: #F6A800; 
	position: fixed; left: 0; top: 0; 
	z-index: 10;
	width: 100%; height: 100%;
	opacity: 0.4;
	z-index: 999;
}

.login-popup{
	display:none;
	background: #fff;
	padding: 10px; 	
	border: 1px solid #0070B8;
	float: left;
	font-size: 1.2em;
	position: fixed;
	top: 50%; left: 50%;
	z-index: 99999;
	box-shadow: 0px 0px 20px #999;
	-moz-box-shadow: 0px 0px 20px #999; /* Firefox */
    -webkit-box-shadow: 0px 0px 20px #999; /* Safari, Chrome */
	border-radius:3px 3px 3px 3px;
    -moz-border-radius: 3px; /* Firefox */
    -webkit-border-radius: 3px; /* Safari, Chrome */
}

img.btn_close {
	float: right; 
	margin: -8px -8px 0 0;
}

 

form.signin .textbox label { 
	display:block; 
	padding-bottom:7px; 
}

form.signin .textbox span { 
	display:block;
}

form.signin p, form.signin span { 
	color:#000; 
	font-size:13px; 
	line-height:18px;
} 

form.signin .textbox input { 
	background:#fff; 
	border-bottom:1px solid #333;
	border-left:1px solid #000;
	border-right:1px solid #333;
	border-top:1px solid #000;
	color:#000; 
	border-radius: 3px 3px 3px 3px;
	-moz-border-radius: 3px;
    -webkit-border-radius: 3px;
	font:13px Arial, Helvetica, sans-serif;
	padding:6px 6px 4px;
	width:200px;
}

form.signin input:-moz-placeholder { color:#bbb; text-shadow:0 0 2px #000; }
form.signin input::-webkit-input-placeholder { color:#bbb; text-shadow:0 0 2px #000;  }

.button { 
	background: -moz-linear-gradient(center top, #f3f3f3, #dddddd);
	background: -webkit-gradient(linear, left top, left bottom, from(#f3f3f3), to(#dddddd));
	background:  -o-linear-gradient(top, #f3f3f3, #dddddd);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorStr='#f3f3f3', EndColorStr='#dddddd');
	border-color:#000; 
	border-width:1px;
	border-radius:4px 4px 4px 4px;
	-moz-border-radius: 4px;
    -webkit-border-radius: 4px;
	color:#333;
	cursor:pointer;
	display:inline-block;
	padding:6px 6px 4px;
	margin-top:10px;
	font:13px; 
	width:214px;
}

.button:hover { background:#ddd; }

</style>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$('a.login-window').click(function() {
		
		// Getting the variable's value from a link 
		var loginBox = $(this).attr('href');

		//Fade in the Popup and add close button
		$(loginBox).fadeIn(300);
		
		//Set the center alignment padding + border
		var popMargTop = ($(loginBox).height() + 24) / 2; 
		var popMargLeft = ($(loginBox).width() + 24) / 2; 
		
		$(loginBox).css({ 
			'margin-top' : -popMargTop,
			'margin-left' : -popMargLeft
		});
		
		// Add the mask to body
		$('body').append('<div id="mask"></div>');
		$('#mask').fadeIn(300);
		
		return false;
	});
	
	// When clicking on the button close or the mask layer the popup closed
	$('a.close, #mask').live('click', function() { 
	  $('#mask , .login-popup').fadeOut(300 , function() {
		$('#mask').remove();  
	}); 
	return false;
	});
});
</script>


 <div class="container">
	<div id="content">
    

        
        <div id="login-box" class="login-popup">
		
        <a href="#" class="close"><img src="images/close_pop.jpg" class="btn_close" title="Close Window" alt="Close" /></a>
          <form method="post" class="signin" action="login.php">
                <fieldset class="textbox">
            	<label class="username">
                <span>Username</span>
                <input id="txtuser" name="txtuser" value="<?php echo $_COOKIE["cookie_user"];?>" type="text" autocomplete="on" placeholder="Username">
                </label>
                
                <label class="password">
                <span>Password</span>
                <input id="txtpass" name="txtpass" value="<?php echo $_COOKIE["cookie_password"];?>" type="password" placeholder="Password">
                </label>
                
                <button class="button yellow small" type="submit">Sign in</button>
                <p>
                <a class="forgot" href="password_recovery.php">Forgot your password?</a>
                </p>
                
                </fieldset>
          </form>
		</div>
    
    </div>
</div>

</body>
</html>
