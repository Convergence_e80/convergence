Configuration for Cryptographp v1.4
www.cryptographp.com


How to install this configuration:
----------------------------------


- Replace the file �cryptographp.cfg.php� by that in this package 
- In the "fonts" directory copy the file "verdana.ttf" (*)



(*) The Verdana font isn't a free font. So, i can't provide the file 
    �verdana.ttf� in this package. You must copy it since another computer.
    Warning: the name must be in small letters: �verdana.ttf�
