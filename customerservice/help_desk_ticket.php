<!--%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

VERSION: 	

- 1.1

COMMENTS:	(add comment describing what has been changed)

-  
- 

DATE:

- 03/14/13

AUTHOR:

- Jose Hermida

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%-->
<?php
session_cache_limiter('private, must-revalidate');
ini_set('display_errors','Off');
include("check_connection.php");	// import file for checking the session of the login
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<link href="favicon.ico" type="image/x-icon" rel="shortcut icon"> 

<link type="text/css" rel="stylesheet" href="style.css" />
<link type="text/css" rel="stylesheet" href="bootstrap.css" />



<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<title>Elettric 80 Inc - Customer Service</title>


	<link rel="stylesheet" href="loading/css/modal-message.css" type="text/css">
	<script type="text/javascript" src="loading/js/ajax.js"></script>
	<script type="text/javascript" src="loading/js/modal-message.js"></script>
	<script type="text/javascript" src="loading/js/ajax-dynamic-content.js"></script>
	<script type="text/javascript" src="js/bootstrap-carousel.js"></script>
	
	
	<link rel="stylesheet" href="buttons.css">
	<style> 
.background1 { 
background-image: url(images/fondo_login.jpg); 
background-repeat: no-repeat; 
} 



.orange {
border-style:solid;
border-width:3px;
border-color:#FE9900;

}

.gray {
border-style:solid;
border-width:1px;
border-color:#CDCDCD;

}

 
.tb11 {
	background:#FFFFFF no-repeat 4px 4px;
	padding:4px 4px 4px 22px;
	border:1px solid #CCCCCC;
	width:350px;
	height:29px;
	font-size: 13px
}
.tb12 {
	background:#FFFFFF no-repeat 4px 4px;
	padding:4px 4px 4px 22px;
	border:1px solid #CCCCCC;
	width:350px;
	height:150px;
	font-size: 13px
}
a:link {
	color: #000000;
}
a:visited {
	color: #000000;
}
a:hover {
	color: #000000;
}
body {
	background-color: #FFFFFF;
}
    .style4 {font-size: 13px;
	         font-family: Arial, Helvetica, sans-serif;
	        }
    .style5 {font-family: Arial, Helvetica, sans-serif}
    .style7 {font-size: 13px; font-family: Arial, Helvetica, sans-serif; }
    .style12 {font-size: 13px; font-family: Arial, Helvetica, sans-serif; }
	
    .style8 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 22px;
	color: #0061A8;
}
.tb111 {	padding:4px 4px 4px 3px;
	border:1px solid #CCCCCC;
	width:205px;
	height:30px;
}
    .style79 {font-family: Arial, Helvetica, sans-serif; font-size: 20px; color: #0061A8; }
    .style80 {font-size: 13px; font-family: Arial, Helvetica, sans-serif; }
    .style81 {font-size: 13px}
    .style82 {
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
}
    body,td,th {
	color: #000000;
}
.style84 {font-size: 13px; font-family: Arial, Helvetica, sans-serif; font-weight: bold; }
    .style85 {font-size: 25px}
    </style>	

	
 

<script type="text/javascript">
<!--
<!--
function submitform()
{
  document.form1.submit();
}

function envio () {
document.getElementById("form1").submit();
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>

<script type="text/javascript" src="jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="jquery.tablesorter.min.js"></script>


<script language="JavaScript1.2" >
<!--

//for tablesorting
$(document).ready(function()     {         $("#myTable").tablesorter();     } ); 

//for mouse over effects
function cambiar_color_over(celda){ 
   celda.style.backgroundColor="#F9BF6B" 
} 
function cambiar_color_out(celda){ 
   celda.style.backgroundColor="#FFFFFF" 
}
//-->
</script>

		 <script>
		 function window_function (bar,list) {
		 	list=document.getElementById(list); 
			bar=document.getElementById(bar);
			
			if(list.style.display=='none') {
				list.style.display='block';
				bar.src="images/history_ticket-.jpg";
			} 
			
			else {
			list.style.display='none'; 
			bar.src='images/history_ticket.jpg';
			}
		 }
		 </script>

 <script>
        function goBack() { window.history.back(); }		 
</script>

</head>

<body onLoad="MM_preloadImages('images/home_ov.jpg','images/spare_parts_ov.jpg','images/maintenance_ov.jpg','images/training_ov.jpg','images/contact_ov.jpg','images/products_ov.jpg','images/help_desk_ov.jpg')"    >
<table width="950" height="45" border="0" align="center" cellpadding="0" cellspacing="0"  >
  <tr>
    <td width="638"><div align="right"><img src="images/phone.jpg" width="24" height="24"></div></td>
    <td width="10">&nbsp;</td>
    <td width="140"><span class="style7">USA: +(847) 329-7717</span></td>
    <td width="28"><div align="right"><img src="images/Mail.jpg" width="24" height="16"></div></td>
    <td width="9">&nbsp;</td>
    <td width="127"><a href="mailto:e80.usa@elettric80.it" class="style4 style5">e80.usa@elettric80.it</a></td>
  </tr>
</table>
<table width="950" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr>
    <td width="617"><table width="950" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="538"><img src="images/logo_after_sales.jpg" width="538" height="73"></td>
        <td width="21">&nbsp;</td>
        <td width="10">&nbsp;</td>
        <td width="381" valign="baseline"><?php 
	session_start();
	if  ($_SESSION["user_access"]== "access") {
  
 
      //include("../connection.php");

      $snom = $_SESSION["nom"];
	  //echo "es: ".$snom;

	  
	  $comp = $_SESSION["company_id"];

	  $tsql = "SELECT * FROM Customer_User_Login WHERE Customer_Id = '$snom'  ";	 
	  //print_r($tsql); 
      $result = sqlsrv_query( $conn, $tsql ); 
	  $row = sqlsrv_fetch_array($result);

	 $tsql_em_dep = "SELECT * FROM Customers WHERE Id = '$comp' ";
	 $result_emp_dep = sqlsrv_query( $conn, $tsql_em_dep);
	 $row_emp_dep = sqlsrv_fetch_array($result_emp_dep);	
	
	
	?>
            <br>
            <br>
            <br>
            <div align="center" class="style80">
              <div align="right">Hello,<strong> <?php echo $row[1]." ".$row[2];   $user = $row[1]." ".$row[2]; ?> </strong> |&nbsp;<a href="logout.php">logout </a></div>
            </div>
          <?php } else { ?>
            <br>
            <br>
            <br>
            <div align="center" class="style80">
              <div align="right"> <a href="ex"> </a></div>
            </div>
          <?php }   ?>
        </td>
      </tr>
    </table>
    <script type="text/javascript">
messageObj = new DHTML_modalMessage();	// We only create one object of this class
messageObj.setShadowOffset(5);	// Large shadow

function displayMessage(url)
{ 
	
	messageObj.setSource(url);
	messageObj.setCssClassMessageBox(false);
	messageObj.setSize(400,200);
	messageObj.setShadowDivVisible(true);	// Enable shadow for these boxes
	messageObj.display();
}

function displayStaticMessage(messageContent,cssClass)
{
	messageObj.setHtmlContent(messageContent);
	messageObj.setSize(300,150);
	messageObj.setCssClassMessageBox(cssClass);
	messageObj.setSource(false);	// no html source since we want to use a static message here.
	messageObj.setShadowDivVisible(false);	// Disable shadow for these boxes	
	messageObj.display();
	
	
}



function closeMessage()
{
	messageObj.close();	
}


</script>
	  
    </td>
  </tr>
  <tr>
    <td><div align="center"> <br>
      <table width="944" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="135"><a href="index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('home','','images/home_ov.jpg',1)"><img src="images/home.jpg" name="home" width="135" height="55" border="0"></a> </td>
          <td width="134"><a href="help_desk.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Help_Desk','','images/help_desk_ov.jpg',1)"><img src="images/help_desk.jpg" name="Help_Desk" width="135" height="55" border="0"></a></td>
          <td width="135"><a href="spare_parts.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('spare_parts','','images/spare_parts_ov.jpg',1)"><img src="images/spare_parts.jpg" name="spare_parts" width="135" height="55" border="0"></a></td>
          <td width="135"><a href="maintenance.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('maintenance','','images/maintenance_ov.jpg',1)"><img src="images/maintenance.jpg" name="maintenance" width="135" height="55" border="0"></a></td>
          <td width="135"><a href="products.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Products','','images/products_ov.jpg',1)"><img src="images/products.jpg" name="Products" width="135" height="55" border="0"></a></td>
          <td width="135"><a href="training.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('training','','images/training_ov.jpg',1)"><img src="images/training.jpg" name="training" width="135" height="55" border="0"></a></td>
          <td width="135"><a href="contact.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Contact','','images/contact_ov.jpg',1)"><img src="images/contact.jpg" name="Contact" width="135" height="55" border="0"></a></td>
        </tr>
      </table>
      </div></td>
  </tr>
  <tr>
    <td>   </td>
  </tr>
  <tr>
    <td>
	
<table width="946" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td bgcolor="#FFFFFF"><div align="center"> <br>
        <br>
          <table width="944" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td width="413"><div align="left"><span class="style8"><span class="style85">Help Desk  </span><br>
          </span></div></td>
          <td width="520"><span class="style8"><span class="style81">
            <?php if  ($_SESSION["user_access"]== "access") { ?>
          </span> Company: <strong>
          <?php  echo $row_emp_dep[1];	 ?>
          </strong><span class="style81">
          <?php }   ?>
          </span></span></td>
          <td width="11">&nbsp;</td>
        </tr>
      </table>
      <br>
    </div>
    <p align="center"></p>
      <div align="center">
          <table width="947" border="0" align="center" cellpadding="0" cellspacing="0">
            <?php $IdUser = $_GET[Id]; ?>
            <form  action="../delete_ticket.php?Id=<?php echo $IdUser; ?>" method="POST" id="Novinky" onSubmit="return handleSubmit();">
              <tr>
                <td width="537"><div align="left">
                    <table width="332" height="37" border="0" cellpadding="0" cellspacing="0">
                      <tr>
                        <td width="131"><div align="left"> <span class="style79">Ticket # <strong><?php echo $IdUser; ?></strong></span></div></td>
                        <td width="59">&nbsp;</td>
                      </tr>
                    </table>
                </div></td>
                <td width="180"><a href="#" onclick="history.go(-1);return false;"></a> </td>
                <td width="74">&nbsp;                    </td>
                <td width="14">&nbsp;  </td>
                <td width="26">&nbsp; </td>
                <td width="7">&nbsp;</td>
                <td width="109"><a href="../add_ticket.php"></a></td>
              </tr>
            </form>
          </table>
      </div>
      <br>
        <?php 
		
    $Id = $_GET[Id];
	
	
	$query_ticket = "select * from Tickets where Id= $Id;";
	$result_query_ticket = sqlsrv_query($conn,$query_ticket);
	$row_query_ticket = sqlsrv_fetch_array($result_query_ticket);

				$query_creator = "select * from Employees where Id=$row_query_ticket[3];";
				$result_query_creator = sqlsrv_query($conn,$query_creator);
				$row_query_creator = sqlsrv_fetch_array($result_query_creator);
				
				if ($row_query_creator[8]!=NULL)
					$profile_pic=$row_query_creator[8];
				else
					$profile_pic="images/no_picture.jpg";
				?>
        <table width="947" height="76" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
          <tr>
            <td align="center" valign="middle" background="images/fondo_ticket_new.jpg" height="90"><table width="947" height="110" border="0" align="right" cellpadding="0" cellspacing="0">
                <tr>
                  <td width="109"><div align="center"><img src="../convergence/<?php echo $profile_pic; ?>" width="90" height="90">
                  </div></td>
                  <td width="8">&nbsp;</td>
                  <td width="362"><table width="356" height="95" border="0" align="left" cellpadding="0" cellspacing="0">
                      <tr>
                        <td width="128"><div align="right" class="style13 style5 style4">Posted by: </div></td>
                        <td width="9"><span class="style12"></span></td>
                        <td width="233">                          <div id="contentPad_"> 
                            
                            <div align="left"><span class="style16"> 
                              
                              
                              <span class="style4"> 
                                
                                 <?php echo $row_query_creator[1]." ".$row_query_creator[13]; $creator = $row_query_creator[0];  ?>                                  </span> 
                              <br />
                            </span></div>
                        </div></td>
                      </tr>
                      <tr>
                        <td><div align="right" class="style13 style5 style4">
                            <?php
					$query_customer = "select * from Customers where Id=$row_query_ticket[8];";
					$result_query_customer = sqlsrv_query($conn,$query_customer);
					$row_query_customer = sqlsrv_fetch_array($result_query_customer);
					
					//echo "cust es: ".$comp."otro cus es: ".$row_query_customer[0];
					if ($comp != $row_query_customer[0]) {
					header("Location: help_desk.php");
					}
					
					?>
                          Customer:</div></td>
                        <td><span class="style12"></span></td>
                        <td><div align="left"><span class="style7"> <?php echo $row_query_customer[1]; ?> </span></div></td>
                      </tr>
                      <tr>
                        <td><div align="right" class="style4">Support Type:</div></td>
                        <td><span class="style12"></span></td>
                        <td><div align="left"><span class="style7"> 
                          <?php 
				         $tsql_support = "select * from Support_Types WHERE Id = '$row_query_customer[12]'"; 
				         //print_r($tsql_support);
				         $result_support = sqlsrv_query( $conn, $tsql_support);
						 $row_support = sqlsrv_fetch_array($result_support);
				         echo $row_support[1];
				          ?>
                        </span></div></td>
                      </tr>
                      <tr>
                        <td><div align="right" class="style13 style5 style4">Connection Option:</div></td>
                        <td><span class="style12"></span></td>
                        <td><div id="contentPad"> 
                          <div align="left"><span class="formInfo style5 style4">  <a href="../tooltip_connection.php?width=450&Id=<?php echo $row_query_customer[14]; ?>" class="jTip" id="one" name=""> <strong> <?php echo $row_query_customer[14]; ?>  </strong> </a> </span> <br />
                            </div>
                        </div></td>
                      </tr>
                      <tr>
                        <td><div align="right"><span class="style13 style5 style4">
                            <?php
					$query_cc = "select * from CustomersEquipment where Id=$row_query_ticket[6];";
					$result_query_cc = sqlsrv_query($conn,$query_cc);
					$row_query_cc = sqlsrv_fetch_array($result_query_cc); 
					?>
                          Serial Number:</span></div></td>
                        <td>&nbsp;</td>
                        <td><div align="left"><span class="style7"> <?php echo $row_query_cc[3]; ?> </span></div></td>
                      </tr>
                  </table></td>
                  <td width="446"><table width="470" height="96" border="0" align="center" cellpadding="0" cellspacing="0">
                      <tr>
                        <td width="128"><div align="right" class="style13 style5 style4">Requested by: </div></td>
                        <td width="10"><span class="style12"></span></td>
                        <td width="278"><div align="left" class="style80">
						<?php
                             $sql_tickets_request = "SELECT * FROM Help_Desk_Form  where Id_Ticket_Assigned  = '$IdUser'";	
							 
				             $result_sql_tickets_request = sqlsrv_query( $conn, $sql_tickets_request);
						     $row_result_sql_tickets_request = sqlsrv_fetch_array($result_sql_tickets_request);
 
							$sql_assignee = "SELECT * FROM Customer_User_Login WHERE Customer_Id='$row_result_sql_tickets_request[8]'";
							$result_sql_assignee = sqlsrv_query( $conn, $sql_assignee); 
							$row_sql_assignee = sqlsrv_fetch_array($result_sql_assignee); 							 
						    echo $row_sql_assignee[1]." ".$row_sql_assignee[2]; 	 						 					
						 
						?></div></td>
                      </tr>
                      <tr>
                        <td><div align="right" class="style13 style5 style4">Contact Name: </div></td>
                        <td><span class="style12"></span></td>
                        <td><div align="left"><span class="style80"><?php echo $row_query_ticket[11]; ?></span></div></td>
                      </tr>
                      <tr>
                        <td><div align="right" class="style13 style5 style4">Cell/Phone: </div></td>
                        <td><span class="style12"></span></td>
                        <td><div align="left"><span class="style80"><?php echo $row_query_ticket[12]; ?></span></div></td>
                      </tr>
                      <tr>
                        <td><div align="right" class="style13 style5 style4">Phone: </div></td>
                        <td><span class="style12"></span></td>
                        <td><div align="left"><span class="style80"><?php echo $row_query_ticket[15]; ?></span></div></td>
                      </tr>
                      <tr>
                        <td><div align="right" class="style13 style5 style4">Email:</div></td>
                        <td>&nbsp;</td>
                        <td>
						  <div align="left"><span class="style80"><?php echo $row_query_ticket[13]; ?></span></div></td>
                      </tr>
                      <tr>
                        <td><div align="right"><span class="style13 style5 style4">Account Manager:</span></div></td>
                        <td>&nbsp;</td>
                        <td><span class="style80">
                          <?php 
				         
						 
				         $tsql_em = "select * from Employees WHERE Id = '$row_query_customer[13]'"; 
				         //print_r($tsql_em);
				         $result_emp = sqlsrv_query( $conn, $tsql_em);
						 $row_emp = sqlsrv_fetch_array($result_emp);
				  
				         echo $row_emp[1]." ".$row_emp[13]; 
				        ?>
                        </span></td>
                      </tr>
                  </table></td>
                  <td width="8">&nbsp;</td>
                  <td width="14">&nbsp;</td>
                </tr>
            </table></td>
          </tr>
        </table>
      <table width="947" border="0" align="center" cellpadding="0" cellspacing="0" class="borde">
          <tr>
            <td><span class="text_bubble">
              <?php
						$query_assignee = "select * from Employees where Id=$row_query_ticket[7];";
						$result_query_assignee = sqlsrv_query($conn,$query_assignee);
						$row_query_assignee = sqlsrv_fetch_array($result_query_assignee);
						?>
              </span><br>
              <br>
              <table width="947" height="181" border="0" align="center" cellpadding="0" cellspacing="0" bordercolor="#F5F5F5">
                <tr>
                  <td><table width="947" border="0" cellpadding="0" cellspacing="0" class="gray">
                    <tr>
                      <td><br>
                        <table width="897" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                          <td width="617"><span class="style84"> <strong> <?php echo $row_query_ticket[1]; ?> </strong> </span></td>
                          <td width="280"><span class="style4"><strong>Posted date: </strong><?php echo date_format($row_query_ticket[9], 'm/d/Y').""; ?></span></td>
                        </tr>
                      </table>
                          <br></td>
                    </tr>
                    <tr>
                      <td><table width="897" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                          <td><span class="style4">
                             <?php 	echo nl2br($row_query_ticket[2]); ?> 
                          </span></td>
                        </tr>
                        
                      </table>
                        <br></td>
                    </tr>
                  </table></td>
                </tr>
                <tr>
                  <td height="42"><div align="justify"><span class="style12"><br>
                  <br>
                  </span></div></td>
                </tr>
                <tr>
                  <td background="images/fondo_ticket_2.jpg"  bgcolor="#C2C2C2"><div align="right">
                    <table width="805" height="34" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                              <td width="139" valign="middle"><div align="right" class="voice style4 style5"> Ticket Assigned to:</div></td>
                              <td width="15">&nbsp;</td>
                              <td width="202"><div align="left"><span class="style16 style4"> <?php echo $row_query_assignee[1]." ".$row_query_assignee[13]; ?> </span></div></td>
                              <td width="109"><div align="right" class="voice style4 style5">Ticket Status:</div></td>
                              <td width="13">&nbsp;</td>
                              <td width="222"><div align="left"><span class="style16 style4">
                                 <?php
					$sql_status = "select * from Ticket_Status where Id='$row_query_ticket[4]'";
					$result_sql_status = sqlsrv_query( $conn, $sql_status); 
					$row_sql_status = sqlsrv_fetch_array($result_sql_status); 
					
					echo $row_sql_status[1]; 
					?> 
                              </span></div></td>
                            </tr>
                          </table>
                    <table width="807" height="30" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                              <td width="139" valign="middle"><div align="right" class="voice style4 style5">Priority:</div></td>
                              <td width="14">&nbsp;</td>
                              <td width="206"><div align="left"><span class="style16 style4">
                                 <?php
					$sql_ticket_level = "select * from Priority where Id='$row_query_ticket[5]'";
					//print_r($sql_ticket_level);
					$result_sql_ticket_level = sqlsrv_query( $conn, $sql_ticket_level); 
					$row_sql_ticket_level = sqlsrv_fetch_array($result_sql_ticket_level); 
					echo  $row_sql_ticket_level[1]; 

					 ?> 
                              </span></div></td>
                              <td width="106"><div align="right" class="voice style4 style5">Job Type:</div></td>
                              <td width="15">&nbsp;</td>
                              <td width="220"><div align="left"><span class="style16 style4">
                                 <?php
					$sql_Job_Type = "select * from Job_Type where id='$row_query_ticket[16]'";
					$result_sql_Job_Type = sqlsrv_query( $conn, $sql_Job_Type); 
					$row_sql_Job_Type = sqlsrv_fetch_array($result_sql_Job_Type); 
					
					echo  $row_sql_Job_Type[1]; 
					?> 
                              </span></div></td>
                            </tr>
                          </table>
                          </div></td>
                </tr>
              </table>
               </td>
          </tr>
        </table>
      <table width="947" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td valign="middle"><br></td>
          </tr>
        </table>
      
      <table width="947" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td><hr></td>
          </tr>
        </table>
      
        <table width="947" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td><h3 class="style14 style82">All Replies </h3></td>
          </tr>
        </table>
      <br>
        <?php 
		$us = trim($user);
	 if ($us == "Super User") {	
	  $query_posts = "select * from Posts where Id_Ticket=$_GET[Id]";
	  } else {
	  $query_posts = "select * from Posts where Id_Ticket=$_GET[Id] and Post_Public = '1';";
	 }
	 $result_query_posts = sqlsrv_query($conn,$query_posts);
	   while ($row_query_posts = sqlsrv_fetch_array($result_query_posts)) {
	   
	   
	?>
        <?php 

		if ($row_query_ticket[3]==$snom) { 
		      
		       $delete_readed_post = "Delete  FROM Posts_Update WHERE Id_Ticket = $_GET[Id];  ";	 
			   $result_delete_readed_post = sqlsrv_query( $conn, $delete_readed_post ); 
		}
		?>
        <table width="947" border="0" align="center" cellpadding="0" cellspacing="0" class="gray">
          <tr>
            <td>
			<?php
			
			if(empty($row_query_posts[7])) {
			
			        //echo "Aca estoy";
			
					$query_creator = "select * from Employees where Id=$row_query_posts[3];";
					$result_query_creator = sqlsrv_query($conn,$query_creator);
					$row_query_creator = sqlsrv_fetch_array($result_query_creator);
					if ($row_query_creator[8]!=NULL)
						$profile_pic=$row_query_creator[8];
					else {
						$profile_pic="images/no_picture.jpg";
						}
			}	else
			
			{
			        
				  	$query_creator = "select * from Customer_User_Login where Customer_Id =$row_query_posts[7];";
					//print_r($query_creator);
					$result_query_creator = sqlsrv_query($conn,$query_creator);
					$row_query_creator = sqlsrv_fetch_array($result_query_creator);
			        $profile_pic="images/no_picture.jpg";
			
			}
			?>
              <br>
                <table width="900" border="0" align="center" cellpadding="0" cellspacing="0" bordercolor="#F5F5F5">
                  <tr>
                    <td><table width="900" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#F5F5F5">
                        <tr>
                          <td width="60"><div align="right"><img src="../convergence/<?php echo $profile_pic; ?>" width="60" height="60"></div></td>
                          <td width="8">&nbsp;</td>
                          <td width="323" valign="top"><br>
                              <table width="313" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td width="99"><div align="right" class="style13 style81 style5">Posted by: </div></td>
                                  <td width="10"><span class="style12"></span></td>
                                  <td width="204"> 
								    <span class="style81"> 
								    <?php 
								     if(empty($row_query_posts[7])) {
								            
											      echo $row_query_creator[1]." ".$row_query_creator[13]; 
												  
											 } else {
											 
											      echo $row_query_creator[1]." ".$row_query_creator[2]; 
											 }
									
								  ?>
							       </span> </td>
                                </tr>
                                <tr>
                                  <td><div align="right" class="style13 style81 style5">Date:</div></td>
                                  <td><span class="style12"></span></td>
                                  <td><span class="style80">
                                     <?php  echo date_format($row_query_posts[4], 'm/d/Y')."";  ?> 
                                  </span></td>
                                </tr>
                            </table></td>
                          <td width="292">&nbsp;</td>
                          <td width="70"><p><a href="../delete_post.php?Id=<?php echo $row_query_posts[0]; ?>&Id_ticket=<?php echo $_GET[Id]; ?>    " onClick="alert('&iquest;Confirm delete?');" ></a><br>
                                  <br>
                                  <br>
                          </p></td>
                          <td width="15"><p>&nbsp;</p></td>
                        </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><span class="style7"><br>
                        <span class="style81">"
                        <?php	echo nl2br($row_query_posts[2]); ?>
                      " </span><br>
                      <br>
                    </span></td>
                  </tr>
                  <tr>
                    <td>
 <?php
					$query_documents = "select * from Documents where Second_Id=$row_query_posts[0] AND Type='post';";
					$result_query_documents = sqlsrv_query($conn,$query_documents);
					while ($row_query_documents = sqlsrv_fetch_array($result_query_documents)) {
							
							//$ext =  stristr($row_query_documents[3], '.');
							//$PATH = "file.file2.file3.pptx";
							//$dir = substr(strrchr($PATH, "."), 0);
							
							$ext = substr(strrchr($row_query_documents[3], "."), 0);
							//$ext =  strrpos($row_query_documents[3], '.');
							//echo $row_query_documents[3]; 
							
							switch ($ext) {
							case '.jpg':
								$ext_show = 1;
								break;
							case '.mp4':
								$ext_show = 2;
								break;
							case '.png':
								$ext_show = 1;
								break;
							case '.rar':
								$ext_show = 3;
								break;
							case '.gif':
								$ext_show = 1;
								break;
							case '.swf':
								$ext_show = 3;
								break;
							case '.fla':
								$ext_show = 3;
								break;
							case '.wma':
								$ext_show = 3;
								break;
							case '.psd':
								$ext_show = 3;
								break;
							case '.docx':
								$ext_show = 3;
								break;
							case '.css':
								$ext_show = 3;
								break;
							case '.flv':
								$ext_show = 3;
								break;
							case '.pdf':
								$ext_show = 3;
								break;
							case '.ppt':
								$ext_show = 3;
								break;
							case '.xls':
								$ext_show = 3;
								break;
							case '.xlsx':
								$ext_show = 3;
								break;
							case '.doc':
								$ext_show = 3;
								break;
							case '.bmp':
								$ext_show = 3;
								break;
							case '.txt':
								$ext_show = 3;
								break;
							case '.pptx':
								$ext_show = 3;
								break;
							case '.zip':
								$ext_show = 3;
								break;																																
							case '.log':
								$ext_show = 3;
								break;									
							case '.mov':
								$ext_show = 3;
								break;						                  }	
                            
						// echo "Es".$row_query_documents[3];
						 
						   ?>
						   
						   
						     <?php if ( $ext_show == 1 ) { ?>
						     <a href="../convergence/uploads/posts_documents/<?php echo $row_query_documents[3]; ?>"  target="_blank" >
                              <img src="../convergence/uploads/posts_documents/<?php echo $row_query_documents[3]; ?>" width="750" border="0">
							 </a>
							 <?php } ?>
							 
							 <?php if ( $ext_show == 2 ) {   ?>
							 
							 <table width="650" border="0" align="center" cellpadding="0" cellspacing="0">
                             <tr>
                               <td>
							   
					
                                 <script src='video/jwplayer.js' type='text/javascript'></script>
                                 <script type="text/javascript">jwplayer.key='YW31bV70+iMwDPazCw4J2EB4lQeMYR4rYifjgQ==';</script>
								<div id="myElement">
								  <div align="center">Loading the player...</div>
								</div>
								
								<script type="text/javascript">
									jwplayer("myElement").setup({
										file: '../convergence/uploads/posts_documents/<?php echo $row_query_documents[3]; ?>',
										image: 'video/video.jpg',
										controlbar: "bottom",
										primary: "html5",
										type: "mp4",
										controls: true,
										allowscriptaccess: 'always'
									});
								</script>
  		   
							   
							   </td>
                             </tr>
                           </table>
						   
						   <?php } ?> 
						   
						   <?php if ( $ext_show == 3 ) {   ?>
						   
								<table width="500" border="0" cellspacing="0" cellpadding="0">
								  <tr>
									<td width="17">
									  <?php if ($ext == '.pdf') { ?>
									            <img src="../convergence/images/pdf_ico.jpg"> 
									  <?php } ?>
									  <?php if ($ext == '.docx') { ?>
									            <img src="../convergence/images/doc_ico.jpg"> 
									  <?php } ?>	
									  <?php if ($ext == '.zip') { ?>
									            <img src="../convergence/images/zip_icon_small.gif"> 
									  <?php } ?>	
									  <?php if ($ext == '.7z') { ?>
									            <img src="../convergence/images/zip_icon_small.gif"> 
									  <?php } ?>	
									  <?php if ($ext == '.rar') { ?>
									            <img src="../convergence/images/zip_icon_small.gif"> 
									  <?php } ?>									  								  								  										  <?php if ($ext == '.xlsx') { ?>
									            <img src="../convergence/images/icon-excel-small.gif"> 
									  <?php } ?>	
									  <?php if ($ext == '.pptx') { ?>
									            <img src="../convergence/images/ppt.jpg"> 
									  <?php } ?>		
									  <?php if ($ext == '.txt') { ?>
									            <img src="../convergence/images/txt_icon.jpg"> 
									  <?php } ?>										  							  								  
									  							 
									</td>
									<td width="10">&nbsp;</td>
									<td width="473">
									  <a href="../convergence/uploads/posts_documents/<?php echo $row_query_documents[3]; ?>"  target="_blank" >
									   <?php //echo substr($row_query_documents[3],0,20);
									           echo substr($row_query_documents[3],0,100); ?>
									  </a>									                                    
								   </td>
								  </tr>
								</table>

							
							    <br>
						    <?php } ?>  
							
							
					<?php } ?>                          </td>
                    </tr>
                  </table>
                      <br>
                </td>
              </tr>
            </table>
              <br></td>
        </tr>
      </table>
      <br>
        <?php 
	}
	?>
        <br>
        <br>
        <table width="947" border="0" align="center" cellpadding="0" cellspacing="0" class="gray">
          <tr>
            <td><form id="form1" name="form1" method="post" action="add_post_save.php?Id=<?php echo $_GET[Id]; ?>&Id_creator=<?php echo $creator; ?>"  enctype="multipart/form-data">
                <div align="center"> <br>
                    <table width="781" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td><div align="left">
                          <textarea name="post" cols="127" rows="8" id="post"></textarea>
                        </div></td>
                      </tr>
                      <tr>
                        <td><div class="description">
                          <div align="left"><br>
                            <input name="file2" type="file" style="display:none;" id="file2" 
				onChange="document.getElementById('file3').style.display='block';"/>
                            <input name="file3" type="file" style="display:none;" id="file3"
				 onChange="document.getElementById('file4').style.display='block';"/>
                            <input name="file4" type="file" style="display:none;" id="file4"/>
                          </div>
                        </div>
                            <table width="881" height="69" border="0" align="center" cellpadding="0" cellspacing="0">
                              <tr>
                                <td width="310"><div align="left">
                                  <div class="description">
                                    <div align="left">Attach files (4 max):</div>
                                  </div>
                                  <input name="file1" type="file"  
				onChange="document.getElementById('file2').style.display='block';">
                                </div></td>
                                <td width="67">&nbsp;</td>
                                <td width="504"><div align="left"><span class="text_bubble">
                                  <input name="hiddenCustomer" type="hidden" value="<?php echo $row_query_ticket[13]; ?>">
                                </span><span class="text_bubble style5 ">
                                <input name="post_copy_2" type="checkbox" id="post_copy_2" >
                                Copy to  Account Manager  | <strong> <?php echo $row_emp[7]; ?>
  <input name="hiddenCustomer_2" type="hidden" value="<?php echo $row_emp[7]; ?>">
</strong></span></div></td>
                              </tr>
                              <tr>
                                <td><div align="left" class="text_bubble style5 "></div></td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                              </tr>
                          </table>
                          <p class="text_bubble"><br>
                            <br>
                          </p>
                        </td>
                      </tr>
                      <tr>
                        <td><div align="right">
                            <table width="557" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="338" align="left"><div align="left"></div></td>
                                <td width="43">&nbsp;</td>
                                <td width="176"><input name="Submit" type="submit" class="button yellow large" value="Submit Post"    >
                                </td>
                              </tr>
                            </table>
                          <br>
                        </div></td>
                      </tr>
                    </table>
                </div>
            </form></td>
          </tr>
        </table>
    </td>
  </tr>
  <tr>
    <td><br>
      <br></td>
  </tr>
</table>	  </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="center"><img src="images/bar.jpg" width="948" height="45"></div></td>
  </tr>
  <div align="center">
    </table>
  </div>
  <table width="816" height="22" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="951" align="center"></td>
  </tr>
  <tr>
    <td align="right" valign="middle"  >
	<br>
<div align="right" >
      <div align="center" class="style5 style81" ><a href="http://www.elettric80.it">www.elettric80.it</a> | &copy;  Elettric 80 Inc <br />
        </div>
    </div></td>
  </tr>
</table>
<script type="text/javascript">
messageObj = new DHTML_modalMessage();	// We only create one object of this class
messageObj.setShadowOffset(5);	// Large shadow

function displayStaticMessage(messageContent,cssClass)
{
	messageObj.setHtmlContent(messageContent);
	messageObj.setSize(300,150);
	messageObj.setCssClassMessageBox(cssClass);
	messageObj.setSource(true);	// no html source since we want to use a static message here.
	messageObj.setShadowDivVisible(false);	// Disable shadow for these boxes	
	messageObj.display();
	
	
}

function closeMessage()
{
	messageObj.close();	
}


</script>
<br />
</body>
</html>
