<html>
  <head>
	<title>excelCOM</title>
	<meta name="author" content="Marcel Wijaya">
	<link href="style.css" rel="stylesheet" type="text/css">
	<script type="text/javascript">
		function Ajax(page){
			var xmlHttp;
			try{	
				xmlHttp=new XMLHttpRequest(); // Firefox, Opera 8.0+, Safari
			}
			catch (e){
				try{
					xmlHttp=new ActiveXObject("Msxml2.XMLHTTP"); // Internet Explorer
				}
				catch (e){
					try{
						xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
					}
					catch (e){
						alert("No AJAX!?");
						return false;
					}
				}
			}
			xmlHttp.open("GET",page,true);
			xmlHttp.send(null);
			xmlHttp.onreadystatechange=function(){
				if(xmlHttp.readyState==4){
					document.getElementById('ReloadThis').innerHTML=xmlHttp.responseText;
					setTimeout('Ajax(page)',2000);
				}
			}
		}
		window.onload=function(){
			page = "dde.php";
			Ajax(page);
		}
	</script>
  </head>
	<body >
		<h1 align="center">excelCOM</h1>
		<div id="ReloadThis"></div>
	</body>
</html>
