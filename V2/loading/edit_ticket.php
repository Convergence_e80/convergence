<?php
include("../loading/connection.php");	// import file for checking the session of the login

	$query_ticket = "select * from Tickets where Id=$_GET[Id];";
	$result_query_ticket = sqlsrv_query($conn,$query_ticket);
	$row_query_ticket = sqlsrv_fetch_array($result_query_ticket);
?>

<script>
function $(id) { return document.getElementById(id); }
</script>

<br />
<table width="398" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr>
    <td><br />
      <table width="216" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td><form id="frm" method="post" action="edit_ticket_save.php?Id=<?php echo $_GET[Id]; ?>" style="margin:0px;">
              <table width="216">
                <tr>
                  <td width="71">Priority:</td>
                  <td width="133"><select id="edit_ticket" name="priority">
                      <?php $query_priority = "select * from Priority";
			$result_query_priority = sqlsrv_query($conn,$query_priority);
			while ($row_query_priority = sqlsrv_fetch_array($result_query_priority)) { ?>
                      <option value="<?php echo $row_query_priority[0]; ?>"
			<?php if ($row_query_priority[0]==$row_query_ticket[5]) echo "selected";?>> <?php echo $row_query_priority[1]; ?> </option>
                      <?php } ?>
                    </select>                  </td>
                </tr>
                <tr>
                  <td>Status:</td>
                  <td><select id="edit_ticket" name="status">
                      <?php
			$query_status = "select * from Ticket_Status";
			$result_query_status = sqlsrv_query($conn,$query_status);
			while ($row_query_status = sqlsrv_fetch_array($result_query_status)) { ?>
                      <option value="<?php echo $row_query_status[0]; ?>"
			<?php if ($row_query_status[0]==$row_query_ticket[4]) echo "selected";?> > <?php echo $row_query_status[1]; ?> </option>
                      <?php } ?>
                    </select>                  </td>
                </tr>
                <tr>
                  <td>Job Type:</td>
                  <td><select id="select3" name="Job_type">
                      <?php
			$query_job_type = "select * from Job_Type ORDER BY id";
			$result_query_job_type = sqlsrv_query($conn,$query_job_type);
			while ($row_query_job_type = sqlsrv_fetch_array($result_query_job_type)) { ?>
                      <option value="<?php echo $row_query_job_type[0]; ?>"
			<?php if ($row_query_job_type[0]==$row_query_ticket[16]) echo "selected";?>> <?php echo $row_query_job_type[1];?> </option>
                      <?php } ?>
                  </select></td>
                </tr>
                <tr>
                  <td>Assignee:</td>
                  <td><select id="select2" name="assignee">
                      <?php
			$query_employees = "select * from Employees ORDER BY Name";
			$result_query_employees = sqlsrv_query($conn,$query_employees);
			while ($row_query_employees = sqlsrv_fetch_array($result_query_employees)) { ?>
                      <option value="<?php echo $row_query_employees[0]; ?>"
			<?php if ($row_query_employees[0]==$row_query_ticket[7]) echo "selected";?>> <?php echo $row_query_employees[1].' '.$row_query_employees[13];?> </option>
                      <?php } ?>
                  </select></td>
                </tr>
                <tr>
                  <td>Division:</td>
                  <td><select id="select" name="division">
                      <?php
			$query_division = "select * from System_Type ORDER BY Id";
			$result_query_division = sqlsrv_query($conn,$query_division);
			while ($row_query_division = sqlsrv_fetch_array($result_query_division)) { ?>
                      <option value="<?php echo $row_query_division[0]; ?>"
			<?php if ($row_query_division[0]==$row_query_ticket[14]) echo "selected";?>> <?php echo $row_query_division[1];?> </option>
                      <?php } ?>
                  </select></td>
                </tr>
                <tr>
                  <td>Level:</td>
                  <td><select id="select4" name="level">
                    <?php
			$query_level = "select * from Ticket_Levels ORDER BY Level_Id ASC";
			$result_query_level = sqlsrv_query($conn,$query_level);
			while ($row_query_level = sqlsrv_fetch_array($result_query_level)) { ?>
                    <option value="<?php echo $row_query_level[0]; ?>"
			<?php if ($row_query_level[0]==$row_query_ticket[18]) echo "selected";?>> <?php echo $row_query_level[1];?> </option>
                    <?php } ?>
                  </select></td>
                </tr>
              </table>
            <br />
              <br />
              <input name="submit" type="submit" value="Edit Ticket" />
              <button onclick="$('frm').action='ticket.php?Id=<?php echo $_GET[Id]; ?>';$('frm').submit();">Cancel</button>
          </form></td>
        </tr>
      </table>
      <br />
<br /></td>
  </tr>
</table>
