<!--%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

VERSION: 	

- 1.1

COMMENTS:	(add comment describing what has been changed)

-  
- 

DATE:

- 03/14/13

AUTHOR:

- Jose Hermida

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%-->

<?php
include("check_connection.php");	// import file for checking the session of the login
?>

<html>
<head>

<link type="text/css" rel="stylesheet" href="style.css" />

<script type="text/javascript" src="jquery-1.6.2.min.js"></script> 
<script type="text/javascript" src="jquery.tablesorter.min.js"></script> 


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Elettric 80 Inc - Data Base</title>


<script language="JavaScript1.2" >
<!--

//for tablesorting
$(document).ready(function()     {         $("#myTable").tablesorter();     } ); 

//for mouse over effects
function cambiar_color_over(celda){ 
   celda.style.backgroundColor="#F9BF6B" 
} 
function cambiar_color_out(celda){ 
   celda.style.backgroundColor="#FFFFFF" 
}
//-->
</script>

</head>

<body>
<br />

<?php include_once('header.php');?>   

<table width="850" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="images/borde_arriba.jpg" width="850" height="20" /></td>
  </tr>
</table>
<table width="850" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="4"  background="images/borde_izq.jpg" background-repeat: repeat-y; >&nbsp;</td>
    <td width="842" bgcolor="#FFFFFF"><div align="center"><br />
      <table width="835" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="605"><img src="images/tickets_top_big.jpg" width="583" height="26" border="0"></td>
            <td width="220"><BR>
                <form id="form1" name="form1" method="get" action="customers_search.php">
                  <input name="search_user" type="text"  id="search_user" />
                  <input name="search" type="submit"  id="search" value="Search" />
              </form></td>
            <td width="10">&nbsp;</td>
          </tr>
        </table>
    </div>
	<a href="add_ticket.php"><div class="add_voice"></div>
	</a>
    
	  <table width="98%" height="15" align="center" cellpadding="0" cellspacing="0"  id="myTable" class="tablesorter">
        <thead> 
			<tr>
				<th width="81" class="voice" align="left">Ticket N.</th>
				<th width="226" class="voice" align="left">Summary</th>
				<th width="70" class="voice" align="left">Status</th>
				<th width="68" class="voice" align="left">Priority</th>
				<th width="148" class="voice" align="left">Customer</th>				
				<th width="141" class="voice" align="left">Contact</th>
				<th width="106" class="voice" align="left">Updated the</th>
			</tr>
		</thead> 
		<tbody>
		
		<?php 			
	    //echo $row[0];
		$sql_tickets = "select * from Tickets where  id_Assignee = $row[0] ";
		$result_sql_tickets = sqlsrv_query( $conn, $sql_tickets); 
		while($row_sql_tickets = sqlsrv_fetch_array($result_sql_tickets)) { 
		
		?>
		
			<tr bgcolor="#FFFFFF" onMouseOver="cambiar_color_over(this)" onMouseOut="cambiar_color_out(this)">
				
			
				
			  <td valign="middle"><div class="description">
			  
			  <a href="ticket.php?Id=<?php echo $row_sql_tickets[0]; ?>"><img src="images/ticket_icon.png" width="20" height="17" border="0"></a><?php echo "# ".$row_sql_tickets[0]; ?><br>
			  </div>
				  <a href="ticket.php?Id=<?php echo $row_sql_tickets[0]; ?>"></a>				</td>
				
				<td>
					<?php
					$sql_assignee = "select * from Employees where Id='$row_sql_tickets[3]'";
					$result_sql_assignee = sqlsrv_query( $conn, $sql_assignee); 
					$row_sql_assignee = sqlsrv_fetch_array($result_sql_assignee); 
					?>
					<div class="voice"><?php echo $row_sql_tickets[1]; ?></div>
					<div class="description"><br>Reported by
					<a href="employee.php?Id=<?php  echo $row_sql_assignee[0];?>"> 
					<?php echo $row_sql_assignee[1]; ?>					</a>
					the
					<?php echo date_format($row_sql_tickets[9], 'm/d/Y').""; ?> </div>				</td>
				<td>
					<?php
					$sql_status = "select * from Ticket_Status where Id='$row_sql_tickets[4]'";
					$result_sql_status = sqlsrv_query( $conn, $sql_status); 
					$row_sql_status = sqlsrv_fetch_array($result_sql_status); 
					
					echo $row_sql_status[1]; 
					?>				</td>
				<td>
					<?php 
					$sql_priority = "select * from Priority where Id='$row_sql_tickets[5]'";
					$result_sql_priority = sqlsrv_query( $conn, $sql_priority); 
					$row_sql_priority = sqlsrv_fetch_array($result_sql_priority); 
					
					echo $row_sql_priority[1]; 
					?>				</td>	
				<td>
					<?php 
					$sql_cust = "select * from Customers where Id='$row_sql_tickets[8]'";
					$result_sql_cust = sqlsrv_query( $conn, $sql_cust); 
					$row_sql_cust = sqlsrv_fetch_array($result_sql_cust); 
					?>
                    <a href="customer.php?Id=<?php echo $row_sql_cust[0]; ?>">
                    <?php
					echo $row_sql_cust[1]; 
					?>
                    </a>				</td>
				<td><a href="customer.php?Id=<?php echo $row_sql_cust[0]; ?>">
				  <?php 
					$sql_assignee = "select * from Employees where Id='$row_sql_tickets[7]'";
					$result_sql_assignee = sqlsrv_query( $conn, $sql_assignee); 
					$row_sql_assignee = sqlsrv_fetch_array($result_sql_assignee); ?>
                  <a href="employee.php?Id=<?php echo $row_sql_assignee[0]; ?>">
                  <?php
						echo $row_sql_assignee[1]; 
						?>
				</a></td>
				<td><?php  echo date_format($row_sql_tickets[10], 'm/d/Y')."";  ?></td>
			</tr>
		
		<?php
		}
		?>	
		</tbody>
		</table>
	
	</td>
    <td width="4" background="images/borde_der.jpg" background-repeat: repeat-y; ></td>
  </tr>
</table>
<table width="850" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="images/borde_abajo.jpg" width="850" height="20" /></td>
  </tr>
</table>
<br />
<table width="850" height="22" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="951" align="center"></td>
  </tr>
  <tr>
    <td align="right" valign="middle"  ><div align="right" >
      <div align="center" > <span >&copy; Elettric 80 Inc Data Base </span><br />
            <br />
      </div>
    </div></td>
  </tr>
</table>
<br />
<p>&nbsp;</p>
</body>
</html>
