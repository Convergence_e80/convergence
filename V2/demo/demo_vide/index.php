<!doctype html>

<head>

   <!-- player skin -->
   <link rel="stylesheet" type="text/css" href="skin/minimalist.css">

   <!-- site specific styling -->
   <style type="text/css">
  
   .flowplayer { 
                 width: 600px;
                 height: 338px; 
	            }
   </style>

   <!-- flowplayer depends on jQuery 1.7.1+ (for now) -->
   <script type="text/javascript" src="jquery.min.js"></script>

   <!-- include flowplayer -->
   <script type="text/javascript" src="flowplayer.min.js"></script>

</head>

<body>

   <!-- the player -->
   <div class="flowplayer" data-swf="flowplayer.swf" data-ratio="0.4167">
      <video>

         <source type="video/mp4" src="myvideo.mp4">

      </video>
   </div>

</body>