<!--%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

VERSION: 	

- 1.1

COMMENTS:	(add comment describing what has been changed)

-  
- 

DATE:

- 03/14/13

AUTHOR:

- Jose Hermida

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%-->

<?php
include("../check_connection.php");	// import file for checking the session of the login
?>

<html>
<head>

<link type="text/css" rel="stylesheet" href="../style.css" />


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Elettric 80 Inc - Ticketing System</title>


<script language="JavaScript1.2" >
<!--
function cambiar_color_over(celda){ 
   celda.style.backgroundColor="#F9BF6B" 
} 
function cambiar_color_out(celda){ 
   celda.style.backgroundColor="#FFFFFF" 
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>

<style type="text/css">
<!--
.tb11 {background:#FFFFFF url(../images/search.png) no-repeat 4px 4px;
	padding:4px 4px 4px 22px;
	border:1px solid #CCCCCC;
	width:150px;
	height:30px;
}
.style31 {font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px;}
.style49 {font-family: Verdana, Arial, Helvetica, sans-serif}
.style59 {font-size: 11px}
.style72 {font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #094FA4; }
-->
</style>
</head>

<body onLoad="MM_preloadImages('../images/customers_top_bottom_ov.jpg','../images/employees_top_bottom_ov.jpg','../images/equipment_top_bottom_ov.jpg','../images/tickets_top_bottom_ov.jpg','../images/statistics_top_bottom_ov.jpg')">
<br />

<table width="850" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="242"><div id="div">
      <div dir="ltr">
        <div align="left"><strong><span id="result_box" lang="en" ei="4" ec="undefined" xml:lang="en"><span title="Click for alternate translations" closure_uid_swyu8z="79"><a href="salir.php" ></a></span></span></strong><a href="../principal.php"><img src="../images/small_logo_good.jpg" width="229" height="70" border="0"/></a><br />
              <br />
        </div>
      </div>
    </div></td>
    <td width="199"></td>
    <td width="82"><a href="../customers.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Customers','','../images/customers_top_bottom_ov.jpg',1)"><img src="../images/customers_top_bottom.jpg" name="Customers" width="82" height="68" border="0" id="Customers" /></a></td>
    <td width="81"><a href="../employees.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Employees','','../images/employees_top_bottom_ov.jpg',1)"><img src="../images/employees_top_bottom.jpg" name="Employees" width="81" height="68" border="0" id="Employees" /></a></td>
    <td width="81"><a href="../equipments.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Equipment','','../images/equipment_top_bottom_ov.jpg',1)"><img src="../images/equipment_top_bottom.jpg" name="Equipment" width="81" height="68" border="0" id="Equipment" /></a></td>
    <td width="81"><a href="../ticket_filter_user.php?Id=my" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Tickets','','../images/tickets_top_bottom_ov.jpg',1)"><img src="../images/tickets_top_bottom.jpg" name="Tickets" width="81" height="68" border="0" id="Tickets" /></a></td>
    <td width="84"><a href="statics.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Statistics','','../images/statistics_top_bottom_ov.jpg',1)"><img src="../images/statistics_top_bottom.jpg" name="Statistics" width="81" height="68" border="0" id="Statistics" /></a></td>
  </tr>
</table>
<table width="849" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="41" height="23"><span class="style59 style49 voice" ><strong>USER: </strong></span></td>
    <td width="638" ><span class="style31"><?php echo $row[1]." ".$row[13];
	                       //echo $_SESSION["ldpa_user"]; ?></span></td>
    <td width="41"><a href="settings.php">
      <div align="center" class="style72" ></div>
    </a></td>
    <td width="129"><div id="gt-res-content">
      <div dir="ltr">
        <div align="center" class="style72"><strong><span id="result_box" lang="en" ei="4" ec="undefined" xml:lang="en"><span title="Click for alternate translations" closure_uid_swyu8z="79"><a href="salir.php" > </a><a href="settings.php"><strong>Settings</strong></a> | <a href="salir.php" >logout</a></span></span></strong></div>
      </div>
    </div></td>
  </tr>
</table>

<table width="850" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../images/borde_arriba.jpg" width="850" height="20" /></td>
  </tr>
</table>
<table width="850" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="4"  background="../images/borde_izq.jpg" background-repeat: repeat-y; >&nbsp;</td>
    <td width="842" bgcolor="#FFFFFF"><div align="center"><br />
	
	    <form id="form1" name="form1" method="GET" action="../employees_search.php">
        <table width="822" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="594"><img src="../images/employees_top.jpg" width="583" height="34" /></td>
            <td width="217">
			

                <div align="left">
                  <table width="204" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="155"><input name="search_user" type="text" class="tb11" id="search_user" /></td>
                      <td width="10">&nbsp;</td>
                      <td width="39"><input type="image" src="../images/go.jpg" name="Submit" value="Submit"></td>
                    </tr>
                  </table>
                </div>
			
            </td>
            <td width="11">&nbsp;</td>
          </tr>
        </table>
		</form>
		
    </div>
      <table width="777" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td width="20" ></td>
          <td width="198" >&nbsp;</td>
          <td width="179" >&nbsp;</td>
          <td width="165" >&nbsp;</td>
          <td width="215" >
            <?php 
		  /* <span >
		  // Security User Check if user 
		 $sql_security_user = "select * from Security_User WHERE Employee_ID = '$_SESSION[nom]'";
		 //print_r($sql_security_user);
		 $result_sql_security_user = sqlsrv_query( $conn, $sql_security_user);
		 $row_security_user = sqlsrv_fetch_array($result_sql_security_user); 
	  
		  If ( $row_security_user[3] == 1) {
		  ?>
          +</span> <a href="add_employee.php" >Add Employee </a>
          <?php } */ ?>
          
		  
		  </td>
        </tr>
      </table>
      <br />
      <table width="800" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="21" ></td>
        <td width="205" class="voice" >Name: </td>
        <td width="185" class="voice" ><div align="center">Tickets Assigned: </div></td>
        <td width="189" class="voice" ><div align="center">Tickets in process:</div></td>
        <td width="200" class="voice" ><div align="center">Tickets Closed: </div></td>
      </tr>
    </table>
  
    <p>
      <?php 
           $tsql = "SELECT DISTINCT Id_Assignee FROM Tickets ORDER BY Id_Assignee ASC";


            $result = sqlsrv_query( $conn, $tsql);
           
		    while($row = sqlsrv_fetch_array($result)) {
			
			
			
			$sql_employee = "SELECT * FROM Employees where Id = '$row[0]'";
			$result_employee = sqlsrv_query( $conn, $sql_employee);
			$row_employee = sqlsrv_fetch_array($result_employee);
			
			
			
			
?>
    <table width="800" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td><table width="800" height="25" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" onMouseOver="cambiar_color_over(this)" onMouseOut="cambiar_color_out(this)">
            <tr>
              <td width="19" ></td>
              <td width="205" ><a href="../employee.php?Id=<?php echo $row_employee[0]; ?>"> <?php echo $row_employee[1]." ".$row_employee[13]; ?> </a></td>
              <td width="185" >
			             <div align="center">
		                 <?php 	
                     
				               $sql_Assignee = "select * from Tickets WHERE Id_Assignee = '$row_employee[0]' and Deleted_Ticket is null ";
				               $result_Assignee = sqlsrv_query( $conn, $sql_Assignee, array(), array( "Scrollable" => 'static' ) );
				               $row_Assignee = sqlsrv_num_rows($result_Assignee);	
				               echo $row_Assignee;  
		   				?>
                                </div></td>
              <td width="200" ><div align="center">
                <?php 	
                     
				               $sql_Process = "select * from Tickets WHERE Id_Assignee = '$row_employee[0]' and Status between 1 and 5 and Deleted_Ticket is null ";
				               $result_Process = sqlsrv_query( $conn, $sql_Process, array(), array( "Scrollable" => 'static' ) );
				               $row_Process = sqlsrv_num_rows($result_Process);	
				               echo $row_Process;  
		   				?>
              </div></td>
              <td width="191" ><div align="center">
                <?php 	
                     
				               $sql_Closed = "select * from Tickets WHERE Id_Assignee = '$row_employee[0]' and Status between 6 and 7 and Deleted_Ticket is null ";
				               $result_Closed = sqlsrv_query( $conn, $sql_Closed, array(), array( "Scrollable" => 'static' ) );
				               $row_Closed = sqlsrv_num_rows($result_Closed);	
				               echo $row_Closed;  
		   				?>
              </div></td>
            </tr>
            
        </table></td>
      </tr>
    </table>
    <table width="800" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td><img src="../images/linea_punteada.jpg" width="800" height="1" /></td>
      </tr>
    </table>
   
    <?php 
	}
  ?>    <br />
    <br />
    <br />
    <br /></td>
    <td width="4" background="../images/borde_der.jpg" background-repeat: repeat-y; ></td>
  </tr>
</table>
<table width="850" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img src="../images/borde_abajo.jpg" width="850" height="20" /></td>
  </tr>
</table>
<br />
<table width="850" height="22" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="951" align="center"></td>
  </tr>
  <tr>
    <td align="right" valign="middle"  ><div align="right" >
      <div align="center" > <span >&copy; Elettric 80 Inc</span> - Ticketing System<br />
        <br />
</div>
    </div></td>
  </tr>
</table>
<br />
<p>&nbsp;</p>
</body>
</html>
