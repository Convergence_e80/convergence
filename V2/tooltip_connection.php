<?php
include("check_connection.php");	// import file for checking the session of the login

	         $user = $_GET[Id];
		 
?>

<style type="text/css">
<!--
.style68 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
}
-->
</style>

<table width="405" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><?php if ($user == 'A' )  {  ?>
      <h1 align="center" class="style68">Option A (This is our standard option)</h1>
      <ul>
        <li class="style68">
          <div align="left">When connecting to monitor the system, get logs,  screen shots, files, and system information the Elettric80 technician <strong>will  be able to connect without waiting</strong> for permission from the customer but  always notifying the customer via email that we are connecting.<br />
              <br />
          </div>
        </li>
        <li class="style68">
          <div align="left">When connecting to make changes the Elettric80  technician <strong>will wait until getting permission</strong> from the customer.<br />
          </div>
        </li>
      </ul>
    <?php }  ?>
    <br />
    <?php if ($user == 'B' )  {  ?>
    <h1 align="center" class="style68">Option B (This is for customers that have exclusively opted  for this option)</h1>
    <ul>
      <li class="style68">
        <div align="left">When connecting to monitor the system, get logs, screen shots, files, and system information the Elettric80 technician <strong>will have to wait</strong> for permission from the customer<br />
            <br />
        </div>
      </li>
      <li class="style68">
        <div align="left">When connecting to make changes the Elettric80 technician <strong>will wait until getting permission</strong> from the customer<br />
        </div>
      </li>
    </ul>
    <?php }  ?></td>
  </tr>
</table>
