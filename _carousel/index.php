<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8"> 
        <meta charset="utf-8">
        <title>Bootply.com - Example Carousel</title>
        <meta name="generator" content="Bootply" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-combined.min.css" rel="stylesheet">
        
        <!--[if lt IE 9]>
          <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <link rel="shortcut icon" href="/bootstrap/img/favicon.ico">
        <link rel="apple-touch-icon" href="/bootstrap/img/apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/bootstrap/img/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/bootstrap/img/apple-touch-icon-114x114.png">









        <!-- CSS code from Bootply.com editor -->
        
        <style type="text/css">
            
        </style>
    </head>
    
    <!-- HTML code from Bootply.com editor -->
    
    <body  >
        
        <div class="row-fluid">	<div class="span9">	<div id="myCarousel" class="carousel slide">		<ol class="carousel-indicators">			<li data-target="#myCarousel" data-slide-to="0" class="active"></li>			<li data-target="#myCarousel" data-slide-to="1"></li>			<li data-target="#myCarousel" data-slide-to="2"></li>		</ol>		<div class="carousel-inner">			<div class="item active">              <img src="//placehold.it/1024x700" width="100%">				<div class="carousel-caption">					<h4>First label</h4>					<p>A working Bootstrap carousel example.</p>				</div>			</div>			<div class="item">              <img src="//placehold.it/1024x700/662222" width="100%">				<div class="carousel-caption">					<h4>Second label</h4>					<p>This is the second slide text.</p>				</div>			</div>			<div class="item">				<img src="//placehold.it/1024x700/119922" width="100%">				<div class="carousel-caption">					<h4>Third label</h4>					<p>Take note of the 'active' and 'slide' classes.</p>				</div>			</div>		</div>		<a class="left carousel-control" href="#myCarousel" data-slide="prev">‹</a>		<a class="right carousel-control" href="#myCarousel" data-slide="next">›</a>	</div>	</div>  	</div>  	
        
        <script type='text/javascript' src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>


        <script type='text/javascript' src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/js/bootstrap.min.js"></script>



        
        <!-- JavaScript jQuery code from Bootply.com editor -->
        
        <script type='text/javascript'>
        
        $(document).ready(function() {
        
            $('#myCarousel').carousel();
        
        });
        
        </script>
        
    </body>
</html>