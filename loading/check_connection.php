<?php
ob_start();
session_start();
if  (!($_SESSION["user_access"])) {
    session_destroy();
	header("Location: index.php"); 
	
}

include("connection.php");

$snom = $_SESSION["nom"];

$tsql = "SELECT * FROM Employees WHERE Id = '$snom'";	  
$result = sqlsrv_query( $conn, $tsql ); 
$row = sqlsrv_fetch_array($result);	  

$login_query = "SELECT * FROM Login WHERE Employee_Id = '$row[0]'";	  
$login_result = sqlsrv_query( $conn, $login_query ); 
$logged = sqlsrv_fetch_array($login_result);

//////////////////////////////////////////////////////////////////////////////////

//finding the url of the page where we are at...

//////////////////////////////////////////////////////////////////////////////////

function curPageURL() {
 $pageURL = 'http';
 if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
 $pageURL .= "://";
 if ($_SERVER["SERVER_PORT"] != "80") {
  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
 } else {
  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
 }
 return $pageURL;
}



$url = curPageURL();		//url

$path = parse_url($url, PHP_URL_PATH);

$components = explode('/', $path);

$php_page = $components[1]; //php_page where we are at

$allow=false;
$finacials_data=false;

switch ($logged[3]) {
	case 1:{$allow=true; $finacials_data=true;} break;
	case 2:{$allow=true;} break;
	case 3:
	{
	switch ($php_page) {
		case 'activity.php': 			$allow=true; break;
		case 'activities.php': 			$allow=true; break;
		case 'add_ticket.php':			$allow=true; break;
		case 'add_ticket_save.php':		$allow=true; break;
		case 'add_post_save.php':		$allow=true; break;
		case 'customer.php': 			$allow=true; break;
		case 'customers.php': 			$allow=true; break;
		case 'customers_search.php': 	$allow=true; break;
		case 'delete_ticket.php':		$allow=true; break;
		case 'edit_employee.php':		$allow=true; break;
		case 'edit_employee_save.php':	$allow=true; break;
		case 'edit_ticket_save.php':	$allow=true; break;
		case 'employee.php': 			$allow=true; break;
		case 'employees.php': 			$allow=true; break;
		case 'employees_search.php': 	$allow=true; break;
		case 'equipment.php': 			$allow=true; break;
		case 'equipments.php': 			$allow=true; break;
		case 'hotel.php': 				$allow=true; break;
		case 'hotels.php': 				$allow=true; break;
		case 'hotels_search.php': 		$allow=true; break;
		case 'job.php': 				$allow=true; break;
		case 'jobs.php': 				$allow=true; break;
		case 'jobs_search.php': 		$allow=true; break;
		case 'principal.php': 			$allow=true; break;
		case 'ticket.php':	 			$allow=true; break;
		case 'tickets.php': 			$allow=true; break;
	}  break;
	}
}	

if  (!$allow) {
header("Location: principal.php?Access=0");
exit; 
}
?>
