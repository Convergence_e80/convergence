<?php
include("../loading/connection.php");	// import file for checking the session of the login
ini_set('display_errors','Off');

	$query_ticket = "select * from Tickets where Id=$_GET[Id];";
	$result_query_ticket = sqlsrv_query($conn,$query_ticket);
	$row_query_ticket = sqlsrv_fetch_array($result_query_ticket);
?>

<script>
function $(id) { return document.getElementById(id); }

jQuery(document).ready(function () {

  var $ = jQuery;

  function toggle_comment() {
    var status = $("#status");
    if (status.val() == "3" || status.val() == "6") {
      $("#comment_row").css("display","table-row");
     if (status.val() == "3") {
        jQuery("td#comment_label").html("Feedback required from Customer (public post)");
      }
      else {
        jQuery("td#comment_label").html("Formal Root Cause Analysis (final public post)");
      } }
    else {
      $("#comment_row").css("display","none");
    }
  }
  
  toggle_comment();

  $("#status").change(toggle_comment);

  $("#frm").submit(function(event){
    var status = $("#status");
    if (status.val() == "3" || status.val() == "6")
    {
      if ($("textarea#comment").val() == '')
      {
        $("textarea#comment").css("border","1px solid red");
         event.preventDefault();
      }
    }
    else
    {
        $("textarea#comment").val('');
    }
   
  });
});

</script>
<style type="text/css">
.style6 {color: #CCCCCC}
</style>

<table width="800" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr>
    <td><br />
      <table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td>
		  
		  <form id="frm" method="post" action="edit_ticket_save.php?Id=<?php echo $_GET[Id]; ?>" style="margin:0px;">
              <table width="765">
                <tr>
                  <td width="85" align="right">Ticket Title:</td>
                  <td width="9">&nbsp;</td>
                  <td width="296"><input name="title" type="text" value="<?php echo $row_query_ticket[1]; ?>" size="45"></td>
                  <td width="9">&nbsp;</td>
                  <td width="146" align="right">&nbsp;</td>
                  <td width="10">&nbsp;</td>
                  <td width="178">&nbsp;</td>
                </tr>
                <tr>
                  <td align="right">Ticket Post:</td>
                  <td>&nbsp;</td>
                  <td><textarea name="post" cols="45" rows="4" class="ckeditor post_edit"><?php echo $row_query_ticket[2]; ?>
                  </textarea></td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td align="right">Priority:</td>
                  <td>&nbsp;</td>
                  <td><select id="edit_ticket" name="priority">
                    <?php $query_priority = "select * from Priority";
			$result_query_priority = sqlsrv_query($conn,$query_priority);
			while ($row_query_priority = sqlsrv_fetch_array($result_query_priority)) { ?>
                    <option value="<?php echo $row_query_priority[0]; ?>" 
			<?php if ($row_query_priority[0]==$row_query_ticket[5]) echo "selected";?>> <?php echo $row_query_priority[1]; ?></option>
                    <?php } ?>
                  </select></td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td align="right">Status:</td>
                  <td>&nbsp;</td>
                  <td><select id="status" name="status">
                    <?php



					 if ($row_query_ticket[4] == "7") { ?>
                       <option value="7" > Closed </option>
                       
                     <?php } else {      


							   $query_status = "select * from Ticket_Status_Edit";
							   $result_query_status = sqlsrv_query($conn,$query_status);
						while ($row_query_status = sqlsrv_fetch_array($result_query_status)) 
								   {   
								      ?>
                    
					
                                     
                                      
                    <option value="<?php echo $row_query_status[0]; ?>"
										<?php   
									      if ($row_query_status[0]==$row_query_ticket[4]) echo "selected";?> > 
                                          
										  <?php echo $row_query_status[1]; ?></option>
                    
                                    
                    <?php   
					         } 
					           } 
					 ?>
                   	
                   <?php  if ($row_query_ticket[4] == "6") { ?> <option value="7" > Closed </option>  <?php } ?>
                
                   
                  </select></td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr id="comment_row" width="100%">
                  <td></td>
                  <td>&nbsp;</td>
                  <td align="left" id="comment_label">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr id="comment_row" width="100%">
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>
                    <textarea name="comment" id="comment" cols="45" rows="4" class="ckeditor"><?php echo $row_query_ticket['Status'] == 6 ? $row_query_ticket['Comment'] : ""; ?></textarea>
                  </td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td align="right">Job Type:</td>
                  <td>&nbsp;</td>
                  <td><select id="select3" name="Job_type">
                    <?php
			$query_job_type = "select * from Job_Type ORDER BY id";
			$result_query_job_type = sqlsrv_query($conn,$query_job_type);
			while ($row_query_job_type = sqlsrv_fetch_array($result_query_job_type)) { ?>
                    <option value="<?php echo $row_query_job_type[0]; ?>"
			<?php if ($row_query_job_type[0]==$row_query_ticket[16]) echo "selected";?>> <?php echo $row_query_job_type[1];?></option>
                    <?php } ?>
                  </select></td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td align="right">Assignee:</td>
                  <td>&nbsp;</td>
                  <td><select id="select2" name="assignee">
                    <?php
			$query_employees = "select * from Employees ORDER BY Name";
			$result_query_employees = sqlsrv_query($conn,$query_employees);
			while ($row_query_employees = sqlsrv_fetch_array($result_query_employees)) { ?>
                    <option value="<?php echo $row_query_employees[0]; ?>"
			<?php if ($row_query_employees[0]==$row_query_ticket[7]) echo "selected";?>> <?php echo $row_query_employees[1].' '.$row_query_employees[13];?></option>
                    <?php } ?>
                  </select></td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td align="right">Division:</td>
                  <td>&nbsp;</td>
                  <td><select id="select" name="division">
                    <?php
			$query_division = "select * from System_Type ORDER BY Id";
			$result_query_division = sqlsrv_query($conn,$query_division);
			while ($row_query_division = sqlsrv_fetch_array($result_query_division)) { ?>
                    <option value="<?php echo $row_query_division[0]; ?>"
			<?php if ($row_query_division[0]==$row_query_ticket[14]) echo "selected";?>> <?php echo $row_query_division[1];?></option>
                    <?php } ?>
                  </select></td>
                  <td>&nbsp;</td>
                  <td align="right">Contact</td>
                  <td>&nbsp;</td>
                  <td> Information:</td>
                </tr>
                <tr>
                  <td align="right">Level:</td>
                  <td>&nbsp;</td>
                  <td><select id="select4" name="level">
                    <?php
			$query_level = "select * from Ticket_Levels ORDER BY Level_Id ASC";
			$result_query_level = sqlsrv_query($conn,$query_level);
			while ($row_query_level = sqlsrv_fetch_array($result_query_level)) { ?>
                    <option value="<?php echo $row_query_level[0]; ?>"
			<?php if ($row_query_level[0]==$row_query_ticket[18]) echo "selected";?>> <?php echo $row_query_level[1];?></option>
                    <?php } ?>
                  </select></td>
                  <td>&nbsp;</td>
                  <td><div align="right">Name:</div></td>
                  <td>&nbsp;</td>
                  <td><span class="style6">
                    <input name="name_contact" type="text" id="name_contact" value="<?php echo $row_query_ticket[11]; ?>" size="30">
                  </span></td>
                </tr>
                <tr>
                  <td align="right">Tag 1: </td>
                  <td>&nbsp;</td>
                  <td><select id="tag1" name="tag1">
                    <option value=""> ---- Without tag 1 ----</option>
                    <?php
							
					$query_tag_1 = "select * from Ticket_Tag where Id_Division = '$row_query_ticket[14]'";
					$result_query_tag_1 = sqlsrv_query($conn,$query_tag_1);
					while ($row_query_tag_1 = sqlsrv_fetch_array($result_query_tag_1)) { ?>
                    <option value="<?php echo $row_query_tag_1[0]; ?>"
					<?php if ($row_query_tag_1[0]==$row_query_ticket[21]) echo "selected";?>> <?php echo $row_query_tag_1[1];?></option>
                    <?php } ?>
                  </select></td>
                  <td>&nbsp;</td>
                  <td><div align="right">Cell Phone:</div></td>
                  <td>&nbsp;</td>
                  <td><span class="style6">
                    <input name="cellphone_contact" type="text" id="cellphone_contact" value="<?php echo $row_query_ticket[12]; ?>" size="30">
                  </span></td>
                </tr>
                <tr>
                  <td align="right">Tag 2: </td>
                  <td>&nbsp;</td>
                  <td><select id="tag2" name="tag2">
                    <option value=""> ---- Without tag 2 ----</option>
                    <?php
					
			$query_tag_2 = "select * from Ticket_Tag where Id_Division = '$row_query_ticket[14]'";
			$result_query_tag_2 = sqlsrv_query($conn,$query_tag_2);
			while ($row_query_tag_2 = sqlsrv_fetch_array($result_query_tag_2)) { ?>
                    <option value="<?php echo $row_query_tag_2[0]; ?>"
			<?php if ($row_query_tag_2[0]==$row_query_ticket[22]) echo "selected";?>> <?php echo $row_query_tag_2[1];?></option>
                    <?php } ?>
                  </select></td>
                  <td>&nbsp;</td>
                  <td><div align="right"> Phone:</div></td>
                  <td>&nbsp;</td>
                  <td><span class="style6">
                    <input name="phone_contact" type="text" id="phone_contact" value="<?php echo $row_query_ticket[15]; ?>" size="30">
                  </span></td>
                </tr>
                <tr>
                  <td align="right">Tag 3: </td>
                  <td>&nbsp;</td>
                  <td><select id="tag3" name="tag3">
                    <option value=""> ---- Without tag 3 ----</option>
                    <?php
					
			$query_tag_3 = "select * from Ticket_Tag where Id_Division = '$row_query_ticket[14]'";
			$result_query_tag_3 = sqlsrv_query($conn,$query_tag_3);
			while ($row_query_tag_3 = sqlsrv_fetch_array($result_query_tag_3)) { ?>
                    <option value="<?php echo $row_query_tag_3[0]; ?>"
			<?php if ($row_query_tag_3[0]==$row_query_ticket[23]) echo "selected";?>> <?php echo $row_query_tag_3[1];?></option>
                    <?php } ?>
                  </select></td>
                  <td>&nbsp;</td>
                  <td><div align="right">Email: </div></td>
                  <td>&nbsp;</td>
                  <td><span class="style6">
                    <input name="email_contact" type="text" id="email_contact" value="<?php echo $row_query_ticket[13]; ?>" size="30">
                  </span></td>
                </tr>
              </table>
            <br />
              <br />
              <input name="submit" type="submit" value="Edit Ticket" />
              <input type="button" value="Cancel" onclick="closeMessage();"/>
          </form></td>
        </tr>
      </table>
      <br></td>
  </tr>
</table>
